<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;

class ReportQueries extends Model {

  public static function report($type) {
    ini_set('memory_limit', '-1');
    set_time_limit(5000);
    $questions = DB::table('questions')
      ->select('question_categories.name as category','questions.code','questions.question')
      ->leftjoin('question_categories','question_categories.uid','=','questions.category_id')
      ->orderBy('question_categories.id')
      ->orderBy('serial')
      ->get();
    if($questions->count() > 0){
      foreach ($questions as $question) {
        $fields[$question->category][$question->code] = $question->question;
      }
    }
    $data = [];
    $title = $fields[$type];
    switch($type){
      case "registration":
        $data = DB::table("registeration_report")
        ->orderBy("entered_on", "ASC")
        ->get();
      break;
      case "assessment":
        $data = DB::table("assessment_report")
        ->orderBy("entered_on", "ASC")
        ->get();
      break;
      case "followup":
        $data = DB::table("followup_report")
        ->orderBy("entered_on", "ASC")
        ->get();
      break;
      case "sample_collection":
        $data = DB::table("sample_collection_report")
        ->orderBy("entered_on", "ASC")
        ->get();
      break;
      case "test_result":
        $data = DB::table("test_result_report")
        ->orderBy("entered_on", "ASC")
        ->get();
      break;
      case "outcome_status":
        $data = DB::table("outcome_status_report")
        ->orderBy("entered_on", "ASC")
        ->get();
      break;
      default:
        $data = [];
    }
    // dd($data);
    return [
      "data" => $data,
      "title" => $title,
    ];
  }
}
