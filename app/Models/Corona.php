<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;

class Corona extends Model
{

    public static function getIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from share internet
             $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
             $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    public static function randomPassword(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $length = rand(10, 12); 
        $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, $length );
        return $password;
    }
    
    public static function gen_uid($input){
        
        if(empty($input['chars'])) return bin2hex(openssl_random_pseudo_bytes(16, $cstrong));
        else return bin2hex(openssl_random_pseudo_bytes($input['chars'], $cstrong));
    }

    public static function gen_salt(){

        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = "";
         
        // default [a-zA-Z0-9]{9}
        $chars = $alpha . $numeric;
        $len = strlen($chars); 
        $length = 26; $pw = '';

        for ($i=0;$i<$length;$i++)
            $pw .= substr($chars, rand(0, $len-1), 1);         

        return $pw = str_shuffle($pw);
    }

    public static function generate_usernames()
    {
        
        $users = DB::table('users')
            ->select('uid','first_name','last_name')
            ->whereNull('username')
            ->get();

        foreach($users as $user)
        {
            $username = strtolower(str_replace(' ','',$user->first_name.'.'.$user->last_name)); 
            DB::table('users')
                ->where('uid', '=', $user->uid)
                ->whereNull('username')
                ->update(array('username'=>$username));
        }
    }


    public static function generate_passwords()
    {
        
        $users = DB::table('users')
            ->whereNull('deleted_at')
            ->select('uid','first_name','password_plain')
            ->whereNull('password')
            ->get();

        foreach($users as $user)
        {
            $salt = static::gen_salt();
            // $password_plain = strtolower(str_replace(' ','.',$user->first_name.'123')); 
            // $password = sha1($password_plain.$salt).":".$salt;
            $password = sha1($user->password_plain.$salt).":".$salt;
            DB::table('users')
                ->where('uid', '=', $user->uid)
                ->whereNull('password')
                ->update(array('password'=>$password));        
                // ->update(array('password_plain'=>$password_plain,'password'=>$password));        
        }
    }

    public static function check_password($passwd, $db_password) {
        // echo $passwd.' | '.$db_password.' | '. $salt;
        // if($db_password === sha1($passwd.$salt))
            // return true; else return false;
        $dbEntry = explode(':',$db_password);
        $cryptpass = $dbEntry[0];
        $salt = $dbEntry[1];
        if($db_password == sha1($passwd.$salt).':'.$salt)
             return true;
        else return false;
    }

    public static function get_push_notifications($input){

        $query = DB::table('notifications')
            ->leftjoin('notification_categories','notification_categories.uid','=','notifications.category_uid')
            ->select('notifications.uid','name as category','notification');
        if(!empty($input['category_uid'])) $query->where('category_uid',$input['category_uid']);
        return $notifications =$query->get();
    }

    public static function get_personal_notifications($input){

        $query = DB::table('user_notifications')
            ->leftjoin('notifications','notifications.uid','=','user_notifications.notification_uid')
            ->leftjoin('notification_categories','notification_categories.uid','=','notifications.category_uid')
            ->where('user_notifications.user_uid',$input['user_uid'])
            ->orderBy('user_notifications.created_at','desc')
            ->select(
                'user_notifications.user_uid',
                'user_notifications.uid as user_notifications_uid',
                'notification_uid',
                'notification_categories.uid as notification_categories_uid',
                'notification_categories.name as notification_category',
                'notifications.notification','user_notifications.created_at'
            );
            // echo $query->toSql();
            $user_notifications = $query->get();
            // echo '<pre>';print_r($user_notifications); exit();
            return $user_notifications;
    }

    public static function get_user_notifications($input){

        // echo '<pre>'; print_r($input); echo '</pre>'; //exit();
        $query = DB::table('notifications')
            ->leftjoin('notification_categories','notification_categories.uid','=','notifications.category_uid')
            ->select('notifications.uid','name as category','notification');
            if(!empty($input['category_uid'])) $query->where('category_uid',$input['category_uid']);
            // echo $query->toSql(); exit();
        $notifications = $query->get();
        if(!empty($notifications)){
        foreach ($notifications as $key => $notification) {
            $user_notifications = DB::table('user_notifications')->where('notification_uid',$notification->uid)->where('user_uid',$input['user_uid'])->count();
            $notification->count = $user_notifications;
        }
        }
        return $notifications;
    }

    public static function add_user_notification($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'notification_uid'  => 'required',
            'user_uid'   => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }
        
        $notification = DB::table('notifications')->select('notification')->where('uid',$input['notification_uid'])->first();
        $user_logs = DB::table('user_logs')->select('device_uid')->where('user_uid',$input['user_uid'])->orderBy('created_at','desc')->first();
        $messages = ['device_uid'=>$user_logs->device_uid,'message'=>$notification->notification];
        // echo '<pre>'; print_r($messages); exit();
        $data = array(
            'notification_uid'  => $input['notification_uid'],
            'user_uid'          => $input['user_uid'],
            'uid'               => static::gen_uid([]),
            );

        $query = DB::table('user_notifications')->insert($data);
        $status['status'] = 'success';
        $status['message'] = 'User notification added successfully';
        $result['result'] = $status;
        $result['response'] = $messages;
        return $result;

    }

    public static function save_push_notification($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'category_uid'   => 'required',
            'notification'  => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $data = array(
            'uid'           => static::gen_uid([]),
            'category_uid'  => $input['category_uid'],
            'notification'  => $input['notification'],
            );
        $query = DB::table('notifications')->insert($data);
        $status['status'] = 'success';
        $status['message'] = 'Notification inserted successfully';
        $result['result'] = $status;
        return $result;

    }

    public static function get_dashboard($input){
        
        $visit_types  = static::get_dictionaries(['table_name'=>'visit_types']);
        $participants = static::get_participants(['eligible'=>'eligible']);
        // echo '<pre>'; print_r($participants); exit();
        if(!empty($visit_types)){
        foreach ($visit_types as $key=>$visit_type) {
            $my_array[$visit_type]=0;
            if(!empty($participants)){
            foreach ($participants as $key => $participant) {
                if(!empty($participant->visit_type) and $participant->visit_type==$visit_type){
                    $my_array[$visit_type] +=1;
                }
            }
            }
        }
        }
        return $my_array;
    }

    public static function get_search_results($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'search_type'   => 'required',
            'search_value'  => 'required',
            'page'          => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        // $participants = static::get_participants([$input['search_type'] => $input['search_value'],'page'=>$input['page']]);
        $query = DB::table('users')->whereNull('deleted_at')
                        ->select('users.*','user_roles.name as user_role')
                        ->leftjoin('user_roles','user_roles.uid','=','users.role_id');
        if($input['search_type']=='participant_name')   $users = $query->where('first_name',$input['search_value']);
        if($input['search_type']=='role_id')            $users = $query->where('role_id',$input['search_value']);
        // echo $query->toSql(); echo '<pre>'; print_r($users); echo '</pre>'; exit();
        $users = $query->get();
        if(count($users)>0){
            $status['status']  = 'success';
            $result['result']  = $status;
            $result['response']  = $users;
        }else{
            $status['status']  = 'error';
            $status['message'] = 'Results not available';
            $result['result']  = $status;
        }
        return $result;

    }

    public static function get_participants($input){

        $query = DB::table('users')->whereNull('deleted_at')
                        ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
                        // ->where('user_roles.name','LHV')
                        ->select('users.*','user_roles.name as user_role');
        $users = $query->get();
        // echo $query->toSql(); echo '<pre>'; print_r($input); echo '</pre>'; exit();
        // echo '<pre>';print_r($users); echo '</pre>';exit();
        return $users;
    }

    public static function forgot_password($input){

        // Validate the Input Fields
        $rules = array(
            'email_address' => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        $user = DB::table('users')->whereNull('deleted_at')
          ->where('email_address', $input['email_address'])
          ->select('id','email_address','plain_password','first_name','last_name')
          ->first();
          // echo '<pre>';print_r($user); exit();

        if(!empty($user->email_address))
        {
            // $code = static::gen_salt();
            $code_date = date('Y-m-d H:i:s');
            $code = sha1($user->id.time());
            $data = array('code' => $code,'code_date'=>$code_date);
            DB::table('users')->where('email_address', $input['email_address'])->update($data);

            //send code via email_address
            $status['status']   = 'success';
            $result['result']   = $status;
            $result['response'] = array(
                'toName' => $user->first_name.' '.$user->last_name,
                'toEmail' => $user->email_address,
                'code' => $code,
                'plain_password' => $user->plain_password,
            );

            $baseURL = 'http://'.$_SERVER['SERVER_NAME'].Config::get('constants.PATH');
            $link = $baseURL.'reset-password?email='.$user->email_address.'&code='.$code;

            $parameters = array(
                'before_days'   => 1,
                'subject'       => 'Reset you password',
                'due_date'      => date('Y-m-d H:i:s'),
                'recipients'    => $user->email_address,
                'notification'  => 'Dear '.$user->first_name.' '.$user->last_name.'
                                    <br>Your password is '.$user->plain_password.'
                                    <br>You can change your password here: <a target="_blank" href='.$link.'>Click here</a> to reset your password.',
                );
            $save_notification = static::save_notification($parameters);
            $status['status']   = 'success';
            $status['message']  = $parameters['notification']; // 'Link to change your password has been sent to your email address, check your email .';
            $result['result']   = $status;
        }else{
            $status['status']   = 'error';
            $status['message']  = 'Email Address does not exist';
            $result['result']   = $status;
        }
        return $result;
    }

    public static function save_notification($input){
        if(!isset($_SESSION)) session_start();

        $debug = FALSE;
        // echo '<pre>';print_r($input);echo '</pre>';
        if(!empty($input['notification_id'])){
            $query = DB::table('notifications');
            $query->whereNull('deleted');
            $query->where('id',$input['notification_id']);
            if($debug == TRUE) echo '<br>'.$query->toSql();
            $notifications = $query->get(array('id'));

            // $input['updated_by'] = $_SESSION['userid'];
            $input['updated_date']  = date('Y-m-d H:i:s');
            $notification_id = $input['notification_id'];
            unset($input['notification_id']);

            // echo '<pre>'; print_r($input); echo '</pre>';
            DB::table('notifications')->where('id', '=', $notification_id)->update($input);
            $status['status']   = 'success';
            $status['message']  = 'Link to change your password has been sent to your email address, you will soon receive it.';
            $result['result']   = $status;

        }else{
            // $input['created_by'] = $_SESSION['userid'];
            $input['created_date']  = date('Y-m-d H:i:s');
            unset($input['notification_id']);
            // echo '<pre>'; print_r($input); echo '</pre>';
            DB::table('notifications')->insertGetId($input);
            $status['status']   = 'error';
            $status['message']  = 'Notification not sent';
            $result['result']   = $status;

        }
        return $result;
    }

    public static function reset_password($input){
        // Validate the Input Fields
        $rules = array(
            'code'              => 'required',
            'email_address'     => 'required',
            'password'          => 'required',
            'confirm_password'  => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }
        elseif ($input['password'] != $input['confirm_password'])
        {
            $status['status'] = 'error';
            $status['message'] = 'New password and confirm password does not match';
            $result['result'] = $status;
            return $result;
        }

        $user = DB::table('users')->whereNull('deleted_at')
            ->where('email_address', $input['email_address'])
            ->where('code', $input['code'])
            ->first();

        if(!empty($user->code))
        {
            $user->code_date;
            $salt = static::gen_salt();
            $data = array(
                'plain_password'=>$input['password'],
                'password' => sha1($input['password'].$salt).":".$salt,
                'code'=>NULL,
            );
            DB::table('users')->where('email_address', $input['email_address'])->update($data);
            $status['status']   = 'success';
            $status['message']  = 'Password has been reset successfully';
            $result['result']   = $status;
        }else{
            $status['status']   = 'error';
            $status['message']  = 'User code is incorrect';
            $result['result']   = $status;
        }
        return $result;
    }

    public static function change_password($input)
    {

        // Validate the Input Fields
        $rules = array(
            'user_id'           => 'required',
            'password'          => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {   
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        $user = DB::table('users')->whereNull('deleted_at')
          ->where('uid', $input['user_id'])
          ->first();

        if(!empty($user->uid))
        {
            // echo '<pre>'; print_r($user); exit();
            $user_id = $input['user_id'];
            unset($input['user_id']); 

            if(!empty($input['password'])){
                $salt = static::gen_salt();
                $input['password'] = sha1($input['password'].$salt).":".$salt;
            }
            DB::table('users')->where('uid',$user_id)->update($input);
            
            $status['status'] = 'success';
            $status['message'] = 'User update successfully';
            $result['result'] = $status;
            
        }else{
            $status['status']   = 'error';
            $status['message']  = 'User does not exists';
            $result['result']   = $status;
        }
        return $result;
    }

    public static function get_dictionaries($input){
        $query = DB::table($input['table_name']);
        if(!empty($input['name'])) $query->where('name',$input['name']);
        if(!empty($input['code'])) $query->whereIn('code',$input['code']);
        if(!empty($input['question_id'])) $query->whereIn('question_id',$input['question_id']);
        // echo '<pre>'; print_r($input); echo '</pre>'; echo '<br>'.$query->toSql();
        $records = $query->get();
        $my_records = [];
        if(!empty($records)){
            foreach ($records as $record) {
                if($input['table_name']=='visit_types'){
                    $my_records[$record->id] = $record->name;                    
                }elseif($input['table_name']=='questions'){
                    $my_records[$record->id] = $record->qtype;
                }elseif($input['table_name']=='question_answers'){
                    $my_records[$record->id] = $record->answer;
                }elseif($input['table_name']=='question_subcategories_subparts'){
                    $my_records[$record->id] = $record->subpart_roman;                    
                }elseif($input['table_name']=='question_subcategories_subparts_sublabel'){
                    $my_records[$record->id] = $record->sublabel_roman;                    
                }else{
                    $my_records[$record->uid] = $record->name;
                }
            }
        }
        return $my_records;
    }

    public static function poppulate_users($input){

        $members = DB::table('members')->select('first_name','last_name','user_role_uid','phone_number')->distinct()->orderBy('first_name')->orderBy('last_name')->get();
        // echo DB::table('members')->select('first_name','last_name','phone_number')->distinct()->orderBy('first_name')->orderBy('last_name')->toSql();
        if($members->count()>0){
        foreach ($members as $member) {
            // echo '<pre>'; print_r($member); echo '</pre>';
            $data['uid'] = static::gen_uid([]);
            $data['role_id'] = $member->user_role_uid;
            $data['first_name'] = $member->first_name;
            $data['last_name'] = $member->last_name;
            $data['username'] = str_replace(' ','',strtolower($member->first_name)).'.'.str_replace(' ','',strtolower($member->last_name));
            $data['password_plain'] = str_replace(' ','',strtolower($member->first_name)).'12';
            $data['phone_number'] = $member->phone_number;
            $data['created_by'] = 'd28qcx5gaauux13mcqdcn9kdq0';
            // echo '<pre>'; print_r($data); echo '</pre>';
            $users = DB::table('users')->whereNull('deleted_at')->where('username',$data['username'])->get();
            if($users->count()>0)
                 $data['uid'] = $users[0]->uid;
            else DB::table('users')->insertGetId($data);
            DB::table('members')
                ->where('first_name',$member->first_name)
                ->where('last_name',$member->last_name)
                ->update(['user_uid'=>$data['uid']]);
            unset($data);
        }
        static::generate_passwords();
        }
    }

    public static function download_registrations($input){
        // echo '<br>1...';
        ini_set('memory_limit', '-1');
        set_time_limit(5000);
        $questions = DB::table('questions')
            ->select('question_categories.name as category','questions.code','questions.question')
            ->leftjoin('question_categories','question_categories.uid','=','questions.category_id')
            ->orderBy('question_categories.id')
            ->orderBy('serial')
            ->get();
        // $users = DB::table('users')->whereNull('deleted_at')->select('uid','first_name','last_name')->get();
        // $userx=[];
        // if(!empty($users)){
        //     foreach ($users as $user) {
        //         $userx[$user->uid] = $user->first_name.' '.$user->last_name;            
        //     }
        // }
        if($questions->count()>0){
            foreach ($questions as $question) {
                $fields[$question->category][$question->code] = $question->question;
            }
        }
        // dd($fields);
        $records = ''; $title = ''; $count = 1;
        $table = $input['table_name'];
        switch($table){
            case "registeration_corrected":
                $table = "registration";
            break;
            default:
                $table = $input['table_name'];
        }
        $app_registrations = DB::table($input['table_name'])
            ->select([$input['table_name'].".*", DB::raw("COALESCE(users.username, '-') as added_by")])
            ->leftJoin("users", "users.uid", "=", $input['table_name'].".added_by")
            ->get();
        // dd($app_registrations->count());
        // dd(DB::table($input['table_name'])
        // ->select([$input['table_name'].".*", DB::raw("COALESCE(users.username, '-') as added_by")])
        // ->leftJoin("users", "users.uid", "=", $input['table_name'].".added_by")
        // ->toSql());
        // return;
        if($app_registrations->count()>0){
            $title .= '<tr>';
            foreach ($app_registrations as $register_user) {
                $records .= '<tr>'; 
                // echo '<br>3...'; exit();
                if(!empty($register_user->metadata)){
                    $metadata = json_decode($register_user->metadata,true);
                    $metadata['entered_on'] = date('Y-m-d h:i:s A',strtotime($register_user->entered_on));
                    if(!empty($metadata)){
                        foreach ($metadata as $key => $value) {
                            // echo '<br>'.$key.': '.$value; continue;
                            // if($key=='added_by')    $metadata[$key] = (!empty($userx[$value]))?$userx[$value]:'-';
                            if($value=='null')  $metadata[$key] = '-';
                            elseif($value=='-2')    $metadata[$key] = '-';
                            elseif($value=='-1')    $metadata[$key] = '-';
                            elseif($value=='0')     $metadata[$key] = 'No';
                            elseif($value=='1')     $metadata[$key] = 'Yes';
                            elseif($value=='none')  $metadata[$key] = '-';   
                        }
                    }
                    // dd($metadata);
                    if(!empty($fields)){
                        // dd($fields[$table]);
                        foreach ($fields[$table] as $key => $value) {
                            if($count == 1){
                                $title  .= '<td>'.$value.'</td>';
                            }
                            try {
                                if(!empty($metadata[$key])) $records .= '<td>'.$metadata[$key].'</td>';
                                else $records .= '<td>-</td>';
                            } catch (Exception $e) {
                                $records .= '<td>--exception--</td>';
                            }                    
                        }
                    }
                }else{
                    $records .= '<td> - </td>';
                }
                $count+=1;
            $records .= '</tr>'; 
            }
            $title .= '</tr>';
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Registrations not available';
            $result['result'] = $status;
            return $result;
        }
        $status['status']   = 'success';
        $result['result']   = $status;
        $result['title'] = $title;
        $result['data'] = $records;
        return $result;
    }


    public static function download_helpline_comm_data($input){

        // Validate the Input Fields
        $rules = array(
            'user_id'       => 'required',
            'type'          => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        switch ($input['type']) {
            case 'COMM_RegisterForm': $table_name = 'registration'; break;
            case 'COMM_AssessmentForm': $table_name = 'assessment'; break;
            case 'COMM_FollowupForm': $table_name = 'followup'; break;
            case 'COMM_SampleCollectionForm': $table_name = 'sample_collection'; break;
            case 'COMM_TestResultForm': $table_name = 'test_result'; break;
            default: $table_name = 'registration'; break;
        }

        $user = DB::table('users')->whereNull('deleted_at')
            ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
            ->where('user_roles.name','Helpline')
            ->where('users.uid',$input['user_id'])
            ->select('users.*')
            ->first();
        // echo '<br> user uid: '.$user->uid;exit();
                // echo '<pre>'; print_r($user); echo '</pre>'; exit();
            $count=0;
        if(!empty($user))
        {
            // echo '<br>1...';
            // echo '<br>village_id: '.$user->village_id.'<br>';
            $query = DB::table('users')->whereNull('deleted_at')->select('users.*')
                    ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
                    ->where('user_roles.name','Community Member')
                    ->select('users.*');
                    // echo $query->toSql().'<br>'; exit(); 
            $community_members = $query->get();
            if(!empty($community_members->count()>0)){
            // echo '<br>2a...';
            foreach ($community_members as $community_member) {
                // echo '<pre>'; print_r($community_member); echo '</pre>';
                // echo '<br>table_name'.$table_name;
                $data_set[$community_member->uid] = DB::table($table_name)->where('added_by',$community_member->uid)->get();
                $count += $data_set[$community_member->uid]->count();
                if($data_set[$community_member->uid]->count()==0){
                    unset($data_set[$community_member->uid]);
                }
            }
            }else{
                // echo '<br>2b...';
                $status['status'] = 'error';
                $status['message'] = 'Community Member not available';
                $result['result'] = $status;
                return $result;
            }
        }else{
            // echo '<br>1b...';
            $status['status'] = 'error';
            $status['message'] = 'Helpline not available';
            $result['result'] = $status;
            return $result;
        }
        // echo '<br>6...';
        if($count>0){
            $status['status']   = 'success';
            $status['length']   = $count;
            $result['result']   = $status;
            $result['response'] = $data_set;
            return $result;
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Data not available';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function download_helpline_lhv_data($input){

        // Validate the Input Fields
        $rules = array(
            'user_id'       => 'required',
            'type'          => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        switch ($input['type']) {
            case 'LHV_RegisterForm': $table_name = 'registration'; break;
            case 'LHV_AssessmentForm': $table_name = 'assessment'; break;
            case 'LHV_FollowupForm': $table_name = 'followup'; break;
            case 'LHV_SampleCollectionForm': $table_name = 'sample_collection'; break;
            case 'LHV_TestResultForm': $table_name = 'test_result'; break;
            default: $table_name = 'registration'; break;
        }

        $user = DB::table('users')->whereNull('deleted_at')
            ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
            ->where('user_roles.name','Helpline')
            ->where('users.uid',$input['user_id'])
            ->select('users.*')
            ->first();
        // echo '<br> user uid: '.$user->uid;exit();
                // echo '<pre>'; print_r($user); echo '</pre>'; exit();
            $count=0;
        if(!empty($user))
        {
            // echo '<br>1...';
            // echo '<br>village_id: '.$user->village_id.'<br>';
            $query = DB::table('users')->whereNull('deleted_at')->select('users.*')
                    ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
                    ->where('user_roles.name','LHV')
                    ->select('users.*');
                    // echo $query->toSql().'<br>'; exit(); 
            $community_members = $query->get();
            if(!empty($community_members->count()>0)){
            // echo '<br>2a...';
            foreach ($community_members as $community_member) {
                // echo '<pre>'; print_r($community_member); echo '</pre>';
                // echo '<br>table_name'.$table_name;
                $data_set[$community_member->uid] = DB::table($table_name)->where('added_by',$community_member->uid)->get();
                $count += $data_set[$community_member->uid]->count();
                // echo '<br>'.DB::table($table_name)->where('added_by',$community_member->uid)->toSql();
                // echo '<br>count'.$data_set[$community_member->uid]->count();
                // echo '<pre>'; print_r($data_set);exit();
                if($data_set[$community_member->uid]->count()==0){
                    unset($data_set[$community_member->uid]);
                }
            }
            }else{
                // echo '<br>2b...';
                $status['status'] = 'error';
                $status['message'] = 'Community Member not available';
                $result['result'] = $status;
                return $result;
            }
        }else{
            // echo '<br>1b...';
            $status['status'] = 'error';
            $status['message'] = 'Helpline not available';
            $result['result'] = $status;
            return $result;
        }
        // echo '<br>6...';
        if($count>0){
            $status['status']   = 'success';
            $status['length']   = $count;
            $result['result']   = $status;
            $result['response'] = $data_set;
            return $result;
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Data not available';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function download_comm_lhv_data($input){

        // Validate the Input Fields
        $rules = array(
            'user_id'    => 'required',
            'type'       => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        switch ($input['type']) {
            case 'LHV_RegisterForm': $table_name = 'registration'; break;
            case 'LHV_AssessmentForm': $table_name = 'assessment'; break;
            case 'LHV_FollowupForm': $table_name = 'followup'; break;
            case 'LHV_SampleCollectionForm': $table_name = 'sample_collection'; break;
            case 'LHV_TestResultForm': $table_name = 'test_result'; break;
            default: $table_name = 'registration'; break;
        }

        $user = DB::table('users')->whereNull('deleted_at')
            ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
            ->where('user_roles.name','Community Member')
            ->where('users.uid',$input['user_id'])
            ->select('users.*')
            ->first();
            // echo '<pre>'; print_r($user); echo '</pre>'; exit();
            
        if(!empty($user))
        {
            // echo '<br>1...';exit();
            $query = DB::table('assessment')->select('member_uid')
                    ->orderBy('added_on','desc')
                    ->where('metadata->sp_assign_community_uid',$user->uid);
            $assessments = $query->get();
            $count=0;
            // echo '<br>count: '.$assessments->count();
            // echo '<br>user_id: '.$user->uid.'<br>'.$query->toSql(); exit();
            if($assessments->count()>0){
            foreach ($assessments as $assessment) {
                // echo '<pre>'; print_r($assessments); echo '</pre>'; exit();
                $data_set[$assessment->member_uid]  = DB::table($table_name)->where('member_uid',$assessment->member_uid)->get();
                $count += $data_set[$assessment->member_uid]->count();
                // echo '<pre>';print_r($data_set); exit();
                if($data_set[$assessment->member_uid]->count()==0){
                    unset($data_set[$assessment->member_uid]);
                }
            }
            }else{
                $status['status'] = 'error';
                $status['message'] = 'Assessments not available';
                $result['result'] = $status;
                return $result;
            }
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Community Member not available';
            $result['result'] = $status;
            return $result;
        }
        if($count>0){
            $status['status']   = 'success';
            $status['length']   = $count;
            $result['result']   = $status;
            $result['response'] = $data_set;
            return $result;
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Data not available';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function download_lhv_comm_data($input){

        // Validate the Input Fields
        $rules = array(
            'user_id'       => 'required',
            'type'          => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        switch ($input['type']) {
            case 'COMM_RegisterForm': $table_name = 'registration'; break;
            case 'COMM_AssessmentForm': $table_name = 'assessment'; break;
            case 'COMM_FollowupForm': $table_name = 'followup'; break;
            case 'COMM_SampleCollectionForm': $table_name = 'sample_collection'; break;
            case 'COMM_TestResultForm': $table_name = 'test_result'; break;
            default: $table_name = 'registration'; break;
        }

        $user = DB::table('users')->whereNull('deleted_at')->select('users.*')
            ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
            ->where('user_roles.name','LHV')
            ->where('users.uid',$input['user_id'])
            ->first();
        // echo '<br> user uid: '.$user->uid;
        if(!empty($user))
        {
            $count=0;
            // echo '<br>1...';
            $query = DB::table('users')->whereNull('deleted_at')->select('users.*')
                    ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
                    ->where('user_roles.name','Community Member')
                    ->where('village_id',$user->village_id);

            $community_members = $query->get();
            if($community_members->count()>0){
            // echo '<br>2a...';
            foreach ($community_members as $community_member) {
                // echo '<pre>'; print_r($community_member); echo '</pre>';
                $data_set[$community_member->uid] = DB::table($table_name)->where('added_by',$community_member->uid)->get();
                $count += $data_set[$community_member->uid]->count();
                if($data_set[$community_member->uid]->count()==0){
                    unset($data_set[$community_member->uid]);
                }
            }
            }else{
                // echo '<br>2b...';
                $status['status'] = 'error';
                $status['message'] = 'Community Member not available';
                $result['result'] = $status;
                return $result;
            }
        }else{
            // echo '<br>1b...';
            $status['status'] = 'error';
            $status['message'] = 'LHV not available';
            $result['result'] = $status;
            return $result;
        }
        // echo '<br>6...';
        if($count>0){
            $status['status']   = 'success';
            $status['length']   = $count;
            $result['result']   = $status;
            $result['response'] = $data_set;
            return $result;
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Data not available';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function download_lhv_data($input){

        // Validate the Input Fields
        $rules = array(
            'user_id'    => 'required',
            // 'type'          => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        $user = DB::table('users')->whereNull('deleted_at')
            ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
            ->where('user_roles.name','LHV')
            ->where('users.uid',$input['user_id'])
            ->first();
        // echo '<br> user uid: '.$user->uid;
        if(!empty($user))
        {
            // echo '<br>1...';
            $query = DB::table('assessment')
                ->orderBy('added_on','desc')
                ->select('member_uid')
                ->where('metadata->sp_assign_community_uid',$user->uid);
            $assessment = $query->first();
            // echo '<pre>'; print_r($assessment); echo '</pre>';
            if(!empty($assessment)){
                // echo '<br>2...';
                $app_registrations = DB::table('registration')->where('member_uid',$assessment->member_uid)->get();
                if($app_registrations->count()>0){
                // echo '<br>3...';
                foreach ($app_registrations as $app_registration) {
                    $assessments  = DB::table('assessment')->where('member_uid',$assessment->member_uid)->get();
                    if(!empty($assessments)){  $app_registration->assessments  = $assessments;
                    }else{  $app_registration->assessments = 'Assessments not available';
                    } 

                    $followups  = DB::table('followup')->where('member_uid',$assessment->member_uid)->get();
                    if($followups->count()>0){  $app_registration->followups  = $followups;
                    }else{  $app_registration->followups = 'Followups not available';
                    }

                    $sample_collections  = DB::table('sample_collection')->where('member_uid',$assessment->member_uid)->get();
                    if($sample_collections->count()>0){  $app_registration->sample_collections  = $sample_collections;
                    }else{  $app_registration->sample_collections = 'Sample Collections not available';
                    }

                    $test_results  = DB::table('test_result')->where('member_uid',$app_registration->member_uid)->get();
                    if($test_results->count()>0){  $app_registration->test_results  = $test_results;
                    }else{  $app_registration->test_results = 'Test Results not available';
                    }
                }
                }else{
                    $status['status'] = 'error';
                    $status['message'] = 'Registrations not available';
                    $result['result'] = $status;
                    return $result;
                }                    
            
            }else{
                $status['status'] = 'error';
                $status['message'] = 'Assessments not available';
                $result['result'] = $status;
                return $result;
            }
        }else{
            $status['status'] = 'error';
            $status['message'] = 'LHV not available';
            $result['result'] = $status;
            return $result;
        }
        $status['status']   = 'success';
        $result['result']   = $status;
        $result['response'] = $app_registrations;
        return $result;
    }

    public static function authenticate_token($input){

        // Validate the Input Fields
        $rules = array(
            'token_uid' => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        // 'select users.*,user_roles.`name` as `role` 
        // from user_logs 
        // left join users on users.device_uid=user_logs.device_uid
        // left join user_roles on user_roles.uid=users.role_id
        // where user_logs.uid=\'89c51938ebe2bca8650dbf4001b64234\'';

            $query = DB::table('user_logs');
            # fix for iphone testing
            // if($input['token_uid']=='hello1234') 
            //      $query->leftjoin('users','users.uid','=','user_logs.user_uid');
            // else 
            $query->leftjoin('users','users.device_uid','=','user_logs.device_uid');
            $query->leftjoin('user_roles','user_roles.uid','=','users.role_id')
                    ->whereNotNull('users.uid')
                    ->where('user_logs.uid',$input['token_uid'])
                    ->select('users.*','user_roles.name as role')
                    ->orderBy('user_logs.id','desc');
          $user = $query->first();
          // echo '<pre>'; print_r($input); echo '</pre>';
          // echo $query->toSql();
          // echo '<pre>'; print_r($user); exit();
        if(!empty($user))
        {
            unset($user->password);
            foreach ($user as $userx) {
                $visit = DB::table('visits')
                        ->leftjoin('visit_types','visit_types.uid','=','visits.visit_type_uid')
                        ->where('user_uid',$user->uid)
                        ->select('visits.uid')
                        ->orderBy('visit_types.id','desc')
                        ->first(); 
                if(!empty($visit)){
                    $user->visit_uid = $visit->uid;
                }
            }
            $status['status']   = 'success';
            $result['result']   = $status;
            $result['response'] = $user;
            DB::table('users')->where('uid',$user->uid)->update(['device_uid'=>null]);
            return $result;
        }else{
            $status['status'] = 'error';
            $status['message'] = 'Your session has expired, kindly login again';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function authenticate_userx($input){

        if(!isset($_SESSION)) session_start();
        // if(!empty($input['api_transaction_id']))
        // $status['api_transaction_id'] = $input['api_transaction_id'];

        // Validate the Input Fields
        $rules = array(
            // 'device_uid'   => 'required',
            'username'     => 'required',
            'password'     => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        $user = DB::table('users')
          ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
          ->where('username', $input['username'])
          ->select('users.*','user_roles.name as role')
          ->first();

        if(!empty($user))
        { 
            if(!empty($user->password)){
                $authenticated = static::check_password($input['password'], $user->password);
                unset($user->plain_password);
            }else{
                // if($input['password']==$user->plain_password){
                //     $salt = static::gen_salt();
                //     $password = sha1($user->plain_password.$salt).":".$salt;
                //     DB::table('users')->where('id','=',$user->id)->update(array('password'=>$password));
                //     $authenticated = true; 
                //     unset($user->plain_password);
                // }else{
                    $status['status'] = 'error';
                    $status['message'] = 'Incorrect Password!';
                    $result['result'] = $status;
                    return $result;
                // }
            }
            
            // print('password $authenticated: '.$authenticated); exit();
            // if($user->mark_deleted == 1){
            //     $status['status'] = 'error';
            //     $status['message'] = 'User does not exists';
            //     $result['result'] = $status;
            //     return $result;
            // }else
            // if($user->disabled == 1){
            //     $status['status'] = 'error';
            //     $status['message'] = 'User does not exists';
            //     $result['result'] = $status;
            //     return $result;
            // }else
            if($authenticated !== true){
                $status['status'] = 'error';
                $status['message'] = 'Incorrect Password!';
                $result['result'] = $status;
                return $result;
            }elseif($authenticated === true){
                unset($user->password);
                $token_uid = static::gen_uid(['chars'=>4]); 
                // echo '<br>'.strlen($token_uid);
                // $token_uid   = 'hello1234';
                $ip_address  = static::getIpAddress();
                // $device_uid  = (!empty($input['device_uid']))?$input['device_uid']:null;
                // if(!empty($input['device_uid'])){
                    // DB::table('user_logs')->insert(['uid'=>$token_uid,'user_uid'=>$user->uid,'ip_address'=>$ip_address,'device_uid'=>$device_uid]);
                    // DB::table('users')->where('uid',$user->uid)->update(['device_uid'=>$device_uid]);
                // }

                $status['status']   = 'success';
                $result['result']   = $status;
                if(!empty($input['device_uid'])) $result['response'] = ['tokenuid'=>$token_uid]; //
                else $result['response'] = $user;
                return $result;
            }
        }
        else
        {   //user does not exist
            $status['status'] = 'error';
            $status['message'] = 'User does not exists'; //'User authentication failed!';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function authenticate_user($input){

        if(!isset($_SESSION)) session_start();
        // if(!empty($input['api_transaction_id']))
        // $status['api_transaction_id'] = $input['api_transaction_id'];

        // Validate the Input Fields
        $rules = array(
            // 'device_uid'   => 'required',
            'username'     => 'required',
            'password'     => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        $user = DB::table('users')
          ->leftjoin('user_roles','user_roles.uid','=','users.role_id')
          ->where('username', $input['username'])
          ->select('users.*','user_roles.name as role')
          ->first();
        if(!empty($user))
        { 
            if(!empty($user->password)){
                $authenticated = static::check_password($input['password'], $user->password);
                unset($user->plain_password);
            }else{
                // if($input['password']==$user->plain_password){
                //     $salt = static::gen_salt();
                //     $password = sha1($user->plain_password.$salt).":".$salt;
                //     DB::table('users')->where('id','=',$user->id)->update(array('password'=>$password));
                //     $authenticated = true; 
                //     unset($user->plain_password);
                // }else{
                    $status['status'] = 'error';
                    $status['message'] = 'Incorrect Password!';
                    $result['result'] = $status;
                    return $result;
                // }
            }
            
            // print('password $authenticated: '.$authenticated); exit();
            // if($user->mark_deleted == 1){
            //     $status['status'] = 'error';
            //     $status['message'] = 'User does not exists';
            //     $result['result'] = $status;
            //     return $result;
            // }else
            // if($user->disabled == 1){
            //     $status['status'] = 'error';
            //     $status['message'] = 'User does not exists';
            //     $result['result'] = $status;
            //     return $result;
            // }else
            if(!empty($user->deleted_at)){
                $status['status'] = 'error';
                $status['message'] = 'User does not exists';
                $result['result'] = $status;
                return $result;
            }elseif($authenticated !== true){
                $status['status'] = 'error';
                $status['message'] = 'Incorrect Password!';
                $result['result'] = $status;
                return $result;
            }elseif($authenticated === true){
                unset($user->password);
                $token_uid = static::gen_uid([]);
                $ip_address  = static::getIpAddress();

                // $response['token_id'] = $token_uid;
                $response['user']           = $user;
                $villages = DB::table('user_villages')
                ->leftjoin('villages','villages.uid','=','user_villages.village_id')
                ->where('user_id',$user->uid)->first();
                $response['users']          = DB::table('users')->distinct()
                                                ->select('users.*')
                                                ->leftjoin('user_villages','user_villages.user_id','=','users.uid')
                                                ->leftjoin('villages','villages.uid','=','user_villages.village_id')
                                                ->where('region_id',$villages->region_id)
                                                ->get();
                $response['user_villages']  = DB::table('user_villages')
                                                ->select('user_villages.*')
                                                ->leftjoin('villages','villages.uid','=','user_villages.village_id')
                                                ->where('region_id',$villages->region_id)
                                                ->get();
                $response['user_roles']     = DB::table('user_roles')->get();
                $response['professions']    = DB::table('professions')->get();
                $response['diagnostic_test'] = DB::table('diagnostic_test')->get();
                $response['test_result_dropdown'] = DB::table('test_result_dropdown')->get();
                $response['test_reason'] = DB::table('test_reason')->get();
                $response['sample_type'] = DB::table('sample_type')->get();
                $response['countries']      = DB::table('countries')->where('uid',$villages->country_id)->get();
                $response['provinces']      = DB::table('provinces')->where('uid',$villages->province_id)->get();
                $response['regions']        = DB::table('regions')->where('uid',$villages->region_id)->get();
                $response['districts']      = DB::table('districts')->where('region_id',$villages->region_id)->get();
                $response['tehsils']        = DB::table('tehsils')->where('region_id',$villages->region_id)->get();
                $response['union_councils'] = DB::table('union_councils')->where('region_id',$villages->region_id)->get();
                $response['villages']       = DB::table('villages')->where('region_id',$villages->region_id)->get();
                $response['facilities']     = DB::table('facilities')->where('region_id',$villages->region_id)->get();
                $response['referral_sites'] = DB::table('referral_sites')
                                                ->select('referral_sites_facilities.uid','referral_sites.village_id','referral_sites_facilities.name')
                                                ->leftjoin('referral_sites_facilities','referral_sites_facilities.uid','=','referral_sites.referral_id')
                                                ->leftjoin('villages','villages.uid','=','referral_sites.village_id')
                                                ->where('region_id',$villages->region_id)
                                                ->get();
                $response['sample_collection_sites']   = DB::table('sample_collection_sites')
                                                ->select('sample_collection_facilities.uid','village_id','sample_collection_facilities.name','sample_collection_types.name as type')
                                                ->leftjoin('sample_collection_facilities','sample_collection_facilities.uid','=','sample_collection_sites.facility_id')
                                                ->leftjoin('sample_collection_types','sample_collection_types.uid','=','sample_collection_facilities.type_id')
                                                ->leftjoin('villages','villages.uid','=','sample_collection_sites.village_id')
                                                ->where('region_id',$villages->region_id)
                                                ->get();
                $response['testing_sites']  = DB::table('testing_sites')
                                                ->select('testing_sites_facilities.uid','testing_sites.village_id','testing_sites_facilities.name')
                                                ->leftjoin('testing_sites_facilities','testing_sites_facilities.uid','=','testing_sites.testing_sites_id')
                                                ->leftjoin('villages','villages.uid','=','testing_sites.village_id')
                                                ->where('region_id',$villages->region_id)
                                                ->get();
                $response['sample_testing_facilities'] = DB::table('sample_testing_facilities')->get();
                // echo '<pre>'; print_r($response['testing_sites']); exit();
                $status['status']   = 'success';
                $result['result']   = $status;
                $result['response'] = $response;
                return $result;
            }
        }
        else
        {   //user does not exist
            $status['status'] = 'error';
            $status['message'] = 'User does not exists'; //'User authentication failed!';
            $result['result'] = $status;
            return $result;
        }
    }

    public static function verify_username($input){

        if(!isset($_SESSION)) session_start();
        if(!empty($input['api_transaction_id']))
        $status['api_transaction_id'] = $input['api_transaction_id'];
        // echo 'inside: <pre>'; print_r($input); echo '</pre>'; //exit();

        // Validate the Input Fields
        $rules = array(
            'username'     => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
        }

        $users = DB::table('users')->whereNull('deleted_at')
          ->where('username', $input['username'])
          ->select('users.id','username','first_name','last_name','email_address')
          ->get();

          if(!empty($users)){

            $status['status'] = 'error';
            $status['message'] = 'Username already exists';
            $result['result'] = $status;
          }else{
            $status['status']   = 'success';
            $result['result']   = $status;
            $result['response'] = $users;
          }
        return $result;
    }

    public static function registered_user($input){

        if(!isset($_SESSION)) session_start();
        $query = DB::table('users')->whereNull('deleted_at');
        $query->where('users.uid',$input['user_id']);
        $user = $query->first();

          if(!empty($user)){
            $status['status']   = 'success';
            $result['result']   = $status;
            $result['response'] = $user;
          }else{
            //user does not exist
            $status['status'] = 'error';
            $status['message'][] = 'User does not exist'; 
            $result['result'] = $status;
          }
        return $result;
    }

    public static function get_biochemical($input){
        return true;
        // $baseline=4;$endline=3;return [["label"=>'Baseline',"value"=>$baseline],["label"=>'Endline',"value" =>$endline]];
        
        // echo '<pre>'; print_r($my_variables); echo '</pre>';
        # get question ids
        # get user answers
        # get question id for E1 and E1c
        # check if (
        # question id E1 is not empty and question id E2 is not empty and question id E3 is not empty and question id E4 is not empty and question id E5 is not empty and question id E6 is not empty and question id E7 is not empty and question id E8 is not empty and question id E9 is not empty and question id E10 is not empty and question id E11 is not empty and question id E12 is not empty 
        # and E1c is empty and E2c is empty and E3c is empty and E4c is empty and E5c is empty and E6c is empty and E7c is empty and E8c is empty and E9c is empty and E10c is empty and E11c is empty and E12c is empty
        # ) then baseline+=1;

        # echo '<br>get question ids';
        $codes = ['E1','E2','E3','E4','E5','E6','E7','E8','E9','E10','E11','E12','E13'];
        $should_not_be_empty =  static::get_dictionaries(['table_name'=>'questions','code'=>$codes]);
        $codes = ['E1c','E2c','E3c','E4c','E5c','E6e','E7c','E8c','E9c','E10c','E11c','E12c','E13c'];
        $should_be_empty =  static::get_dictionaries(['table_name'=>'questions','code'=>$codes]);
        $should_not_be_empty = array_keys($should_not_be_empty); $should_be_empty = array_keys($should_be_empty);
        // print_r($should_not_be_empty); print_r($should_be_empty);

        $baseline=0;$endline=0;
        #echo '<br>fetch visit types';
        $my_basics  = static::get_dictionaries(['table_name'=>'visit_types']);unset($my_basics[2]);unset($my_basics[3]);unset($my_basics[4]);
        #echo '<br>fetch users';
        $users = static::get_participants(['eligible'=>'eligible']);
        if(!empty($my_basics)){
        foreach ($my_basics as $visit_type_id => $my_basic) {
            $score = 0;
            if(!empty($users)){
            foreach ($users as $key => $user) {
                // echo '<br>'.$user->first_name.' '.$user->last_name;
                if(!empty($user->visits[$visit_type_id]) and !empty($user->visits[$visit_type_id]->questionnaire_completed_at)){
                    // echo '<br>fetch users\' '.$my_basic.' visits';
                    // echo '<br>'.$user->visits[$visit_type_id]->query_string;
                    $query_string = json_decode($user->visits[$visit_type_id]->query_string,true);
                    $score=1; //initially set score to one
                    if(!empty($should_not_be_empty)){
                    foreach ($should_not_be_empty as $value) {
                    if(!empty($query_string[$value])){
                        // echo '<br>value:'.$value.'| score:'.
                        $score=0; //if condition is reversed, unset score to zero
                    }
                    // else echo '<br>if: '.$value.' is empty, should_not_be_empty';
                    }
                    }
                    if(!empty($should_be_empty)){
                    foreach ($should_be_empty as $value) {
                    if(empty($query_string[$value])){
                        // echo '<br>value:'.$value.'| score:'.
                        $score=0; //if condition is reversed, unset score to zero
                    }
                    // else echo '<br>and if: '.$value.' is not empty, should_be_empty | '.$query_string[$value];
                    }
                    }
                    if($visit_type_id<1) $baseline=$score; else $endline=$score;
                }
            }
            }
        }
        }

        // $baseline=2;  $endline=0;
        $my_value   = ['baseline'=>$baseline,'4th Follow up'=>$endline];
        $labels     = ['Baseline'=>'baseline','Endline'=>'4th Follow up'];
        $data=[];
        if(!empty($labels)){
        foreach ($labels as $key => $label) {
            $data[] = ["label"=>$key,"value"=>$my_value[$label]];
        }
        }
        // echo '<pre>'; print_r($data); echo '</pre>'; exit();
        return $data;
    }

    public static function save_profile($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'username' => 'required',
            'role_id' => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $query = DB::table('users')->whereNull('deleted_at');
        if(!empty($input['user_id'])) $query->where('uid',$input['user_id']);
        else $query->where('username', $input['username']);
        $user = $query->select('id')->first(); unset($input['username']);
        // if($debug = TRUE) echo '<br>'.$query->toSql(); //exit();
        // echo '<pre>'; print_r($input); echo '</pre>'; exit();
        if(!empty($user)){
            unset($input['user_id']); unset($input['username']);
            $input['updated_at'] = date('Y-m-d H:i:s');

            if(!empty($input['password'])){
                $salt = static::gen_salt();
                $input['password'] = sha1($input['password'].$salt).":".$salt;
            }
            DB::table('users')->where('id',$user->id)->update($input);
            
            $status['status'] = 'success';
            $status['message'] = 'User update successfully';
            $result['result'] = $status;
        }else{
            // user already exists
            $status['status'] = 'error';
            $status['message'] = 'User does not exists';
            $result['result'] = $status;
        }
        return $result;

    }

    public static function get_profile($input){

        $privileges = array('ADMIN','LHW','VACCINATOR','DATA COLLECTOR','LHS','CMW','CHW');
        $access_levels = array('ADMIN','COUNTRY','PROVINCE','DISTRICT','TEHSIL','UNION COUNCIL');
        $query = DB::table('users')->whereNull('deleted_at');
        if(!empty($input['username'])) $query->where('username',$input['username']);
        // echo $query->toSql(); echo '<br>username: '.$input['username'];
        // exit();
        return $users = $query->first();
        /*
            'users.uid',
            'users.disabled',
            'users.username',
            'user_countries.name as country',
            'user_provinces.name as province',
            'user_districts.name as district',
            'user_ucs.name as union_council'
        */
    }

    public static function save_test_result($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'member_uid'    => 'required',
            'record_date'   => 'required',
            'type'          => 'required',
            'added_by'      => 'required',
            'added_on'      => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $input['uid']   = static::gen_uid([]);
        // echo '<pre>'; print_r($input); echo '</pre>'; exit();
        DB::table('test_result')->insert($input);

        $status['status']  = 'success';
        $status['message'] = 'Test Result added successfully';
        $result['result']  = $status;

        return $result;
    }

    public static function save_followup($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'member_uid'    => 'required',
            'record_date'   => 'required',
            'type'          => 'required',
            'added_by'      => 'required',
            'added_on'      => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $input['uid']   = static::gen_uid([]);
        // echo '<pre>'; print_r($input); echo '</pre>'; exit();
        DB::table('followup')->insert($input);

        $status['status']  = 'success';
        $status['message'] = 'Followup added successfully';
        $result['result']  = $status;

        return $result;
    }

    public static function save_sample_collection($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'member_uid'    => 'required',
            'record_date'   => 'required',
            'type'          => 'required',
            'added_by'      => 'required',
            'added_on'      => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $input['uid']   = static::gen_uid([]);
        // echo '<pre>'; print_r($input); echo '</pre>'; exit();
        DB::table('sample_collection')->insert($input);

        $status['status']  = 'success';
        $status['message'] = 'Sample Collection added successfully';
        $result['result']  = $status;

        return $result;
    }

    public static function save_assessment($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'member_uid'    => 'required',
            'record_date'   => 'required',
            'type'          => 'required',
            'added_by'      => 'required',
            'added_on'      => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $input['uid']   = static::gen_uid([]);
        DB::table('assessment')->insert($input);

        $status['status']  = 'success';
        $status['message'] = 'Assessment added successfully';
        $result['result']  = $status;

        return $result;
    }

    public static function register_participant($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'member_uid'    => 'required',
            'record_date'   => 'required',
            'type'          => 'required',
            'added_on'      => 'required',
            'added_by'      => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $input['uid']   = static::gen_uid([]);
        DB::table('registration')->insert($input);
        $status['status'] = 'success';
        $status['message'] = 'Registration complete';
        $result['result'] = $status;
        return $result;
    }

    public static function generate_uid($input){

        $records = DB::table($input['table_name'])->get();
        if(!empty($records)){
        foreach ($records as $record) {
            DB::table($input['table_name'])->where('id',$record->id)->whereNull('uid')->update(['uid'=>static::gen_uid([])]);
        }
        }
    }

    public static function change_status($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'user_uid' => 'required',
            'checked'   => 'required',
            'type'      => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        // echo '<pre>'; print_r($input); echo '</pre>'; exit();
        $user = DB::table('users')->where('uid',$input['user_uid'])->first();
        $date = date('Y-m-d H:i:s');
        if(!empty($user)){
            if($input['type']=='users'){
                if($input['checked']=='true') $data = ['approved_at'=>$date];
                else $data = ['approved_at'=>null];
                DB::table('users')->where('uid',$input['user_uid'])->update($data);
            }
        }
        $status['status']  = 'success';
        $status['message'] = 'User updated successfully';
        $result['result']  = $status;
        return $result;
    }

    public static function register_user($input){

        if(!isset($_SESSION)) session_start();
        // Validate the Input Fields
        $rules = array(
            'username' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status']   = 'error';
            $status['message']  = $validator->messages()->all();
            $result['result']   = $status;
            return $result;
        }

        $query = DB::table('users');
        if(!empty($input['user_id'])) $query->where('uid',$input['user_id']);
        else $query->where('username', $input['username']);
        $users = $query->select('id')->first();
        // echo '<pre>'; print_r($users); echo '</pre>'; 
        // echo '<br>'.$query->toSql(); exit();
        if(empty($users)){
            $input['uid'] = static::gen_uid([]);
            unset($input['user_id']);
            if(!empty($input['password'])){
                $salt = static::gen_salt();
                $input['password_plain'] = $input['password'];
                $input['password'] = sha1($input['password'].$salt).":".$salt;
            }
            $input['uid'] = static::gen_uid([]);
            $input['created_at']  = date('Y-m-d H:i:s');
            $input['created_by'] = $_SESSION['uid'];
            DB::table('users')->insertGetId($input);

            $status['status'] = 'success';
            $status['message'] = 'User added successfully';
            $result['result'] = $status;
        }else{

            if(!empty($input['user_id'])){
                $user_id = $input['user_id']; 
                unset($input['user_id']); unset($input['username']);
                $input['updated_at']  = date('Y-m-d H:i:s');

                if(!empty($input['password'])){
                    $salt = static::gen_salt();
                    $input['password_plain'] = $input['password'];
                    $input['password'] = sha1($input['password'].$salt).":".$salt;
                }
                DB::table('users')->where('uid',$user_id)->update($input);
                
                $status['status'] = 'success';
                $status['message'] = 'User update successfully';
                $result['result'] = $status;
            }else{
                // user already exists
                $status['status'] = 'error';
                $status['message'] = 'User already exists';
                $result['result'] = $status;
            }
        }
        return $result;

    }

}
