<?php

namespace App\Http\Middleware;

use Closure;

class GovernmentPageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/signin');
        $user = \DB::table("u01_users")->where("username", "=", $_SESSION["username"])->first();
        if($user === null) return redirect('/logout');
        if($user->privilege !== 3) return redirect('/dashboard');
        $limitations = json_decode($user->metadata, true);
        $details = [];
        $details['privilege'] = $user->privilege;
        $details['access_level'] = $user->access_level;
        $details['show_epi'] = $limitations["epi"];
        $details['show_mnch'] = $limitations["mnch"];
        $request->attributes->add($details);
        return $next($request);
    }
}
