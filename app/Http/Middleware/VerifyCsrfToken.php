<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/api-login',
        '/corona-login',
        '/corona-register',
        '/corona-assessment',
        '/corona-test-result',
        '/corona-followup',
        '/corona-sample-collection',
        '/corona-download-lhv-data',
        '/corona-download-lhv-comm-data',
        '/corona-download-comm-lhv-data',
        '/corona-download-helpline-lhv-data',
        '/corona-download-helpline-comm-data',
    ];
}
