<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Session;

use App\Invoice;
use App\Models\Corona;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;

class InvoicesPerMonthSheet implements FromView, WithTitle
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function view(): View
    {
        try {
            $register_user = corona::download_registrations(['table_name'=>$this->name]);            
            return view('export')->with('data',$register_user['data'])->with('title',$register_user['title']);
        } catch (Exception $e) {
            echo 'exception';
        }
    }

    public function title(): string
    {
        return str_replace('_',' ',$this->name);
    }
}
