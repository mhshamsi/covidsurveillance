<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Session;

// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReportExport implements WithMultipleSheets {

  protected $names = [
    'registration', 
    'assessment', 
    'followup',
    'sample_collection',
    'test_result',
    // 'outcome_status'
  ];

  public function __construct() {}

  public function sheets(): array {
    $sheets = [];
    foreach ($this->names as $name) {
      $sheets[] = new ReportSheet($name);
    }
    return $sheets;
  }
}
