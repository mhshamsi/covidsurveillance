<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Corona;
use App\Charts\SampleChart;
use Session;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use FCM;

class CoronaController extends Controller
{

	public function getIndex(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        else return redirect('/report');
	}

    public function getSignin($token_uid){
        
        if(!isset($_SESSION)) session_start();
        $result = corona::authenticate_token(['token_uid'=>$token_uid]);
        // echo '<pre>'; print_r($result); exit();
        if($result['result']['status'] ==  'success'){
            $_SESSION['token_uid']     = $token_uid;
            $_SESSION['uid']           = $result['response']->uid;
            $_SESSION['role_id']      = $result['response']->role_id;
            $_SESSION['role']          = $result['response']->role;
            $_SESSION['username']      = $result['response']->username;
            $_SESSION['first_name']    = $result['response']->first_name;
            $_SESSION['last_name']     = $result['response']->last_name;
            if($result['response']->role == 'Participant') return redirect('/user-food-diary');
            else if($result['response']->role == 'Admin')  return redirect('/users');
        }else return json_encode($result);
    }

    public function getLogout(){

        if(!isset($_SESSION)) session_start();
        session_destroy();
        return redirect('/login');
        if($_SESSION['token_uid'] == 'hello123'){
            echo 'successfully logged out...';
        }else{
            if(!empty($_SESSION['token_uid'])){
                $login = '/signin/'.$_SESSION['token_uid'];
            }
            else $login = '/login';
        }
    }

    public function getLogin(){

        if(!isset($_SESSION)) session_start();
        if(!empty($_SESSION['username'])) return redirect('/users');
        return view('login');
    }

    public function getApiLogin(Request $request,$device_uid,$username,$password){

        if(!isset($_SESSION)) session_start();
        // $session_id = session_id();
        // if(empty($request->post('device_uid'))){
        //     echo '{"result":{"status":"error","message":["The device uid field is required."]}}';
        //     exit();
        // }
        // echo '<pre>';print_r($username); exit();
        $parameters =  array(
          'device_uid'  => $device_uid,
          'username'    => $username,
          'password'    => $password,
        );

        // echo '<pre>';print_r($parameters); exit();
        // if(!empty($request->post('device_uid')))
        $result = corona::authenticate_userx($parameters);
        return json_encode($result);
    }

    public function postApiLogin(Request $request){

        if(!isset($_SESSION)) session_start();
        // $session_id = session_id();
        // if(empty($request->post('device_uid'))){
        //     echo '{"result":{"status":"error","message":["The device uid field is required."]}}';
        //     exit();
        // }
        $parameters =  array(
          'username'    => $request->post('username'),
          'password'    => $request->post('password'),
        );

        $result = corona::authenticate_user($parameters);
        return json_encode($result);
    }

    public function postAjaxLogin(Request $request){

        if(!isset($_SESSION)) session_start();
        // $session_id = session_id();
        $parameters =  array(
          'username'    => $request->post('username'),
          'password'    => $request->post('password'),
        );

        $result = corona::authenticate_user($parameters);
        // echo '<pre>';print_r($result); exit();
        if($result['result']['status'] ==  'success'){
            $_SESSION['uid']          = $result['response']['user']->uid;
            $_SESSION['role_id']      = $result['result']['role_id'] = $result['response']['user']->role_id;
            $_SESSION['role']         = $result['result']['role'] = $result['response']['user']->role;
            $_SESSION['username']     = $result['response']['user']->username;
            $_SESSION['first_name']   = $result['response']['user']->first_name;
            $_SESSION['last_name']    = $result['response']['user']->last_name;
            // if(!empty($result['response']->visit_uid)) $_SESSION['visit_uid'] = $result['response']->visit_uid;
        }
        // echo '<pre>'; print_r($_SESSION); exit();
        return json_encode($result);
    }

    public function getDashboard($token_uid=''){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');

      // $eligibles    = corona::get_participants(['eligible'=>'eligible']);
      // $biochemical  = corona::get_biochemical([]);
      // print_r($biochemical); exit();

      // $dashboard    = corona::get_dashboard([]);
      // if (!empty($dashboard)) {
      //   $arrData = [];
      //   $participation_data = array("chart" => array("caption" => "","showValues" => "0","theme" => "fusion"));
      //   foreach ($dashboard as $key=>$value) {
      //      $participation_data["data"][] = ["label" => $key,"value" => $value];
      //   }
      //   $participation = json_encode($participation_data);

      //   $biochemical_data = array("chart" => array("caption" => "","showValues" => "0","theme" => "fusion"));
      //   $biochemical_data["data"] = $biochemical;
      //   $biochemical = json_encode($biochemical_data);
      // }
        return view('dashboard');
            // ->with('participation',$participation)
            // ->with('biochemical',$biochemical)
            // ->with('eligibles',$eligibles);
    }

    public function getIneligibleParticipants(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $users = corona::get_participants(['eligible'=>'ineligible']);
        $visit_types = corona::get_dictionaries(['table_name'=>'visit_types']);
        // echo '<pre>';print_r($users); exit();
        return view('ineligible-participants')->with('users',$users)->with('visit_types',$visit_types);
    }

    public function getParticipants(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $users = corona::get_participants(['eligible'=>'eligible']);
        // $visit_types = corona::get_dictionaries(['table_name'=>'visit_types']);
        // echo '<pre>';print_r($users); exit();
        return view('participants')->with('users',$users);
    }

    public function postAjaxChangeStatus(Request $request){

        $parameters = array(
            'user_uid' => $request->post('user_uid'),
            'checked' => $request->post('checked'),
            'type' => $request->post('type'),
            // 'starting_date' => $request->post('starting_date'),
        );
        $change_status = corona::change_status($parameters);
        return json_encode($change_status);
    }

    public function getPoppulateUsers(Request $request){

        $users = corona::poppulate_users([]);
        echo 'users inserted successfully';
    }

    public function getReport(Request $request){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');

        $register_user = corona::download_registrations(['table_name'=>'registration']);
        return view('report')
            ->with('data',(!empty($register_user['data']))?$register_user['data']:'')
            ->with('title',(!empty($register_user['title']))?$register_user['title']:'');
    }

    public function getExport(Request $request){
        error_reporting(0);
        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        return Excel::download(new ReportExport, 'Report '.date('mdY').'-'.date('Hms').'.xlsx');
    }

    public function getUsers(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');

        $users          = corona::get_participants(['eligible'=>'eligible']);
        $user_roles    = corona::get_dictionaries(['table_name'=>'user_roles']);
        // echo '<pre>'; print_r($users); exit();
        return view('users')->with('users',$users)->with('user_roles',$user_roles);
    }

    public function getUser($user_uid=''){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $user = array(); 
        if(!empty($user_uid)){
         $user = corona::get_participants(['user_uid'=>$user_uid]);
         $user = $user[0];
        }
        $user_roles    = corona::get_dictionaries(['table_name'=>'user_roles']);
        // echo '<pre>'; print_r($user_roles); echo '</pre>'; exit();
        return view('user')->with('user',$user)->with('user_roles',$user_roles);
        }

    public function getUserAdvice(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');

        $user_uid = $_SESSION['uid'];
        $personal_notifications = corona::get_personal_notifications(['user_uid'=>$user_uid]);
        return view('user-advice')->with('personal_notifications',$personal_notifications);
    } 

    public function getPushNotification(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $push_notifications = corona::get_push_notifications([]);
        $notification_categories =  corona::get_dictionaries(array('table_name'=>'notification_categories'));
        return view('push-notification')
                ->with('push_notifications',$push_notifications)
                ->with('notification_categories',$notification_categories);
    }

    public function getUserPushNotification($user_uid){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $push_notifications = corona::get_user_notifications(['user_uid'=>$user_uid]);
        $notification_categories =  corona::get_dictionaries(array('table_name'=>'notification_categories'));
        $users = corona::get_participants(['user_uid'=>$user_uid]);
        // echo '<pre>'; print_r($notification_categories->uid); exit();
        return view('user-push-notification')->with('push_notifications',$push_notifications)
                ->with('notification_categories',$notification_categories)
                ->with('users',$users)
                ->with('user_uid',$user_uid);
    }
  
    public function postAjaxGetNotification(Request $request){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');        
        $parameters = array(
            'category_uid' => $request->post('category_uid'),
        );
        $push_notifications = corona::get_push_notifications($parameters);
        // $push_notifications = corona::get_user_notifications($parameters);
        if($push_notifications->count()>0){
        // echo '<pre>'; print_r($notification_categories); echo '</pre>'; exit();
        $serial=0; echo '                            ';
        foreach ($push_notifications as $push_notification) {
            echo '<tr>
                    <td style="color: #333333">'.(++$serial).'</td>
                    <td colspan="2" style="color: #333333">'.$push_notification->notification.'</td>
                </tr>';
            }
        }else{
            echo '<tr>
            <td colspan="4">Push notifications are not available.</td>
        </tr>';
        }
    }

    public function postAjaxGetUserNotification(Request $request){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        
        $parameters = array(
          'category_uid' => $request->post('category_uid'),
          'user_uid' => $request->post('user_uid'),
          );
        $push_notifications = corona::get_user_notifications($parameters);
        if($push_notifications->count()>0){
        // echo '<pre>'; print_r($notification_categories); echo '</pre>'; exit();
        $serial=0; echo '                            ';
        foreach ($push_notifications as $push_notification) {
            echo '<tr>
            <td style="color: #333333">'.(++$serial).'</td>
            <td style="color: #333333">'.$push_notification->notification.'</td>
            <td><span class="btn btn-success">'.$push_notification->count.'</span></td>
            <td><button type="button" onclick="send_notification(\''.$push_notification->uid.'\',\''.$request->post('user_uid').'\')" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Send</button></td>
        </tr>
        ';
                }}else{
            echo '<tr>
            <td colspan="4">Push notifications are not available.</td>
        </tr>';
            } 
    }

    public function postAjaxAddUserNotification(Request $request){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $parameters = array(
            'notification_uid' => $request->post('notification_uid'),
            'user_uid' => $request->post('user_uid'),
        );
        $add_user_notification = corona::add_user_notification($parameters);
        $device_uid = $add_user_notification['response']['device_uid'];
        $message    = $add_user_notification['response']['message'];
        // echo '<pre>'; print_r($add_user_notification); exit();

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(0);
        $notificationBuilder = new PayloadNotificationBuilder('Covid Surveillance');
        $notificationBuilder->setBody($message)->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $device_uid;   //$device_uid
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        $downstreamResponse->tokensToDelete();  //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToModify();  //return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToRetry();   //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensWithError(); //return Array (key:token, value:error) - in production you should remove from your database the tokens
        // echo '<pre>'; print_r($downstreamResponse); exit();
        return $add_user_notification;
    }

    public function getAddNotification(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        $notification_categories = corona::get_dictionaries(array('table_name'=>'notification_categories'));
        // echo '<pre>'; print_r($notification_categories);exit();
        return view('add-notification')->with('notification_categories',$notification_categories);
        }  

        public function postAjaxAddNotification(Request $request){

        $parameters = array(
            'category_uid'=> $request->post('category_uid'),
            'notification'=> $request->post('notification'),
          );
        return corona::save_push_notification($parameters);
    }

    public function getAdminProfile(){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/login');
        // echo '<pre>';print_r($_SESSION); echo '</pre>';
        $profile      = corona::get_profile(['username'=>$_SESSION['username']]);
        $user_roles   = corona::get_dictionaries(array('table_name'=>'user_roles'));
        // echo '<pre>';print_r($user_roles); exit();
        return view('admin-profile')->with('user_roles',$user_roles)->with('profile',$profile);
    }

    public function postAdminProfile(Request $request){

        $parameters =  array(
          'first_name'    => $request->post('first_name'),
          'last_name'     => $request->post('last_name'),
          'email_address' => $request->post('email_address'),
          'username'      => $request->post('username'),
          'role_id'      => $request->post('role_id'),
        );

        $result = corona::save_profile($parameters);
        // return redirect()->route()->with('message', 'Post was created!');
        echo json_encode($result);

    }

    public function getUserProfile($visit_uid=''){

        if(!isset($_SESSION)) session_start();
        if(empty($_SESSION['username'])) return redirect('/users');
        if($_SESSION['role']=='Admin' and empty($visit_uid)) return redirect('/users');

        $remove_questions = []; $show_questions = []; $query_answers = []; $question_answers=[];
        $user_uid = $visit_uid; unset($visit_uid);
        $question_answers =  corona::get_dictionaries(array('table_name'=>'question_answers'));
        if(!empty($visit_uid)){
            $users = corona::get_participants(['visit_uid'=>$visit_uid]);
            $query_answers = json_decode($users[0]->query_answers,true);
        }else{
            if(!empty($user_uid)){
                $users = corona::get_participants(['user_uid'=>$user_uid]);
                $query_answers = json_decode($users[0]->query_string,true);
            }else{
                $users = corona::get_participants(['user_uid'=>$_SESSION['uid']]);                
            }
        }
        // echo '<pre>'; print_r($query_answers); exit();
        $categories = corona::get_questions([]);
        // echo '<pre>'; print_r($categories); exit();
        return view('user-profile')
          ->with('categories',$categories)
          ->with('users',$users)
          ->with('visit_uuid',$user_uid)
          ->with('query_answers',$query_answers)
          ->with('question_answers',$question_answers);
    }

    public function postApiDownloadHelplineCommData(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'user_id'       => $request->post('user_id'),
            'type'          => $request->post('type'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::download_helpline_comm_data($parameters);
        return json_encode($register_user);
    }

    public function postApiDownloadHelplineLhvData(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'user_id'       => $request->post('user_id'),
            'type'          => $request->post('type'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::download_helpline_lhv_data($parameters);
        return json_encode($register_user);
    }

    public function postApiDownloadCommLhvData(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'user_id'    => $request->post('user_id'),
            'type'       => $request->post('type'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::download_comm_lhv_data($parameters);
        return json_encode($register_user);
    }

    public function postApiDownloadLhvCommData(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'user_id'    => $request->post('user_id'),
            'type'       => $request->post('type'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::download_lhv_comm_data($parameters);
        return json_encode($register_user);
    }

    public function postApiDownloadLhvData(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'user_id'    => $request->post('user_id'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::download_lhv_data($parameters);
        return json_encode($register_user);
    }

    public function postApiSynchronize(Request $request){

        // echo '<pre>'; print_r($request); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'member_uid'    => $request->post('member_uid'),
            'record_date'   => $request->post('record_date'),
            'type'          => $request->post('type'),
            'metadata'      => $request->post('metadata'),
            'added_by'      => $request->post('added_by'),
            'added_on'      => $request->post('added_on'),
            'entered_on'    => date('Y-m-d H:i:s'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::save_assessment($parameters);
        return json_encode($register_user);
    }

    public function postApiTestResult(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'member_uid'    => $request->post('member_uid'),
            'record_date'   => $request->post('record_date'),
            'type'          => $request->post('type'),
            'metadata'      => $request->post('metadata'),
            'added_by'      => $request->post('added_by'),
            'added_on'      => $request->post('added_on'),
            'entered_on'    => date('Y-m-d H:i:s'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::save_test_result($parameters);
        return json_encode($register_user);
    }

    public function postApiSampleCollection(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'member_uid'    => $request->post('member_uid'),
            'record_date'   => $request->post('record_date'),
            'type'          => $request->post('type'),
            'metadata'      => $request->post('metadata'),
            'added_by'      => $request->post('added_by'),
            'added_on'      => $request->post('added_on'),
            'entered_on'    => date('Y-m-d H:i:s'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::save_sample_collection($parameters);
        return json_encode($register_user);
    }

    public function postApiFollowup(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'member_uid'    => $request->post('member_uid'),
            'record_date'   => $request->post('record_date'),
            'type'          => $request->post('type'),
            'metadata'      => $request->post('metadata'),
            'added_by'      => $request->post('added_by'),
            'added_on'      => $request->post('added_on'),
            'entered_on'    => date('Y-m-d H:i:s'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::save_followup($parameters);
        return json_encode($register_user);
    }

    public function postApiAssessment(Request $request){

        // echo '<pre>'; print_r($questions); echo '</pre>';exit();
        date_default_timezone_set('Asia/Karachi');
        $parameters = array(
            'member_uid'    => $request->post('member_uid'),
            'record_date'   => $request->post('record_date'),
            'type'          => $request->post('type'),
            'metadata'      => $request->post('metadata'),
            'added_by'      => $request->post('added_by'),
            'added_on'      => $request->post('added_on'),
            'entered_on'    => date('Y-m-d H:i:s'),
        );
        // echo '<pre>';print_r($parameters); exit();
        $register_user = corona::save_assessment($parameters);
        return json_encode($register_user);
    }

    public function postApiRegister(Request $request){

        // echo '<pre>'; print_r($request); echo '</pre>';exit();
        $parameters = array(
            'member_uid'    => $request->post('member_uid'),
            'record_date'   => $request->post('record_date'),
            'type'          => $request->post('type'),
            'metadata'      => $request->post('metadata'),
            'added_by'      => $request->post('added_by'),
            'added_on'      => $request->post('added_on'),
            'entered_on'    => date('Y-m-d H:i:s'),
        );
        $register_user = corona::register_participant($parameters);
        // echo '<pre>';print_r($register_user); exit();
        return json_encode($register_user);
    }

    public function postAjaxRegister(Request $request){

        $parameters = array(
            'first_name'        => $request->post('first_name'),
            'last_name'         => $request->post('last_name'),
            'email_address'     => $request->post('email_address'),
            'username'          => $request->post('username'),
            'password'          => $request->post('password'),
            'role_id'           => $request->post('role_id'),
            // 'confirm_password'  => $request->post('confirm_password'),
            // 'query_string'      => json_encode($questions),
        );
        // echo '<pre>'; print_r($parameters); echo '</pre>';exit();
        $register_user = corona::register_user($parameters);
        // echo '<pre>';print_r($register_user); exit();
        return json_encode($register_user);
    }

    public function postAjaxSearchIneligibleParticipants(Request $request){

        // $users = corona::get_participants([$request->post('search_type') => $request->post('search_value')]);
        $parameters = array(
            'search_type'   => $request->post('search_type'),
            'search_value'  => $request->post('search_value'),
            'page'          => $request->post('page'),
            );
        $results = corona::get_search_results($parameters);
        $visit_types = corona::get_dictionaries(['table_name'=>'visit_types']);

        // echo '<pre>'; print_r($results); echo '</pre>';exit();
        if($results['result']['status']=='success'){
            $users = $results['response'];
            if(!empty($users)){
                $my_users = ''; $serial=0;
                foreach ($users as $key => $user) {
                    $my_users .= '<tr>
                            <td style="color: #333333">'.(++$serial).'</td>
                            <td style="color: #333333">'.$user->first_name.' '.$user->last_name.'</td>
                        </tr>';
                            // <td style="color: #333333"><a href="/user-profile/'.$user->uid.'">Profile</a></td>
                }
            $results['response'] = $my_users;
            }
        }
        return $results;
    }

    public function postAjaxSearchParticipants(Request $request){

        $parameters = array(
            'search_type'   => $request->post('search_type'),
            'search_value'  => $request->post('search_value'),
            'page'          => $request->post('page'),
            );
        $results = corona::get_search_results($parameters);
        $visit_types = corona::get_dictionaries(['table_name'=>'visit_types']);

        // echo '<pre>'; print_r($results); echo '</pre>';exit();
        if($results['result']['status']=='success'){
            $users = $results['response'];
            if(!empty($users)){
                $my_users = ''; $serial=0;
                foreach ($users as $key => $user) {
                    $my_users .= '<tr>
                            <td style="color: #333333">'.(++$serial).'</td>
                            <td style="color: #333333">'.$user->first_name.' '.$user->last_name.'</td>
                            <td>
                                <div class="progress" style="height: 23px">';
                                if(!empty($visit_types)){
                                    $my_progress='';
                                    foreach ($visit_types as $key=>$visit_type) {
                                        $my_progress .= '<div class="progress-bar bg-'.(($key < ($user->visit_type_id))?'success':'danger').'" role="progressbar" style="width:20%">'.$visit_type.'</div>';
                                    }
                                }
                    $my_users .= $my_progress.'
                                </div>
                            </td>
                            <td>
                                <div class="dropdown text-center">
                                    <a href="#" class="card-drop" data-toggle="dropdown" aria-expanded="true"><i class="mdi mdi-dots-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <!-- <a href="/user-profile/'.$user->uid.'" class="dropdown-item">Profile</a> -->
                                        <a href="/user-visits/'.$user->uid.'" class="dropdown-item">Visits</a>
                                        <a href="/user-push-notification/'.$user->uid.'" class="dropdown-item">Push Notifications</a>
                                        <!-- <a href="javascript:void(0);" class="dropdown-item">Export</a> -->
                                    </div>
                                </div>
                            </td>
                        </tr>';
                }
            $results['response'] = $my_users;
            }
        }
        return $results;
    }

    public function postAjaxSearchControlPanel(Request $request){

        // $users = corona::get_participants([$request->post('search_type') => $request->post('search_value')]);
        $parameters = array(
            'search_type'   => $request->post('search_type'),
            'search_value'  => $request->post('search_value'),
            'page'          => $request->post('page'),
            );
        // echo '<pre>'; print_r($parameters); echo '</pre>';exit();
        // $users          = corona::get_participants(['eligible'=>'eligible']);
        $results = corona::get_search_results($parameters);
        // echo '<pre>'; print_r($results); echo '</pre>';exit();
        if($results['result']['status']=='success'){
            $users = $results['response'];
            if(!empty($users)){
                $my_users = ''; $serial=0;
                foreach ($users as $key => $user) {
                    $my_users .= '<tr>
                                    <td style="color: #333333">1</td>
                                    <td style="color: #333333"><a href="user/'.$user->uid.'">'.$user->first_name.' '.$user->last_name.'</a></td>
                                    <td style="color: #333333">'.$user->user_role.'</td>
                                    <td>
                                        <input type="checkbox" name="users_'.$user->uid.'" id="'.$user->uid.'" onclick="change_status( this.id,this.checked,\'users\',\'\')"> 
                                        <label for="" style="position: relative; top: -7px; left: 7px;">Approve</label>
                                    </td>
                                </tr>';


                }
            $results['response'] = $my_users;
            }
        }
        return $results;
    }

    public function getGenerateUsernames(){

        if(!isset($_SESSION)) session_start();
        // if($_SESSION['role']!='Admin' and empty($visit_uid)) return redirect('/logout');
        corona::generate_usernames();
        echo 'successful...';
    }

    public function getGeneratePassword(){

        if(!isset($_SESSION)) session_start();
        // if($_SESSION['role']!='Admin' and empty($visit_uid)) return redirect('/logout');
        corona::generate_passwords();
        echo 'successful...';
    }

    public function getGenerateUid($table_name){

        set_time_limit(0);
        if(!isset($_SESSION)) session_start();
        // if($_SESSION['role']!='Admin' and empty($visit_uid)) return redirect('/logout');
        if(!empty($table_name)) corona::generate_uid(['table_name'=>$table_name]);
        echo 'success';
    }

}
