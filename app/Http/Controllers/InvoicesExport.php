<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Session;

use App\Invoice;
use App\Models\Corona;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;


class InvoicesExport implements WithMultipleSheets
{
    use Exportable;
    protected $names;

    public function __construct()
    {
        $this->names = array('registeration_corrected'/* ,'assessment','followup','sample_collection','test_result','outcome_status' */);
        // ,'followup_home_isolation'
    }

    public function sheets(): array
    {
        $sheets = [];
        foreach ($this->names as $name) $sheets[] = new InvoicesPerMonthSheet($name);
        return $sheets;
    }
}
