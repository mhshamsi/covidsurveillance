<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Session;

use App\Models\ReportQueries;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;

class ReportSheet implements FromView, WithTitle
{
  private $name;

  public function __construct($name) {
    $this->name = $name;
  }

  public function view(): View {
    try {
      $details = ReportQueries::report($this->name);
      return view('report-export')->with('data',$details['data'])->with('titles',$details['title']);
    } catch (Exception $e) {
      echo 'exception';
    }
  }

  public function title(): string {
    return str_replace('_',' ',$this->name);
  }
}
