<?php
  if(!function_exists("vaccine_report_get_data")){
    function vaccine_report_get_data(array $data, string $vaccine_id, int $inside_uc, bool $outreach){
      foreach ($data as $report) {
        $report = (array)$report;
        if(
          $report['vaccine_id'] == $vaccine_id && 
          ($inside_uc !== -1 ? $report['inside_uc'] == $inside_uc : 1) &&
          $report['outreach'] == $outreach
        ) return $report['count'];
      }
      return 0;
    }
  }

  if(!function_exists("vaccines_by_village")){
    function vaccines_by_village(array $data, string $column, int $village_id = null, string $vaccine_id, int $inside_uc = null, int $outreach = null){
      $count = 0;
      foreach($data as $row ){
        $row = (array) $row;
        if($row["village_id"] === $village_id){
          if($row["vaccine_id"] === $vaccine_id){
            if($row["inside_uc"] === $inside_uc){
              if($row["outreach"] === $outreach){
                $count += $row[$column];
              }elseif($outreach === null){
                $count += $row[$column];
              }
            }
          }
        }elseif($village_id === null){
          if($row["vaccine_id"] === $vaccine_id){
            if($row["inside_uc"] === $inside_uc){
              if($row["outreach"] === $outreach){
                $count += $row[$column];
              }elseif($outreach === null){
                $count += $row[$column];
              }
            }elseif($inside_uc === null){
              if($row["outreach"] === $outreach){
                $count += $row[$column];
              }elseif($outreach === null){
                $count += $row[$column];
              }
            }
          }
        }
      }
      // dd($count);
      return $count;
    }
  }

  if(!function_exists("report_vaccinated_by")){
    function report_vaccinated_by(array $data, string $column, array $vaccinated_by){
      $count = 0;
      foreach($data as $row ){
        $row = (array) $row;
        // dd($row);
        if(in_array($row["privilege"], $vaccinated_by)){
          $count += $row[$column];
        }
      }
      return $count;
    }
  }

  if(!function_exists("convert_for_report")){
    function convert_for_report($value, $name = "") : string {
      $skippable = ["member_uid", "lat", "lng", "sampleId","age"];
      if(in_array($name, $skippable)) return $value === null ? "-" : $value;
      switch ($value) {
        case null:
        case 'null':
        case '-2':
        case '-1':
        case 'none':
          return "-";
        case '0':
          return "No";
        case '1':
          return "Yes";
        default:
          return $value;
      }
    }
  }
?>