@extends('layout.default_teeko')
@section('content')

                                <div class="panel-heading">
                                    <h4>Error 404 - Not Found</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <p>Page could not be found.</p>
                                        </div>
                                    </div><!-- form-group -->
                                </div><!-- panel-body -->
                                <div class="panel-footer">

                                </div><!-- panel-footer -->

@stop



