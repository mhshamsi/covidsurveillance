@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Score</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li> -->
                        <!-- <li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page">Score</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->

<script type="text/javascript" src="{{ asset('fusioncharts/js/fusioncharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('fusioncharts/js/fusioncharts.charts.js') }}"></script>
<script type="text/javascript" src="{{ asset('fusioncharts/js/themes/fusioncharts.theme.fint.js') }}"></script>
<?php 
$qualitativeChart = new FusionCharts("angulargauge", "ChartQualitative" , "100%", 200, "chart-qualitative", "json",' {
        chart: {
          caption: "Qualitative Analysis",
          lowerlimit: "0",
          upperlimit: "6",
          showvalue: "1",
          numbersuffix: "",
          theme: "fusion",
          showtooltip: "0"
        },
        colorrange: {
          color: [
            {
              minvalue: "0",
              maxvalue: "1",
              code: "#62B58F"
            },
            {
              minvalue: "1",
              maxvalue: "2",
              code: "#62B58F"
            },
            {
              minvalue: "2",
              maxvalue: "3",
              code: "#FFC533"
            },
            {
              minvalue: "3",
              maxvalue: "4",
              code: "#FFC533"
            },

            {
              minvalue: "4",
              maxvalue: "5",
              code: "#F2726F"
            },
            {
              minvalue: "5",
              maxvalue: "6",
              code: "#F2726F"
            },
          ]
        },
        dials: {
          dial: [
            {
              value: "'.$dsr_qualitative.'"
            }
          ]
        }
    }');

$quantitativeChart = new FusionCharts("angulargauge", "ChartQuantitative" , "100%", 200, "chart-quantitative", "json",' {
        chart: {
          caption: "Quantitative Analysis",
          lowerlimit: "0",
          upperlimit: "6",
          showvalue: "1",
          numbersuffix: "",
          theme: "fusion",
          showtooltip: "0"
        },
        colorrange: {
          color: [
            {
              minvalue: "0",
              maxvalue: "1",
              code: "#62B58F"
            },
            {
              minvalue: "1",
              maxvalue: "2",
              code: "#62B58F"
            },
            {
              minvalue: "2",
              maxvalue: "3",
              code: "#FFC533"
            },
            {
              minvalue: "3",
              maxvalue: "4",
              code: "#FFC533"
            },

            {
              minvalue: "4",
              maxvalue: "5",
              code: "#F2726F"
            },
            {
              minvalue: "5",
              maxvalue: "6",
              code: "#F2726F"
            },
          ]
        },
        dials: {
          dial: [
            {
              value: "'.$dsr_quantitative.'"
            }
          ]
        }
    }');
        $quantitativeChart->render();
        $qualitativeChart->render();
?>
<div class="row">
    <!-- start chart one  -->
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <div class="card">
            <h4 class="card-header">Quality: <?php echo $dsr_qualitative;?></h4>
            <div class="card-body">
                <!-- <div id="c3chart_donut"></div> -->
                <div id="chart-qualitative"></div>
            </div>
        </div>
    </div>
    <!-- end chart one  -->
    <!-- start chart two  -->
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
    <div class="card">
        <h4 class="card-header">Quantity: <?php echo $dsr_quantitative;?></h4>
        <div class="card-body">
            <div id="chart-quantitative"></div>
        </div>
    </div>
    </div>
    <!-- end chart two  -->
</div>
@endsection