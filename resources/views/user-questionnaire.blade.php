@extends('layout.default') 
@section('content') 
<!-- ============================================================== --> 
<!-- pageheader --> 
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php if($_SESSION['role']=='Admin'){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Questionnaire</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li> -->
                        <!-- <li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page" style="color:#333333;">Questionnaire</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php }?>
<!-- ============================================================== --> 
<!-- end pageheader --> 
<!-- ============================================================== -->
<?php if(empty($users[0]->approved_at)){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Yeh sawalnama is waqt muyassir nahi hai. Aap is sawalnamay ko apne aglay tehqeeqi visit pe pur kar sakenge.</h3>
            </div>
        </div>
    </div>
</div>
<?php }elseif(empty($users[0]->questionnaire_approved_at)){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Yeh sawalnama is waqt muyassir nahi hai. Aap is sawalnamay ko apne aglay tehqeeqi visit pe pur kar sakenge.</h3>
            </div>
        </div>
    </div>
</div>
<?php }else{?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <?php if(empty($visit_uuid)){?>
                <form method="POST" name="follow-up-<?php echo $visit_uid;?>" id="follow-up-<?php echo $visit_uid;?>" data-persist="garlic" enctype="multipart/form-data"> 
                    <?php }?>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <?php
                    // echo '<pre>'; print_r($categories); echo '</pre>'; //exit();
                    // $required='';
                    $required=' required';
                    if(!empty($categories)){
                    foreach ($categories as $category) {
                        if(!empty($category->question_subcategories)){
                        ?><h3><?php echo $category->category_roman;?></h3><?php 
                        foreach ($category->question_subcategories as $question_subcategory) {
                            if(!empty($question_subcategory->questions)){
                            $serial=0; $subpart=''; $sublabel='';
                            ?><h3><?php echo $question_subcategory->subcategory_roman;?></h3><?php 
                            foreach ($question_subcategory->questions as $question) {
                                if( $subpart != $question->subcategory_subpart ) {
                                    $subpart  = $question->subcategory_subpart;
                                    if(!empty($question->subcategory_subpart)) echo '<h4>'.$subparts[$question->subcategory_subpart].'</h4>';
                                }
                                
                                if( $sublabel != $question->subcategory_subpart_sublabel ) {
                                    $sublabel  = $question->subcategory_subpart_sublabel;
                                    if(!empty($question->subcategory_subpart_sublabel)) echo '<h4>'.$sublabels   [$question->subcategory_subpart_sublabel].'</h4>';
                                }
                                echo '<div class="form-group">';
                                ?><label><?php echo ($question->code).') '.$question->urdu;?></label><?php
                                if(empty($visit_uuid)){
                                switch ($question->qtype) {
                                    case 'datetime':
                                    if($question->code=='B1')
                                         echo '<input type="date" class="form-control" name="question_'.$question->id.'" id="question_'.$question->id.'" value='.date('Y-m-d').''.$required.'>';
                                    else echo '<input type="date" class="form-control" name="question_'.$question->id.'" id="question_'.$question->id.'"'.$required.'>';
                                        // #calculate gestational age
                                        unset($gestational);
                                        $gestational['B11'] = ['question_28'=> 'question_29'];
                                        if(!empty($gestational)){
                                        foreach ($gestational as $key => $value) {
                                            if(!empty($value)){
                                            foreach ($value as $key2 => $value2) {
                                                // echo '<br>'.$key.'| '.$key2.'| '.$value2;
                                                if($question->code==$key){
                                                ?><script type="text/javascript">
                                                $('#<?php echo $key2?>').blur(function(){
                                                    var start_date  = new Date($('#<?php echo $key2?>').val());
                                                    var end_date    = new Date(); // today
                                                    var timeDiff    = end_date.getTime() - start_date.getTime();
                                                    $('#<?php echo $value2?>').val((timeDiff / (1000 * 60 * 60 * 24 * 365)).toFixed(0)); //milliseconds per year
                                                });
                                                </script><?php 
                                                }
                                            }
                                            }
                                        }
                                        }

                                        unset($gestational);
                                        $gestational['E1a'] = ['question_691'=> 'question_692'];
                                        $gestational['E2a'] = ['question_693'=> 'question_694'];
                                        $gestational['E3a'] = ['question_695'=> 'question_696'];
                                        $gestational['E4a'] = ['question_697'=> 'question_698'];
                                        $gestational['E5a'] = ['question_699'=> 'question_700'];
                                        $gestational['E6c'] = ['question_703'=> 'question_704'];
                                        $gestational['E7a'] = ['question_705'=> 'question_706'];
                                        $gestational['E8a'] = ['question_707'=> 'question_708'];
                                        $gestational['E9a'] = ['question_709'=> 'question_710'];
                                        $gestational['E10a'] = ['question_711'=> 'question_712'];
                                        $gestational['E11a'] = ['question_713'=> 'question_714'];
                                        $gestational['E12a'] = ['question_715'=> 'question_716'];
                                        $gestational['E13a'] = ['question_717'=> 'question_718'];
                                        $gestational['E13a'] = ['question_717'=> 'question_718'];
                                        if(!empty($gestational)){
                                        foreach ($gestational as $key => $value) {
                                            if(!empty($value)){
                                            foreach ($value as $key2 => $value2) {
                                                // echo '<br>'.$key.'| '.$key2.'| '.$value2;
                                                if($question->code==$key){
                                                    ?><script type="text/javascript">
                                                    $('#<?php echo $key2?>').blur(function(){
                                                        var start_date  = new Date($('#lmp_date').val());
                                                        var end_date    = new Date($('#<?php echo $key2?>').val());
                                                        var timeDiff    = end_date.getTime() - start_date.getTime();
                                                        $('#<?php echo $value2?>').val( (timeDiff / (1000 * 60 * 60 * 24)).toFixed(0) ); //milliseconds per day
                                                    });
                                                    </script><?php 
                                                }
                                            }
                                            }
                                        }
                                        }
                                        break;
                                    case 'number':
                                    if($question->code=='E1'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_48"]').blur(function(){
                                            if( $("[name='question_48']").val()=='' ){
                                                $('[name="question_720"]').prop('disabled', false);
                                                $('[name="question_720"]').prop('required', true);
                                            }else{
                                                $('[name="question_720"]').val('');
                                                $('[name="question_720"]').prop('disabled', true);
                                                $('[name="question_720"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E6'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_53"]').blur(function(){
                                            if( $("[name='question_53']").val()=='' ){
                                                $('[name="question_725"]').prop('disabled', false);
                                                $('[name="question_725"]').prop('required', true);
                                            }else{
                                                $('[name="question_725"]').prop('disabled', true);
                                                $('[name="question_725"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E7'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_689"]').blur(function(){
                                            if( $("[name='question_689']").val()=='' ){
                                                $('[name="question_726"]').prop('disabled', false);
                                                $('[name="question_726"]').prop('required', true);
                                            }else{
                                                $('[name="question_726"]').val('');
                                                $('[name="question_726"]').prop('disabled', true);
                                                $('[name="question_726"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E8'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_54"]').blur(function(){
                                            if( $("[name='question_54']").val()=='' ){
                                                $('[name="question_727"]').prop('disabled', false);
                                                $('[name="question_727"]').prop('required', true);
                                            }else{
                                                $('[name="question_727"]').val('');
                                                $('[name="question_727"]').prop('disabled', true);
                                                $('[name="question_727"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E9'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_55"]').blur(function(){
                                            if( $("[name='question_55']").val()=='' ){
                                                $('[name="question_728"]').prop('disabled', false);
                                                $('[name="question_728"]').prop('required', true);
                                            }else{
                                                $('[name="question_728"]').val('');
                                                $('[name="question_728"]').prop('disabled', true);
                                                $('[name="question_728"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E10'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_56"]').blur(function(){
                                            if( $("[name='question_56']").val()=='' ){
                                                $('[name="question_729"]').prop('disabled', false);
                                                $('[name="question_729"]').prop('required', true);
                                            }else{
                                                $('[name="question_729"]').val('');
                                                $('[name="question_729"]').prop('disabled', true);
                                                $('[name="question_729"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E11'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_57"]').blur(function(){
                                            if( $("[name='question_57']").val()=='' ){
                                                $('[name="question_730"]').prop('disabled', false);
                                                $('[name="question_730"]').prop('required', true);
                                            }else{
                                                $('[name="question_730"]').val('');
                                                $('[name="question_730"]').prop('disabled', true);
                                                $('[name="question_730"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E12'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_58"]').blur(function(){
                                            if( $("[name='question_58']").val()=='' ){
                                                $('[name="question_731"]').prop('disabled', false);
                                                $('[name="question_731"]').prop('required', true);
                                            }else{
                                                $('[name="question_731"]').val('');
                                                $('[name="question_731"]').prop('disabled', true);
                                                $('[name="question_731"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='E13'){
                                        echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_59"]').blur(function(){
                                            if( $("[name='question_59']").val()=='' ){
                                                $('[name="question_732"]').prop('disabled', false);
                                                $('[name="question_732"]').prop('required', true);
                                            }else{
                                                $('[name="question_732"]').val('');
                                                $('[name="question_732"]').prop('disabled', true);
                                                $('[name="question_732"]').prop('required', false);
                                            }
                                        });
                                        </script>
                                    <?php
                                    }elseif($question->code=='I8.1'){
                                         echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($food_diary[$question->code])?$food_diary[$question->code]:'').'"'.$required.'>';
                                    }elseif($question->code=='B20')
                                         echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" value="'.(!empty($users[0]->family_members)?$users[0]->family_members:'').'"'.$required.'>';
                                    else echo '<input type="number" step="0.01" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'"'.$required.'>';

                                        if($question->code=='G3a'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_70"]').blur(function(){
                                                if( $("[name='question_70']").val()=='' ){
                                                    $('[name="question_735"]').prop('disabled', false);
                                                    $('[name="question_735"]').prop('required', true);
                                                }else{
                                                    $('[name="question_735"]').val('');
                                                    $('[name="question_735"]').prop('disabled', true);
                                                    $('[name="question_735"]').prop('required', false);
                                                }
                                            });
                                            </script>
                                            <?php
                                        }

                                        unset($zero_value);
                                        // $zero_value['J6']     = ['question_162' => 'question_163'];
                                        // $zero_value['J7']     = ['question_164' => 'question_165'];
                                        // $zero_value['J8']     = ['question_166' => 'question_167'];
                                        // echo '<pre>';print_r($zero_value); exit();

                                        if(!empty($zero_value)){
                                        foreach ($zero_value as $key1 => $value1) {
                                        foreach ($value1 as $key2 => $value2) {
                                            if($question->code==$key1){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="<?php echo $key2;?>"]').blur(function(){
                                                if($("[name='<?php echo $key2;?>']:checked + label").text()=='Nahi')
                                                   $('[name="<?php echo $value2;?>"]').val('');
                                            });
                                            </script>
                                            <?php 
                                            }
                                        }
                                        }
                                        }

                                        unset($extras);
                                        $extras['J6a']       = ['question_162' => 'question_163'];
                                        $extras['J7a']       = ['question_164' => 'question_165'];
                                        $extras['J8a']       = ['question_166' => 'question_167'];

                                        if(!empty($extras)){
                                            foreach ($extras as $key1 => $value1) {
                                            foreach ($value1 as $key2 => $value2) {
                                                if($question->code==$key1){
                                                // echo '<br>'.$key1.$key2.$value2;
                                                ?>
                                                <script type="text/javascript">
                                                $('[name="<?php echo $key2?>"]').click(function(){
                                                    if( $("[name='<?php echo $key2?>']:checked + label").text()=='Nahi'){
                                                        $("[name='<?php echo $value2?>']").val('');
                                                        $("[name='<?php echo $value2?>']").prop("checked", false);
                                                        $("[name='<?php echo $value2?>']").prop("disabled", true);
                                                    }else{
                                                        $("[name='<?php echo $value2?>']").prop("checked", true);
                                                        $("[name='<?php echo $value2?>']").prop("disabled", false);
                                                    }
                                                });
                                                </script>
                                                <?php 
                                                }
                                            }
                                            }
                                        }


                                        break;
                                    case 'minutes':
                                        echo '<input type="hidden" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'">';
                                        echo '<input type="number" class="form-control" name="question_'.$question->id.'_hours" id="hours_'.$question->id.'"'.$required.'>';
                                        echo '<input type="number" class="form-control" name="question_'.$question->id.'_mins" id="minutes_'.$question->id.'"'.$required.'>';
                                        if($question->code=='J10'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_169_hours']").blur(function(){
                                            if($("[name='question_169_hours']").val()=='')
                                                 $('[name="question_169_mins"]').prop('required',  true);
                                            else $('[name="question_169_mins"]').prop('required',  false);
                                            });
                                            $("[name='question_169_mins']").blur(function(){
                                            if($("[name='question_169_mins']").val()=='')
                                                 $('[name="question_169_hours"]').prop('required',  true);
                                            else $('[name="question_169_hours"]').prop('required',  false);
                                            });
                                            </script>
                                            <?php 
                                            }
                                        if($question->code=='J11'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_170_hours']").blur(function(){
                                            if($("[name='question_170_hours']").val()=='')
                                                 $('[name="question_170_mins"]').prop('required',  true);
                                            else $('[name="question_170_mins"]').prop('required',  false);
                                            });
                                            $("[name='question_170_mins']").blur(function(){
                                            if($("[name='question_170_mins']").val()=='')
                                                 $('[name="question_170_hours"]').prop('required',  true);
                                            else $('[name="question_170_hours"]').prop('required',  false);
                                            });
                                            </script>
                                            <?php 
                                            }
                                        if($question->code=='J12'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_171_hours']").blur(function(){
                                            if($("[name='question_171_hours']").val()=='')
                                                 $('[name="question_171_mins"]').prop('required',  true);
                                            else $('[name="question_171_mins"]').prop('required',  false);
                                            });
                                            $("[name='question_171_mins']").blur(function(){
                                            if($("[name='question_171_mins']").val()=='')
                                                 $('[name="question_171_hours"]').prop('required',  true);
                                            else $('[name="question_171_hours"]').prop('required',  false);
                                            });
                                            </script>
                                            <?php 
                                            }
                                            break;
                                    case 'combo':
                                        if(!empty($question->answers)){
                                            echo '<select class="form-control" name="question_'.$question->id.'" id="question_'.$question->id.'"'.$required.'>';
                                            echo '<option value="">Choose...</option>';
                                            if($question->code=='C3'){
                                                foreach ($question->answers as $answer) {
                                                    if(!empty($users[0]->kitni_martaba) and $users[0]->kitni_martaba==$answer->id)
                                                         echo '<option value="'.$answer->id.'" selected="selected">'.$answer->answer.'</option>';
                                                    else echo '<option value="'.$answer->id.'">'.$answer->answer.'</option>';
                                                } 
                                                echo '</select>';
                                            }else{
                                                foreach ($question->answers as $answer) {
                                                    echo '<option value="'.$answer->id.'">'.$answer->answer.'</option>';
                                                } 
                                            }
                                            echo '</select>'; 
                                            if($question->code=='E1c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_720']").change(function(){
                                                if($("[name='question_720']").val()==''){
                                                    $('[name="question_48"]').prop('disabled',  false);
                                                    $('[name="question_48"]').prop('required',  true);
                                                    $('[name="question_691"]').prop('disabled', false);
                                                    $('[name="question_691"]').prop('required', true);
                                                    $('[name="question_692"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_48"]').prop('disabled',  true);
                                                    $('[name="question_48"]').prop('required',  false);
                                                    $('[name="question_691"]').prop('disabled', true);
                                                    $('[name="question_691"]').prop('required', false);
                                                    $('[name="question_692"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E2c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_721']").change(function(){
                                                if($("[name='question_721']").val()==''){
                                                    $('[name="question_49"]').prop('disabled',  false);
                                                    $('[name="question_49"]').prop('required',  true);
                                                    $('[name="question_693"]').prop('disabled', false);
                                                    $('[name="question_693"]').prop('required', true);
                                                    $('[name="question_694"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_49"]').prop('disabled',  true);
                                                    $('[name="question_49"]').prop('required',  false);
                                                    $('[name="question_693"]').prop('disabled', true);
                                                    $('[name="question_693"]').prop('required', false);
                                                    $('[name="question_694"]').prop('disabled', true);
                                                }
                                            });

                                            $("[name='question_49']").change(function(){
                                            if($("[name='question_49']").val()==''){
                                                $('[name="question_721"]').prop('disabled', false);
                                                $('[name="question_721"]').prop('required', true);
                                            }else{
                                                $('[name="question_721"]').val('');
                                                $('[name="question_721"]').prop('disabled', true);
                                                $('[name="question_721"]').prop('required', false);
                                            }
                                            });
                                            </script>
                                            <?php
                                            }
                                            if($question->code=='E3c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_722']").change(function(){
                                            if($("[name='question_722']").val()==''){
                                                $('[name="question_50"]').prop('disabled', false);
                                                $('[name="question_50"]').prop('required', true);
                                                $('[name="question_695"]').prop('disabled', false);
                                                $('[name="question_695"]').prop('required', true);
                                                $('[name="question_696"]').prop('disabled', false);
                                            }else{
                                                $('[name="question_50"]').prop('disabled', true);
                                                $('[name="question_50"]').prop('required', false);
                                                $('[name="question_695"]').prop('disabled', true);
                                                $('[name="question_695"]').prop('required', false);
                                                $('[name="question_696"]').prop('disabled', true);
                                            }
                                            });
                                            $("[name='question_50']").change(function(){
                                            if($("[name='question_50']").val()==''){
                                                $('[name="question_722"]').prop('disabled', false);
                                                $('[name="question_722"]').prop('required', true);
                                            }else{
                                                $('[name="question_722"]').val('');
                                                $('[name="question_722"]').prop('disabled', true);
                                                $('[name="question_722"]').prop('required', false);
                                            }
                                            });
                                            </script>
                                            <?php
                                            }
                                            if($question->code=='E4c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_723']").change(function(){
                                            if($("[name='question_723']").val()==''){
                                                $('[name="question_51"]').prop('disabled', false);
                                                $('[name="question_51"]').prop('required', true);
                                                $('[name="question_697"]').prop('disabled', false);
                                                $('[name="question_697"]').prop('required', true);
                                                $('[name="question_698"]').prop('disabled', false);
                                            }else{
                                                $('[name="question_51"]').prop('disabled', true);
                                                $('[name="question_51"]').prop('required', false);
                                                $('[name="question_697"]').prop('disabled', true);
                                                $('[name="question_697"]').prop('required', false);
                                                $('[name="question_698"]').prop('disabled', true);
                                            }
                                            });
                                            $("[name='question_51']").change(function(){
                                            if($("[name='question_51']").val()==''){
                                                $('[name="question_723"]').prop('disabled', false);
                                                $('[name="question_723"]').prop('required', true);
                                            }else{
                                                $('[name="question_723"]').val('');
                                                $('[name="question_723"]').prop('disabled', true);
                                                $('[name="question_723"]').prop('required', false);
                                            }
                                            });
                                            </script>
                                            <?php
                                            }
                                            if($question->code=='E5c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_724']").change(function(){
                                            if($("[name='question_724']").val()==''){
                                                $('[name="question_52"]').prop('disabled', false);
                                                $('[name="question_52"]').prop('required', true);
                                                $('[name="question_699"]').prop('disabled', false);
                                                $('[name="question_699"]').prop('required', true);
                                                $('[name="question_700"]').prop('disabled', false);
                                            }else{
                                                $('[name="question_52"]').prop('disabled', true);
                                                $('[name="question_52"]').prop('required', false);
                                                $('[name="question_699"]').prop('disabled', true);
                                                $('[name="question_699"]').prop('required', false);
                                                $('[name="question_700"]').prop('disabled', true);
                                            }
                                            });
                                            $("[name='question_52']").change(function(){
                                            if($("[name='question_52']").val()==''){
                                                $('[name="question_724"]').prop('disabled', false);
                                                $('[name="question_724"]').prop('required', true);
                                            }else{
                                                $('[name="question_724"]').val('');
                                                $('[name="question_724"]').prop('disabled', true);
                                                $('[name="question_724"]').prop('required', false);
                                            }
                                            });
                                            </script>
                                            <?php
                                            }

                                            if($question->code=='E6e'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_725']").change(function(){
                                                if($("[name='question_725']").val()==''){
                                                    $('[name="question_53"]').prop('disabled', false);
                                                    $('[name="question_53"]').prop('required', true);
                                                    $('[name="question_701"]').prop('disabled', false);
                                                    $('[name="question_701"]').prop('required', true);
                                                    $('[name="question_702"]').prop('disabled', false);
                                                    $('[name="question_702"]').prop('required', true);
                                                    $('[name="question_703"]').prop('disabled', false);
                                                    $('[name="question_703"]').prop('required', true);
                                                    $('[name="question_704"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_53"]').prop('disabled', true);
                                                    $('[name="question_53"]').prop('required', false);
                                                    $('[name="question_701"]').prop('disabled', true);
                                                    $('[name="question_701"]').prop('required', false);
                                                    $('[name="question_702"]').prop('disabled', true);
                                                    $('[name="question_702"]').prop('required', false);
                                                    $('[name="question_703"]').prop('disabled', true);
                                                    $('[name="question_703"]').prop('required', false);
                                                    $('[name="question_704"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E7c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_726']").change(function(){
                                                if($("[name='question_726']").val()==''){
                                                    $('[name="question_689"]').prop('disabled', false);
                                                    $('[name="question_689"]').prop('required', true);
                                                    $('[name="question_705"]').prop('disabled', false);
                                                    $('[name="question_705"]').prop('required', true);
                                                    $('[name="question_706"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_689"]').prop('disabled', true);
                                                    $('[name="question_689"]').prop('required', false);
                                                    $('[name="question_705"]').prop('disabled', true);
                                                    $('[name="question_705"]').prop('required', false);
                                                    $('[name="question_706"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E8c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_727']").change(function(){
                                                if($("[name='question_727']").val()==''){
                                                    $('[name="question_54"]').prop('disabled',  false);
                                                    $('[name="question_54"]').prop('required',  true);
                                                    $('[name="question_707"]').prop('disabled', false);
                                                    $('[name="question_707"]').prop('required', true);
                                                    $('[name="question_708"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_54"]').prop('disabled',  true);
                                                    $('[name="question_54"]').prop('required',  false);
                                                    $('[name="question_707"]').prop('disabled', true);
                                                    $('[name="question_707"]').prop('required', false);
                                                    $('[name="question_708"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E9c'){
                                            ?>
                                            <script type="text/javascript">
                                            $("[name='question_728']").change(function(){
                                                if($("[name='question_728']").val()==''){
                                                    $('[name="question_55"]').prop('disabled',  false);
                                                    $('[name="question_55"]').prop('required',  true);
                                                    $('[name="question_709"]').prop('disabled', false);
                                                    $('[name="question_709"]').prop('required', true);
                                                    $('[name="question_710"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_55"]').prop('disabled',  true);
                                                    $('[name="question_55"]').prop('required',  false);
                                                    $('[name="question_709"]').prop('disabled', true);
                                                    $('[name="question_709"]').prop('required', false);
                                                    $('[name="question_710"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E10c'){
                                            ?>
                                            <script type="text/javascript"> 
                                            $("[name='question_729']").change(function(){
                                                if($("[name='question_729']").val()==''){
                                                    $('[name="question_56"]').prop('disabled',  false);
                                                    $('[name="question_56"]').prop('required',  true);
                                                    $('[name="question_711"]').prop('disabled', false);
                                                    $('[name="question_711"]').prop('required', true);
                                                    $('[name="question_712"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_56"]').prop('disabled',  true);
                                                    $('[name="question_56"]').prop('required',  false);
                                                    $('[name="question_711"]').prop('disabled', true);
                                                    $('[name="question_711"]').prop('required', false);
                                                    $('[name="question_712"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E11c'){
                                            ?>
                                            <script type="text/javascript"> 
                                            $("[name='question_730']").change(function(){
                                                if($("[name='question_730']").val()==''){
                                                    $('[name="question_57"]').prop('disabled',  false);
                                                    $('[name="question_57"]').prop('required',  true);
                                                    $('[name="question_713"]').prop('disabled', false);
                                                    $('[name="question_713"]').prop('required', true);
                                                    $('[name="question_714"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_57"]').prop('disabled',  true);
                                                    $('[name="question_57"]').prop('required',  false);
                                                    $('[name="question_713"]').prop('disabled', true);
                                                    $('[name="question_713"]').prop('required', false);
                                                    $('[name="question_714"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E12c'){
                                            ?>
                                            <script type="text/javascript"> 
                                            $("[name='question_731']").change(function(){
                                                if($("[name='question_731']").val()==''){
                                                    $('[name="question_58"]').prop('disabled',  false);
                                                    $('[name="question_58"]').prop('required',  true);
                                                    $('[name="question_715"]').prop('disabled', false);
                                                    $('[name="question_715"]').prop('required', true);
                                                    $('[name="question_716"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_58"]').prop('disabled',  true);
                                                    $('[name="question_58"]').prop('required',  false);
                                                    $('[name="question_715"]').prop('disabled', true);
                                                    $('[name="question_715"]').prop('required', false);
                                                    $('[name="question_716"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                            if($question->code=='E13c'){
                                            ?>
                                            <script type="text/javascript"> 
                                            $("[name='question_732']").change(function(){
                                                if($("[name='question_732']").val()==''){
                                                    $('[name="question_59"]').prop('disabled',  false);
                                                    $('[name="question_59"]').prop('required',  true);
                                                    $('[name="question_717"]').prop('disabled', false);
                                                    $('[name="question_717"]').prop('required', true);
                                                    $('[name="question_718"]').prop('disabled', false);
                                                }else{
                                                    $('[name="question_59"]').prop('disabled',  true);
                                                    $('[name="question_59"]').prop('required',  false);
                                                    $('[name="question_717"]').prop('disabled', true);
                                                    $('[name="question_717"]').prop('required', false);
                                                    $('[name="question_718"]').prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }


                                            if($question->code=='H3'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_71"],[name="question_72"]').click(function(){
                                                var str1 = $("[name='question_71']:checked + label").text();
                                                var str2 = $("[name='question_72']:checked + label").text();
                                                if (str1.toLowerCase().indexOf("han") >= 0 || str2.toLowerCase().indexOf("han") >= 0){
                                                    $('[name="question_73"]').prop('disabled', false);
                                                    $('[name="question_73"]').prop('required', true);
                                                }else{
                                                    $('[name="question_73"]').val('');
                                                    $('[name="question_73"]').prop('disabled', true);
                                                    $('[name="question_73"]').prop('required', false);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }

                                            if($question->code=='H6'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_74"],[name="question_75"]').click(function(){
                                                var str1 = $("[name='question_74']:checked + label").text();
                                                var str2 = $("[name='question_75']:checked + label").text();
                                                if (str1.toLowerCase().indexOf("han") >= 0 || str2.toLowerCase().indexOf("han") >= 0){
                                                    $('[name="question_76"]').prop('disabled', false);
                                                    $('[name="question_76"]').prop('required', true);
                                                }else{
                                                    $('[name="question_76"]').val('');
                                                    $('[name="question_76"]').prop('disabled', true);
                                                    $('[name="question_76"]').prop('required', false);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }
                                            if($question->code=='H9'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_77"],[name="question_78"]').click(function(){
                                                var str1 = $("[name='question_77']:checked + label").text();
                                                var str2 = $("[name='question_78']:checked + label").text();
                                                if (str1.toLowerCase().indexOf("han") >= 0 || str2.toLowerCase().indexOf("han") >= 0){
                                                    $('[name="question_79"]').prop('disabled', false);
                                                    $('[name="question_79"]').prop('required', true);
                                                }else{
                                                    $('[name="question_79"]').val('');
                                                    $('[name="question_79"]').prop('disabled', true);
                                                    $('[name="question_79"]').prop('required', false);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }

                                            if($question->code=='H12'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_80"],[name="question_81"]').click(function(){
                                                var str1 = $("[name='question_80']:checked + label").text();
                                                var str2 = $("[name='question_81']:checked + label").text();
                                                if (str1.toLowerCase().indexOf("han") >= 0 || str2.toLowerCase().indexOf("han") >= 0){
                                                    $('[name="question_82"]').prop('disabled', false);
                                                    $('[name="question_82"]').prop('required', true);
                                                }else{
                                                    $('[name="question_82"]').val('');
                                                    $('[name="question_82"]').prop('disabled', true);
                                                    $('[name="question_82"]').prop('required', false);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }

                                            if($question->code=='G3b'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_735"]').change(function(){
                                                if( $("[name='question_735'] option:selected").text()=='Did not respond'){
                                                    $('[name="question_70"]').val('');
                                                    $('[name="question_70"]').prop('disabled', true);
                                                    $('[name="question_70"]').prop('required', false);
                                                }else{
                                                    $('[name="question_70"]').prop('disabled', false);
                                                    $('[name="question_70"]').prop('required', true);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }

                                            if($question->code=='B15'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_32"]').change(function(){
                                                var str = $("[name='question_32'] option:selected").text();
                                                if (str.toLowerCase().indexOf("nahi") >= 0 || str.toLowerCase().indexOf("berozgar") >= 0){
                                                    $('[name="question_33"]').val('');
                                                    $('[name="question_33"]').prop('disabled', true);
                                                    $('[name="question_33"]').prop('required', false);
                                                }else{
                                                    $('[name="question_33"]').prop('disabled', false);
                                                    $('[name="question_33"]').prop('required', true);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }

                                            if($question->code=='I6.1'){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="question_141"]').change(function(){
                                                var str = $("[name='question_141'] option:selected").text();
                                                if (str.toLowerCase().indexOf("makhan") >= 0 || str.toLowerCase().indexOf("margarine") >= 0 || str.toLowerCase().indexOf("ghee") >= 0){
                                                    $('[name="question_142"]').val('');
                                                    $('[name="question_142"]').prop('disabled', true);
                                                    $('[name="question_142"]').prop('required', false);

                                                    $('[name="question_737"]').val('');
                                                    $('[name="question_737"]').prop('disabled', true);
                                                    $('[name="question_737"]').prop('required', false);

                                                    $('[name="question_143"]').val('');
                                                    $('[name="question_143"]').prop('disabled', true);
                                                    $('[name="question_143"]').prop('required', false);
                                                }else{
                                                    $('[name="question_142"]').val('');
                                                    $('[name="question_142"]').prop('disabled', false);
                                                    $('[name="question_142"]').prop('required', true);

                                                    $('[name="question_737"]').val('');
                                                    $('[name="question_737"]').prop('disabled', false);
                                                    $('[name="question_737"]').prop('required', true);

                                                    $('[name="question_143"]').val('');
                                                    $('[name="question_143"]').prop('disabled', false);
                                                    $('[name="question_143"]').prop('required', true);
                                                }
                                            });
                                            </script>
                                            <?php
                                            }
                                        }
                                       // $my_variables['B15']       = ['question_32' => 'question_33'];
                                        break;
                                    case 'radio':
                                        if(!empty($question->answers)){
                                            echo '<div>';
                                            // echo '<br>code: '.$question->code;                                            
                                            // print_r($food_diary);
                                            if(!empty($food_diary[$question->code])){
                                                // echo '<br>entry: '.$food_diary[$question->code];
                                                $style = (count($question->answers)>3)?' style="width:30px"':' style="width:102px"';
                                                foreach ($question->answers as $answer) {
                                                    if($food_diary[$question->code]==$answer->answer){
                                                    echo '<input checked type="radio" name="question_'.$question->id.'" id="answer_'.$answer->id.'" value="'.$answer->id.'"'.$required.'>
                                                    <label for="answer_'.$answer->id.'"'.$style.'>'.$answer->answer.'</label>';                                                

                                                    }elseif($answer->answer == '>=5' and $food_diary[$question->code] >= 5){
                                                    echo '<input checked type="radio" name="question_'.$question->id.'" id="answer_'.$answer->id.'" value="'.$answer->id.'"'.$required.'>
                                                    <label for="answer_'.$answer->id.'"'.$style.'>'.$answer->answer.'</label>';                                                

                                                    }elseif($answer->answer == '<1' and $food_diary[$question->code] =='<1'){
                                                    echo '<input checked type="radio" name="question_'.$question->id.'" id="answer_'.$answer->id.'" value="'.$answer->id.'"'.$required.'>
                                                    <label for="answer_'.$answer->id.'"'.$style.'>'.$answer->answer.'</label>';                                                
                                                        
                                                    }else{
                                                    echo '<input type="radio" name="question_'.$question->id.'" id="answer_'.$answer->id.'" value="'.$answer->id.'"'.$required.'>
                                                    <label for="answer_'.$answer->id.'"'.$style.'>'.$answer->answer.'</label>';                                                
                                                        
                                                    }
                                                } unset($style); 
                                            }else{
                                                $style = (count($question->answers)>3)?' style="width:30px"':' style="width:102px"';
                                                foreach ($question->answers as $answer) {
                                                    echo '<input type="radio" name="question_'.$question->id.'" id="answer_'.$answer->id.'" value="'.$answer->id.'"'.$required.'>
                                                    <label for="answer_'.$answer->id.'"'.$style.'>'.$answer->answer.'</label>';                                                
                                                } unset($style); 
                                            }
                                            echo '</div>';

{
    unset($zero_value);
    $zero_value['I1.1']     = ['question_83' => 'question_84'];
    $zero_value['I1.2']     = ['question_85' => 'question_86'];
    $zero_value['I1.3']     = ['question_87' => 'question_88'];
    $zero_value['I1.4']     = ['question_89' => 'question_90'];
    $zero_value['I1.5']     = ['question_91' => 'question_92'];
    $zero_value['I1.7']     = ['question_95' => 'question_96'];
    $zero_value['I1.8']     = ['question_97' => 'question_98'];
    $zero_value['I1.9']     = ['question_99' => 'question_100'];
    $zero_value['I1.10']    = ['question_101' => 'question_102'];
    $zero_value['I1.11']    = ['question_103' => 'question_104'];
    $zero_value['I2.1']     = ['question_105' => 'question_106'];
    $zero_value['I2.2']     = ['question_107' => 'question_108'];
    $zero_value['I3.1']     = ['question_109' => 'question_110'];
    $zero_value['I3.2']     = ['question_111' => 'question_112'];
    $zero_value['I3.3']     = ['question_113' => 'question_114'];
    $zero_value['I3.4']     = ['question_115' => 'question_116'];
    $zero_value['I3.5']     = ['question_117' => 'question_118'];
    $zero_value['I3.6']     = ['question_119' => 'question_120'];
    $zero_value['I4.1']     = ['question_121' => 'question_122'];
    $zero_value['I4.2']     = ['question_123' => 'question_124'];
    $zero_value['I4.3']     = ['question_125' => 'question_126'];
    $zero_value['I4.4']     = ['question_127' => 'question_128'];
    $zero_value['I4.5']     = ['question_129' => 'question_130'];
    $zero_value['I4.6']     = ['question_131' => 'question_132'];
    $zero_value['I4.7']     = ['question_133' => 'question_134'];
    $zero_value['I4.8']     = ['question_135' => 'question_136'];
    $zero_value['I5.1']     = ['question_137' => 'question_138'];
    $zero_value['I5.2']     = ['question_139' => 'question_140'];
    // $zero_value['I6.1']     = ['question_141' => 'question_142'];
    $zero_value['I7.1']     = ['question_144' => 'question_145'];
    $zero_value['I7.2']     = ['question_146' => 'question_147'];
    $zero_value['I7.3']     = ['question_148' => 'question_149'];
    $zero_value['I7.4']     = ['question_150' => 'question_151'];
    $zero_value['J1']       = ['question_153' => 'question_154'];
    $zero_value['J2']       = ['question_155' => 'question_156'];
    $zero_value['J4']       = ['question_158' => 'question_159'];
    $zero_value['J5']       = ['question_160' => 'question_161'];
    $zero_value['J6']       = ['question_162' => 'question_163'];
    $zero_value['J7']       = ['question_164' => 'question_165'];
    $zero_value['J8']       = ['question_166' => 'question_167'];

    $unselectable['I1.1a']     = ['question_83' => 'question_84'];
    $unselectable['I1.2a']     = ['question_85' => 'question_86'];
    $unselectable['I1.3a']     = ['question_87' => 'question_88'];
    $unselectable['I1.4a']     = ['question_89' => 'question_90'];
    $unselectable['I1.5a']     = ['question_91' => 'question_92'];
    $unselectable['I1.7a']     = ['question_95' => 'question_96'];
    $unselectable['I1.8a']     = ['question_97' => 'question_98'];
    $unselectable['I1.9a']     = ['question_99' => 'question_100'];
    $unselectable['I1.10a']    = ['question_101' => 'question_102'];
    $unselectable['I1.11a']    = ['question_103' => 'question_104'];
    $unselectable['I2.1a']     = ['question_105' => 'question_106'];
    $unselectable['I2.2a']     = ['question_107' => 'question_108'];
    $unselectable['I3.1a']     = ['question_109' => 'question_110'];
    $unselectable['I3.2a']     = ['question_111' => 'question_112'];
    $unselectable['I3.3a']     = ['question_113' => 'question_114'];
    $unselectable['I3.4a']     = ['question_115' => 'question_116'];
    $unselectable['I3.5a']     = ['question_117' => 'question_118'];
    $unselectable['I3.6a']     = ['question_119' => 'question_120'];
    $unselectable['I4.1a']     = ['question_121' => 'question_122'];
    $unselectable['I4.2a']     = ['question_123' => 'question_124'];
    $unselectable['I4.3a']     = ['question_125' => 'question_126'];
    $unselectable['I4.4a']     = ['question_127' => 'question_128'];
    $unselectable['I4.5a']     = ['question_129' => 'question_130'];
    $unselectable['I4.6a']     = ['question_131' => 'question_132'];
    $unselectable['I4.7a']     = ['question_133' => 'question_134'];
    $unselectable['I4.8a']     = ['question_135' => 'question_136'];
    $unselectable['I5.1a']     = ['question_137' => 'question_138'];
    $unselectable['I5.2a']     = ['question_139' => 'question_140'];
    // $unselectable['I6.1a']     = ['question_141' => 'question_142'];
    $unselectable['I7.1a']     = ['question_144' => 'question_145'];
    $unselectable['I7.2a']     = ['question_146' => 'question_147'];
    $unselectable['I7.3a']     = ['question_148' => 'question_149'];
    $unselectable['I7.4a']     = ['question_150' => 'question_151'];
    $unselectable['J1a']       = ['question_153' => 'question_154'];
    $unselectable['J2a']       = ['question_155' => 'question_156'];
    $unselectable['J4a']       = ['question_158' => 'question_159'];

    unset($extras);
    $extras['J5a']       = ['question_160' => 'question_161'];
}




                                        if($question->code=='C1'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_39"]').click(function(){
                                            var str = $("[name='question_39']:checked + label").text();
                                            if (str.toLowerCase().indexOf("nahi") >= 0){
                                                $('[name="question_40"]').val('');
                                                $('[name="question_40"]').prop('disabled', true);
                                                $('[name="question_40"]').prop('required', false);
                                                $('[name="question_740"]').val('');
                                                $('[name="question_740"]').prop('disabled', true);
                                                $('[name="question_740"]').prop('required', false);
                                            }else{
                                                $('[name="question_40"]').prop('disabled', false);
                                                $('[name="question_40"]').prop('required', true);
                                                $('[name="question_740"]').prop('disabled', false);
                                                $('[name="question_740"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='C2'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_41"]').click(function(){
                                            var str = $("[name='question_41']:checked + label").text();
                                            if (str.toLowerCase().indexOf("nahi") >= 0){
                                                $('[name="question_42"]').val('');
                                                $('[name="question_42"]').prop('disabled', true);
                                                $('[name="question_42"]').prop('required', false);
                                                $('[name="question_741"]').val('');
                                                $('[name="question_741"]').prop('disabled', true);
                                                $('[name="question_741"]').prop('required', false);
                                            }else{
                                                $('[name="question_42"]').prop('disabled', false);
                                                $('[name="question_42"]').prop('required', true);
                                                $('[name="question_741"]').prop('disabled', false);
                                                $('[name="question_741"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='G3'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_69"]').click(function(){
                                            if( $("[name='question_69']:checked + label").text()=='Nahi' ||  $("[name='question_69']:checked + label").text()=='Not Applicable'){
                                                $('[name="question_70"]').val('');
                                                $('[name="question_70"]').prop('disabled', true);
                                                $('[name="question_70"]').prop('required', false);
                                                $('[name="question_735"]').val('');
                                                $('[name="question_735"]').prop('disabled', true);
                                                $('[name="question_735"]').prop('required', false);
                                            }else{
                                                $('[name="question_70"]').prop('disabled', false);
                                                $('[name="question_70"]').prop('required', true);
                                                $('[name="question_735"]').prop('disabled', false);
                                                $('[name="question_735"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='I4.9'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_742"]').click(function(){
                                            if( $("[name='question_742']:checked + label").text()=='Nahi'){

                                                $('[name="question_743"]').prop('disabled', true);
                                                $('[name="question_743"]').prop('required', false);
                                                $('[name="question_744"]').prop("checked", false);
                                                $('[name="question_744"]').prop('disabled', true);
                                                $('[name="question_744"]').prop('required', false);
                                                $('[name="question_749"]').prop("checked", false);
                                                $('[name="question_749"]').prop('disabled', true);
                                                $('[name="question_749"]').prop('required', false);
                                            }else{
                                                $('[name="question_743"]').prop('disabled', false);
                                                $('[name="question_743"]').prop('required', true);
                                                $('[name="question_744"]').prop('disabled', false);
                                                $('[name="question_744"]').prop('required', true);
                                                $('[name="question_749"]').prop('disabled', false);
                                                $('[name="question_749"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='I1.12'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_758"]').click(function(){
                                            if( $("[name='question_758']:checked + label").text()=='Nahi'){
                                                $('[name="question_759"]').prop('disabled', true);
                                                $('[name="question_759"]').prop('required', false);
                                                $('[name="question_760"]').prop("checked", false);
                                                $('[name="question_760"]').prop('disabled', true);
                                                $('[name="question_760"]').prop('required', false);
                                                $('[name="question_761"]').prop("checked", false);
                                                $('[name="question_761"]').prop('disabled', true);
                                                $('[name="question_761"]').prop('required', false);
                                            }else{
                                                $('[name="question_759"]').prop('disabled', false);
                                                $('[name="question_759"]').prop('required', true);
                                                $('[name="question_760"]').prop('disabled', false);
                                                $('[name="question_760"]').prop('required', true);
                                                $('[name="question_761"]').prop('disabled', false);
                                                $('[name="question_761"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='I2.3'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_754"]').click(function(){
                                            if( $("[name='question_754']:checked + label").text()=='Nahi'){
                                                $('[name="question_755"]').prop('disabled', true);
                                                $('[name="question_755"]').prop('required', false);
                                                $('[name="question_756"]').prop("checked", false);
                                                $('[name="question_756"]').prop('disabled', true);
                                                $('[name="question_756"]').prop('required', false);
                                                $('[name="question_757"]').prop("checked", false);
                                                $('[name="question_757"]').prop('disabled', true);
                                                $('[name="question_757"]').prop('required', false);
                                            }else{
                                                $('[name="question_755"]').prop('disabled', false);
                                                $('[name="question_755"]').prop('required', true);
                                                $('[name="question_756"]').prop('disabled', false);
                                                $('[name="question_756"]').prop('required', true);
                                                $('[name="question_757"]').prop('disabled', false);
                                                $('[name="question_757"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='I3.7'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_750"]').click(function(){
                                            if( $("[name='question_750']:checked + label").text()=='Nahi'){
                                                $('[name="question_751"]').prop('disabled', true);
                                                $('[name="question_751"]').prop('required', false);
                                                $('[name="question_752"]').prop("checked", false);
                                                $('[name="question_752"]').prop('disabled', true);
                                                $('[name="question_752"]').prop('required', false);
                                                $('[name="question_753"]').prop("checked", false);
                                                $('[name="question_753"]').prop('disabled', true);
                                                $('[name="question_753"]').prop('required', false);
                                            }else{
                                                $('[name="question_751"]').prop('disabled', false);
                                                $('[name="question_751"]').prop('required', true);
                                                $('[name="question_752"]').prop('disabled', false);
                                                $('[name="question_752"]').prop('required', true);
                                                $('[name="question_753"]').prop('disabled', false);
                                                $('[name="question_753"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if($question->code=='I5.3'){
                                        ?>
                                        <script type="text/javascript">
                                        $('[name="question_745"]').click(function(){
                                            if( $("[name='question_745']:checked + label").text()=='Nahi'){
                                                $('[name="question_746"]').prop('disabled', true);
                                                $('[name="question_746"]').prop('required', false);
                                                $('[name="question_747"]').prop("checked", false);
                                                $('[name="question_747"]').prop('disabled', true);
                                                $('[name="question_747"]').prop('required', false);
                                                $('[name="question_748"]').prop("checked", false);
                                                $('[name="question_748"]').prop('disabled', true);
                                                $('[name="question_748"]').prop('required', false);
                                            }else{
                                                $('[name="question_746"]').prop('disabled', false);
                                                $('[name="question_746"]').prop('required', true);
                                                $('[name="question_747"]').prop('disabled', false);
                                                $('[name="question_747"]').prop('required', true);
                                                $('[name="question_748"]').prop('disabled', false);
                                                $('[name="question_748"]').prop('required', true);
                                            }
                                        });
                                        </script>
                                        <?php
                                        }

                                        if(!empty($extras)){
                                            foreach ($extras as $key1 => $value1) {
                                            foreach ($value1 as $key2 => $value2) {
                                                if($question->code==$key1){
                                                // echo '<br>'.$key1.$key2.$value2;
                                                ?>
                                                <script type="text/javascript">
                                                $('[name="<?php echo $key2?>"]').click(function(){
                                                    if( $("[name='<?php echo $key2?>']:checked + label").text()=='Nahi'){
                                                        $("[name='<?php echo $value2?>']").val('');
                                                        $("[name='<?php echo $value2?>']").prop("checked", false);
                                                        $("[name='<?php echo $value2?>']").prop("disabled", true);
                                                    }else{
                                                        $("[name='<?php echo $value2?>']").prop("checked", true);
                                                        $("[name='<?php echo $value2?>']").prop("disabled", false);
                                                    }
                                                });
                                                </script>
                                                <?php 
                                                }
                                            }
                                            }
                                        }

                                        if(!empty($zero_value)){
                                        foreach ($zero_value as $key1 => $value1) {
                                        foreach ($value1 as $key2 => $value2) {
                                            if($question->code==$key1){
                                            // echo '<br>'.$key1.$key2.$value2;
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="<?php echo $key2?>"]').click(function(){
                                                if( $("[name='<?php echo $key2?>']:checked + label").text()==0){
                                                    $("[name='<?php echo $value2?>']").prop("checked", false);

                                                    $("[name='<?php echo $value2;?>']").prop('required', false);
                                                    $("[name='<?php echo $value2;?>']").prop('disabled', true);
                                                }else{
                                                    $("[name='<?php echo $value2;?>']").prop('required', true);
                                                    $("[name='<?php echo $value2;?>']").prop('disabled', false);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                        }
                                        }
                                        }

                                        if(!empty($unselectable)){
                                        foreach ($unselectable as $key1 => $value1) {
                                        foreach ($value1 as $key2 => $value2) {
                                            if($question->code==$key1){
                                            // echo '<br>'.$key1.$key2.$value2;
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="<?php echo $value2;?>"]').click(function(){
                                                if($("[name='<?php echo $key2;?>']:checked + label").text()==0){
                                                    toastr.error('<?php echo $key1;?> cannot be selected because previous question is 0');
                                                    $("[name='<?php echo $value2;?>']").prop("checked", false);
                                                }
                                            });
                                            </script><?php 
                                            }
                                            
                                        }
                                        }
                                        }
                                        }
                                        break;
                                    case 'descriptive':
                                        if($question->code=='B3')
                                             echo '<input type="text" name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control" value="'.(!empty($users[0]->mr_number)?$users[0]->mr_number:'').'"'.$required.'>';
                                        elseif($question->code=='B4')
                                             echo '<input type="text" name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control" value="'.$users[0]->first_name.' '.$users[0]->last_name.'"'.$required.'>';
                                        else echo '<input type="text" name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control"'.$required.'>';

                                        unset($zero_value);
                                        $zero_value['B6a']    = ['question_23' => 'question_738'];
                                        $zero_value['B7a']    = ['question_24' => 'question_739'];
                                        $zero_value['C1b']    = ['question_40' => 'question_740'];
                                        $zero_value['C2b']    = ['question_42' => 'question_741'];
                                        $zero_value['C3a']    = ['question_43' => 'question_736'];
                                        $zero_value['I6.2a']   = ['question_142' => 'question_737'];
                                        // echo '<pre>';print_r($zero_value); exit();

                                        if(!empty($zero_value)){
                                        foreach ($zero_value as $key1 => $value1) {
                                        foreach ($value1 as $key2 => $value2) {
                                            if($question->code==$key1){
                                            ?>
                                            <script type="text/javascript">
                                            $('[name="<?php echo $key2;?>"]').change(function(){
                                                var str = $("#<?php echo $key2;?> option:selected").text();
                                                if (str.toLowerCase().indexOf("wazahat karain") >= 0){
                                                    $("[name='<?php echo $value2;?>']").val('');
                                                    $("[name='<?php echo $value2;?>']").prop('required', true);
                                                    $("[name='<?php echo $value2;?>']").prop('disabled', false);
                                                }else{
                                                    $("[name='<?php echo $value2;?>']").val('');
                                                    $("[name='<?php echo $value2;?>']").prop('required', false);
                                                    $("[name='<?php echo $value2;?>']").prop('disabled', true);
                                                }
                                            });
                                            </script>
                                            <?php 
                                            }
                                        }
                                        }
                                        }
                                        break;
                                    case 'readonly':
                                        if($question->code=='B2')
                                             echo '<input type="text" readonly="readonly" name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control" value="'.$users[0]->study_id.'">';
                                        else echo '<input type="text" readonly="readonly" name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control">';
                                        break;
                                    default:
                                        echo '<div>answer type not available</div>'.$question->qtype;
                                    break;
                                }
                                }else{

                                    if(!empty($query_answers)){
                                        // echo '<br>qtype: '.$question->qtype;
                                    switch ($question->qtype) {
                                        case 'datetime':
                                            if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.date('F d, Y',strtotime($query_answers[$question->id])).'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                            break;
                                        case 'radio':
                                            if(!empty($query_answers[$question->id]))
                                                // echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                                echo '<br><label>Ans) '.$question_answers[$query_answers[$question->id]].'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                        break;
                                        case 'combo':
                                                // if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                                if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.$question_answers[$query_answers[$question->id]].'</lable>';
                                                else echo '<br><label>Ans) -- Not Answered --';
                                        break;
                                        default:
                                            if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                        break;
                                    }
                                    }else{
                                        echo '<br><label>Ans) -- Not Answered --</lable>';
                                    }
                                }
                                echo '</div>';

                            }
                            }
                        }
                        }
                        if(empty($visit_uuid)){?>
                        <div class="form-group">
                            <button type="submit" id="submit" class="btn btn-primary btn-lg">Submit</button>
                            <input type="hidden" id="visit_uid" value="<?php echo $visit_uid;?>" class="form-control" required="required">
                            <input type="hidden" id="lmp_date" value="<?php echo $users[0]->lmp_date;?>" class="form-control" required="required">
                            <button type="button" name="save" id="save" class="btn btn-lg btn-primary">Save for Later</button>
                            <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake">
                        </div>
                        <?php 
                        }
                    }
                    }
                    ?>
                </div>
                <?php if(empty($visit_uuid)){?></form><?php }?>
            </div>
        </div>
    </div>
</div>
<?php }


if(empty($visit_uuid)){?>
<script type="text/javascript">

    $('#save').click(function () {
        
        var baseURL = '<?php echo Config::get('constants.PATH');?>';
        $('#snake').show();
        setTimeout(function() {
            $('#snake').hide();
            toastr.success('Saved for later...');
            setTimeout(function() {
                window.location.href = baseURL+"user-questionnaire";
            }, 3000);
        }, 5000);
        return false;
    });

    $("form").submit(function(){

        var query_string = $('form').serialize();
        var baseURL = '<?php echo Config::get('constants.PATH');?>';
        var form_data = new FormData(); 
        form_data.append('visit_uid',$('#visit_uid').val());
        form_data.append('query_string',query_string);
        $.ajax({
            dataType: 'json',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: baseURL+'ajax-save-visits',
            data: form_data,
            beforeSend: function(){
              $('#snake').show();
              console.log('sending data...');
            },
            success: function(result) {
                // var result = $.parseJSON(result);
                if(result.result.status=='success'){
                    $('#snake').hide();
                    toastr.success(result.result.message);
                    setTimeout(function() {
                        window.location.href = baseURL+"user-visits";
                    }, 5000);
                    return false;
                }else{
                    $('#snake').hide();
                    toastr.error(result.result.message);
                    return false;
                }
            }
        });
    return false;
    });
</script>
<?php }?>
@endsection
