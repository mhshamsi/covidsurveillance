@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Push Notifications: <?php echo $users[0]->first_name.' '.$users[0]->last_name;?></h2>
            <div class="page-breadcrumb">
                <ol class="breadcrumb">
                    <!-- <li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li> -->
                    <!-- <li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li> -->
                    <!-- <li class="breadcrumb-item active" aria-current="page" style="color: #333333">User Push Notifications</li> -->
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>    
                                <th><div class="form-group">Categories:</div></th>
                                <th>
                                    <div class="form-group">
                                        <select id="category_uid" onchange="get_notifications(this.value,'<?php echo $user_uid;?>');" class="form-control">
                                            <option value="">Select Category</option>
                                        <?php if(!empty($notification_categories)){ $count=0;
                                        foreach ($notification_categories as $uid => $name) {
                                            if($count==0) $selected = ' selected="selected"'; else $selected='';
                                            echo '<option value="'.$uid.'"'.$selected.'>'.$name.'</option>'; $count++;
                                        }}?></select>
                                    </div>
                                </th>
                                <th rowspan="2" width="95">No of Times Message Sent</th>
                                <th rowspan="2">Action</th>
                            </tr>
                            <tr>
                                <th>S.No</th>
                                <th>Notifications
                                    <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake"></th>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($push_notifications->count()>0){
                            // echo '<pre>'; print_r($notification_categories); echo '</pre>'; exit();
                            $serial=0;
                            foreach ($push_notifications as $push_notification) {?>
                            <tr>
                                <td style="color: #333333"><?php echo ++$serial;?></td>
                                <td style="color: #333333"><?php echo $push_notification->notification;?></td>
                                <td><span class="btn btn-success"><?php echo $push_notification->count;?></span></td>
                                <td><button type="button" onclick="send_notification('<?php echo $push_notification->uid;?>','<?php echo $user_uid;?>')" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Send</button></td>
                            </tr>
                            <?php }}else{?>
                            <tr>
                                <td colspan="3">User push notifications are not available.</td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function get_notifications(category_uid,user_uid){

    if($.trim(category_uid) == ''){
        toastr.error('Category cannot be blank.');
        $('#category_uid').focus();
        return false;
    };
    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    form_data.append('category_uid',category_uid); 
    form_data.append('user_uid',user_uid);
    $.ajax({
        dataType: 'text',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-user-push-notification',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            $('#snake').hide();
            $("table").find("tr:gt(1)").remove();
            $("tbody").append(result);
            // $("tbody").append(result);
            return false;

            // if(result.result.status=='success'){
            //     $('#snake').hide();
            //     $("table").find("tr:gt(1)").remove();
            //     $("tbody").append(result);
            // }else{
            //     $('#snake').hide();
            //     toastr.error(result.result.message);
            // }
        }
    });
    return false;


}
function send_notification(notification_uid,user_uid){

    // alert(notification_uid +': '+user_uid);    
    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    form_data.append('notification_uid',notification_uid); 
    form_data.append('user_uid',user_uid); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-add-user-notification',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            if(result.result.status=='success'){
                $('#snake').hide();
                toastr.success(result.result.message);
                setTimeout(function() {
                    window.location.href = baseURL+"user-push-notification/"+user_uid;
                }, 10);
            }else{
                $('#snake').hide();
                toastr.error(result.result.message);
            }
        }
    });
    return false;
}
</script>
@endsection