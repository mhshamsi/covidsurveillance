<table>
  <thead>
    <tr>
      @foreach ($titles as $title)
      <th>{{$title}}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
    @foreach ($data as $row)
    <tr>
      @foreach ($titles as $item => $title)
      <td>{{convert_for_report($row->$item, $item)}}</td>
      @endforeach
    </tr>
    @endforeach
  </tbody>
</table>