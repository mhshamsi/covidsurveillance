@extends('layout.default') 
@section('content')
<!-- ============================================================== --> 
<!-- pageheader --> 
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php if($_SESSION['role']=='Admin'){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Food Diary<?php echo ': '.$users[0]->first_name.' '.$users[0]->last_name;?></h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="./admin-users" class="breadcrumb-link">Users</a></li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page" style="color: #333333;">Food Diary</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php }?>
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<?php if(!empty($user_days[0]->date_start) and $user_days[0]->date_start > date("Y-m-d") or ( empty($users[0]->fooddiary_approved_at) or count($user_days)==0 )){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Yeh diary is waqt muyassir nahi hai. Aap is diary ko apne aglay tehqeeqi visit se aik haftey pehle pur kar sakenge.</h3>
            </div>
        </div>
    </div>
</div>
<?php }else{?>
<div class="row">
    <div class="col-xl-12 col-lg-4 col-md-4 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Visit - <?php echo $visit_types[$user_days[0]->visit_type_id]?></h3><br>
                <table class="table">
                    <tbody>
                    <?php if(!empty($user_days)){
                    foreach ($user_days as $user_day) {?>
                        <tr>
                            <td><a href="/user-food-diary-detail/<?php echo $user_day->uid?><?php echo (!empty($visit_uid)?'/'.$visit_uid:'')?>" class="btn btn-primary"><?php echo ucfirst($user_day->name);?></a></td>
                            <td style="color: #333333;"><?php echo date('l, F d, Y',strtotime($user_day->date));?></td>
                        </tr>
                    <?php }}?>                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php }?>
@endsection