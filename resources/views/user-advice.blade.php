@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<?php if($_SESSION['role']=='Admin'){?>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Advice</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li> -->
                        <!-- <li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page">Advice</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php }?>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<?php if($personal_notifications->count()>0){
$serial=0; 
foreach ($personal_notifications as $personal_notification) {
// echo '<pre>'; print_r($personal_notification); exit();
    ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3>Advice - <?php echo ++$serial;?></h3>
                <p style="font-size: 16px; color: #333333;"><?php echo $personal_notification->notification;?>
                <br /><?php echo date('F d, Y',strtotime($personal_notification->created_at));?></p>
            </div>
        </div>
    </div>
</div>
<?php }}else{?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3>Advice</h3>
                <p style="font-size: 16px; color: #333333;">Advice is not available.</p>
            </div>
        </div>
    </div>
</div>
<?php }?>        
@endsection