@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Ineligible Participants</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item active" aria-current="page" style="color: #333333;">Participants</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive" style="min-height:350px;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if(count($users)>0){
                            // echo '<pre>'; print_r($users); echo '</pre>'; exit();
                        $serial=0;
                        foreach ($users as $user) {
                            ?>
                        <tr>
                            <td style="color: #333333"><?php echo ++$serial;?></td>
                            <td style="color: #333333"><?php echo $user->first_name.' '.$user->last_name;?></td>
                            <td style="color: #333333">
                                <button type="button" class="btn btn-primary float-right" onclick="window.location.href='/user-profile/<?php echo $user->uid;?>'">Profile</button>
                            </tr>
                        <?php }}else{?>
                        <tr>
                            <td>
                                Ineligible Participants are not available.
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
                <nav aria-label="Page navigation example" style="display:none;">
                    <ul class="pagination justify-content-end">
                        <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
            
        </div>
    </div>
</div>
<script src="<?php echo asset('js/purumeed.js');?>"></script>
@endsection