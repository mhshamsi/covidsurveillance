@extends('layout.default') 
@section('content')
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Recommendations</h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <!-- <li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li> -->
                                            <!-- <li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li> -->
                                            <!-- <li class="breadcrumb-item active" aria-current="page" style="color: #333333">Recommendations</li> -->
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php 
                        if(!empty($recommendations)){
                        $serial=0;
                        $json_recommendations = json_decode($recommendations->recommendations,true);
                        foreach ($json_recommendations as $my_recommendations) {
                        // echo '<pre>'; print_r($my_recommendations); exit();?>
                        <div class="card">
                            <?php 
                            if(!empty($my_recommendations)){
                                foreach ($my_recommendations as $my_recommendation) {?>
                            <div class="card-body">
                                <h3>Recommendation - <?php echo (++$serial);?></h3>
                                <p style="font-size: 16px; color: #333333;"><?php echo $my_recommendation;?></p>
                            </div><?php 
                                }
                            }
                            ?>
                        </div>
                        <?php }}else{?>
                        <div class="card">
                            <div class="card-body">
                                <p style="font-size: 16px; color: #333333;">Recommendations are not available for this visit.</p>
                            </div>
                        </div><?php 
                        }

                        ?>
                    </div>
                </div>
@endsection