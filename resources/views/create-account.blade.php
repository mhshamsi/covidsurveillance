<!doctype html>
<html lang="en">

 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Nutrition Design One</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/libs/css/style.css">
    <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <script>
        function checkEthnicity(val) {
            var element = document.getElementById('ethnicity');
            if (val == 'other') {
                element.style.display = 'block';
            }
            else
            element.style.display = 'none';
        }

        function checkReligion(val) {
            var element = document.getElementById('religion');
            if (val == 'other') {
                element.style.display = 'block';
            }
            else
            element.style.display = 'none';
        }
    </script>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
       <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg fixed-top" style="background-color:#ffffff">
                <a class="navbar-brand" href="../pages/login.html">Nutrition</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name">John Abraham</h5>
                                    <span class="status"></span><span class="ml-2">Available</span>
                                </div>
                                <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a>
                                <a class="dropdown-item" href="./login.html"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="">
            <div class="container-fluid dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Create An Account </h2>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h3>Tehqeeq ka sawal Nama (Baseline)</h3>
                                                <h4 class="">Section A: Ahliyat aur Ijazt Nama</h4>
                                                <div class="form-group">
                                                    <label>Kya aap hamal sy hain?</label> &nbsp;
                                                    <div class="">
                                                        <input type="radio" name="optionsHamalSayHain" id="hamalSayJeeHaa" value="jeehaa" checked>
                                                        <label for="hamalSayJeeHaa">Jee Haa</label>
                                                        <input type="radio" name="optionsHamalSayHain" id="hamalSayJeeNahi" value="jeenahi">
                                                        <label for="hamalSayJeeNahi">Jee Nahi</label>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label for="">Aap ke akhri haiz ki muddat ka pehla din kon sa tha? Agar aap ko sahai tareekh yaad nahe hai to andazan tareekh bataiye?</label>
                                                        <input type="date" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label for="">Akhri haiz ki muddat ky mutabiq bachy ki pedaish ki mutawaqqa tareekh kon si hai?</label>
                                                        <input type="date" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label for="">Ultrasound ke mutabiq bache ki pedaish ki mutawaqqa tareekh kon si hai?</label>
                                                        <input type="date" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label for="">Kya aap ky paas zati smartphone ya mobile phone internet ki saholat ke sath moujud hai?</label> &nbsp;
                                                        <div class="">
                                                            <input type="radio" name="optionsSmartPhoneMobile" id="smartPhoneMobileJeeHaa" value="jeehaa" checked>
                                                            <label for="smartPhoneMobileJeeHaa">Jee Haa</label>
                                                            <input type="radio" name="optionsSmartPhoneMobile" id="smartPhoneMobileJeeNahi" value="jeenahi">
                                                            <label for="smartPhoneMobileJeeNahi">Jee Nahi</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label for="">Kya aap muqami asolon ke mutabiq is tehqeeq men shirkat karne ke liye razamand hain?</label> &nbsp;
                                                        <div class="">
                                                            <input type="radio" name="optionsMuqamiAsolonRazamand" id="MuqamiAsolonRazamandJeeHaa" value="jeehaa" checked>
                                                            <label for="MuqamiAsolonRazamandJeeHaa">Jee Haa</label>
                                                            <input type="radio" name="optionsMuqamiAsolonRazamand" id="MuqamiAsolonRazamandJeeNahi" value="jeenahi">
                                                            <label for="MuqamiAsolonRazamandJeeNahi">Jee Nahi</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label for="">Kya aap english ya urdu men parh likh sakhtay hain?</label> &nbsp;
                                                        <div class="">
                                                            <input type="radio" name="optionsEnglishUrduParhLikh" id="EnglishUrduParhLikhJeeHaa" value="jeehaa" checked>
                                                            <label for="EnglishUrduParhLikhJeeHaa">Jee Haa</label>
                                                            <input type="radio" name="optionsEnglishUrduParhLikh" id="EnglishUrduParhLikhJeeNahi" value="jeenahi">
                                                            <label for="EnglishUrduParhLikhJeeNahi">Jee Nahi</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="alert alert-light text-center" role="alert">
                                                        <strong> Agar mandarja bala sawalat men kisi bhi sawal ka jawab “nahi” hai to shirkat kar shirkat ki mustahiq nahi hai.</strong>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kay aap ko ziabtees hai?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsZiabteesHai" id="ZiabteesHaiJeeHaa" value="jeehaa" checked>
                                                                <label for="ZiabteesHaiJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsZiabteesHai" id="ziabteesHaiJeeNahi" value="jeenahi">
                                                                <label for="ziabteesHaiJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya aap ko dil aur khoon ki ragon ki bimari hai? (feshar khon ka badhna / high blood pressure, angina)</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsDilRagonBinari" id="dilRagonBimariJeeHaa" value="jeehaa">
                                                                <label for="dilRagonBimariJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsDilRagonBinari" id="dilRagonBimariJeeNahi" value="jeenahi">
                                                                <label for="dilRagonBimariJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya aap kisi degar bimari ki waja sy gizai parhaiz per hain jesa ke ziabtees ya dil aur khoon ki ragon ki bimari?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsDegerBimariParhaiz" id="degarBimariParhaizJeeHaa" value="jeehaa">
                                                                <label for="degarBimariParhaizJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsDegerBimariParhaiz" id="degarBimariParhaizJeeNahi" value="jeenahi">
                                                                <label for="degarBimariParhaizJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya aap ksi bimari ki adwiyat istamal kar rahe hain jesa ke feshar khon ka badhna/high blood pressure?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsBimariAdwiyatIstamalFeshar" id="bimariAdwiyatIstamalFesharJeeHaa" value="jeehaa">
                                                                <label for="bimariAdwiyatIstamalFesharJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsBimariAdwiyatIstamalFeshar" id="bimariAdwiyatIstamalFesharJeeNahi" value="jeenahi">
                                                                <label for="bimariAdwiyatIstamalFesharJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya aap ksi bimari ki adwiyat istamal kar rahe hain jesa ke Glucophage?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsBimariAdwiyatIstamalGlucophage" id="bimariAdwiyatIstamalGlucophageJeeHaa" value="jeehaa">
                                                                <label for="bimariAdwiyatIstamalGlucophageJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsBimariAdwiyatIstamalGlucophage" id="bimariAdwiyatIstamalGlucophageJeeNahi" value="jeenahi">
                                                                <label for="bimariAdwiyatIstamalGlucophageJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya aap koi dawa istamal kar rahe hain jesa ke Ascard?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsDawaIstamalAscard" id="dawaIstamalAscardJeeHaa" value="jeehaa">
                                                                <label for="dawaIstamalAscardJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsDawaIstamalAscard" id="dawaIstamalAscardJeeNahi" value="jeenahi">
                                                                <label for="dawaIstamalAscardJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya aap kisi khatarnak bimari ka shikar hain jese ke gurde ya jigar ki bimari,SLE?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsKhatarnakBimariGurdeJigar" id="khatarnakBimariGurdeJigarJeeHaa" value="jeehaa">
                                                                <label for="khatarnakBimariGurdeJigarJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsKhatarnakBimariGurdeJigar" id="khatarnakBimariGurdeJigarJeeNahi" value="jeenahi">
                                                                <label for="khatarnakBimariGurdeJigarJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="alert alert-light text-center" role="alert">
                                                            <strong> Agar mandarja bala sawalat men kisi bhi sawal ka jawab “han” hai to khatoon shirkat ki mustahiq nahi hain. </strong>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya khatoon shirkat ki mustahiq hai?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsKhatoonShirkatMustahiqHai" id="khatoonShirkatMustahiqHaiJeeHaa" value="jeehaa">
                                                                <label for="khatoonShirkatMustahiqHaiJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsKhatoonShirkatMustahiqHai" id="khatoonShirkatMustahiqHaiJeeNahi" value="jeenahi">
                                                                <label for="khatoonShirkatMustahiqHaiJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya khatoon shirkat karna chahti hain aur bachy ki pedaish ke pehle saal tak tehqeeq men shamil rehna chahti hain?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsKhatoonBachyPedaishTahqeeqShamil" id="khatoonBachyPedaishTahqeeqShamilJeeHaa" value="jeehaa">
                                                                <label for="khatoonBachyPedaishTahqeeqShamilJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsKhatoonBachyPedaishTahqeeqShamil" id="khatoonBachyPedaishTahqeeqShamilJeeNahi" value="jeenahi">
                                                                <label for="khatoonBachyPedaishTahqeeqShamilJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kya unho ne malomati ijazt namay ko parha, samjha aur dastakhat kar lia hai?</label> &nbsp;
                                                            <div class="">
                                                                <input type="radio" name="optionsMalomatiIjazatDastakhatKarLiaHai" id="malomatiIjazatDastakhatKarLiaHaiJeeHaa" value="jeehaa">
                                                                <label for="malomatiIjazatDastakhatKarLiaHaiJeeHaa">Jee Haa</label>
                                                                <input type="radio" name="optionsMalomatiIjazatDastakhatKarLiaHai" id="malomatiIjazatDastakhatKarLiaHaiJeeNahi" value="jeenahi">
                                                                <label for="malomatiIjazatDastakhatKarLiaHaiJeeNahi">Jee Nahi</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="alert alert-light text-center" role="alert">
                                                            <strong> Agar mandarja bala sawalat men kisi bhi sawal ka jawab “han” hai to khatoon tehqeeq men shirkat kar sakti hai </strong>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <h4 class="">Section B: Shirkat kar se mutaliq malumat: Shirkat kar pur karain</h4>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Tareekh</label>
                                                            <input type="date" name="" class="form-control" id="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Tehqeeq ka shanakhti number</label>
                                                            <input type="number" name="" class="form-control" id="" placeholder="4220112345667">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Medical record number</label>
                                                            <input type="number" name="" class="form-control" id="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Naam</label>
                                                            <input type="text" name="" class="form-control" id="" placeholder="Naam">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Walid /Khawand ka naam</label>
                                                            <input type="text" name="" class="form-control" id="" placeholder=Walid /Khawand ka naam">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Zaat</label>
                                                            <select name="" id="" class="form-control" onchange="checkZaat(this.value)">
                                                                <option value="">choose...</option>
                                                                <option value="sindhi">Sindhi</option>
                                                                <option value="punjabi">Punjabi</option>
                                                                <option value="balochi">Balochi</option>
                                                                <option value="pakhtoon">Pakhtoon</option>
                                                                <option value="muhajir">Muhajir</option>\
                                                                <option value="memon">Memon</option>
                                                                <option value="gujrati">Gujrati</option>
                                                                <option value="degarZaat">Degar (wazahat karain)</option>                                                         
                                                            </select>
                                                            <input type="text" name="zaat" class="form-control" id="zaat" style="display:none;" placeholder="zaat wazahat karain">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Mazhab</label>
                                                            <select name="" id="" class="form-control" onchange="checkMazhab(this.value)">
                                                                <option value="">choose...</option>
                                                                <option value="islam">Islam</option>
                                                                <option value="hindu">Hindu</option>
                                                                <option value="esai">Esai</option>
                                                                <option value="degarMazhab">Degar (wazahat karain)</option>                                                         
                                                            </select>
                                                            <input type="text" name="mazhab" class="form-control" id="mazhab" style="display:none;" placeholder="mazhab wazahat karain">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="addressInput">Address</label>
                                                            <textarea class="form-control" name="" id="addressInput" cols="30" rows="2"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="zipCodeInput">Zip/Elaqe ka code</label>
                                                            <input type="text" class="form-control" id="zipCodeInput">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="rabtaGharNumber">Rabty ka number (ghar ka number)</label>
                                                            <input type="number" class="form-control" id="rabtaGharNumber">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="rabtaMobileNum">Rabty ka number (mobile ka number)</label>
                                                            <input type="number" class="form-control" id="zipCodeInput">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Tarikh</label>
                                                            <input type="date" class="form-control"> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="umerSaalonMen">Umer (saalon men)</label>
                                                            <input type="number" class="form-control" id="umerSaalonMen">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Aap ki taleemi qabliyat</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="gairTaleemYafta">Gair taleem yafta</option>
                                                                <option value="primary">Primary</option>
                                                                <option value="secondary">Secondary</option>
                                                                <option value="college">College</option>
                                                                <option value="university">University</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Khawand ki taleemi qabliyat</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="gairTaleemYafta">Gair taleem yafta</option>
                                                                <option value="primary">Primary</option>
                                                                <option value="secondary">Secondary</option>
                                                                <option value="college">College</option>
                                                                <option value="university">University</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Aap ka paisha (agar berozgar hain to sawal B15a ko chor dain</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="nokriPaisha">Nokri paisha</option>
                                                                <option value="berozgar">Berozgar</option>
                                                                <option value="apnaKarobar">Apna karobar</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                           <label for="paishayTaaluq">Agar kaam karte hain tu aap kis paishay se taaluq rakhte hain?</label>
                                                            <input type="text" class="form-control" id="paishayTaaluq">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Khawand ka paisha</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="nokriPaisha">Nokri paisha</option>
                                                                <option value="berozgar">Berozgar</option>
                                                                <option value="apnaKarobar">Apna karobar</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Ahl-e-khana ki mahana aamdani kitni hai?</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="50000SeKam">50000 se kam</option>
                                                                <option value="50000Se100000Tak">50000 se 100000 tak</option>
                                                                <option value="100000Se200000Tak">100000 se 200000 tak</option>
                                                                <option value="200000Se500000Tak">200000 se 500000 tak</option>
                                                                <option value="500000SeZiada">500000 se ziada</option>
                                                                <option value="JawabNahiDiya">Jawab nahi diya</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Kis qisam ke ghar men rehte hain?</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="ZatiMilkiyat">Zati milkiyat</option>
                                                                <option value="KirayePer">Kiraye per</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="">Gharana kis tarah ka hai?</label>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">choose...</option>
                                                                <option value="infaradiKhandaniNizam">Infaradi khandani nizam</option>
                                                                <option value="mushtarkaKhandaniNizam">Mushtarka khandani nizam</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="GharAfradTaadad">Ghar men kitne afrad rehte hain?</label>
                                                            <input type="number" class="form-control" id="GharAfradTaadad">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="username">Username</label>
                                                            <input type="text" class="form-control" id="username">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="password">Password</label>
                                                            <input type="text" class="form-control" id="password">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label for="confirmPassword">Confirm Password</label>
                                                            <input type="text" class="form-control" id="confirmPassword">
                                                        </div>
                                                    </div>
                                                    <br>
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            Copyright © 2019 Nutrition. All rights reserved.
                        </div>
                        <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                               
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets/libs/js/main-js.js"></script>
</body>
 
</html>