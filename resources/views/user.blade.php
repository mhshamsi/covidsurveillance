                @extends('layout.default') 
                @section('content')
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <meta name="csrf-token" content="{{ csrf_token() }}" />
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="page-header">
                                <h2 class="pageheader-title">Register User</h2>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="name">First Name</label>
                                        <input type="text" class="form-control" id="first_name" value="<?php echo (!empty($user->first_name)?$user->first_name:'')?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="name">Last Name</label>
                                        <input type="text" class="form-control" id="last_name" value="<?php echo (!empty($user->last_name)?$user->last_name:'')?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="email_address">Email Address</label>
                                        <input type="email" id="email_address" value="<?php echo (!empty($user->email_address)?$user->email_address:'')?>" class="form-control" value="johndoe@mail.com">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" id="username" value="<?php echo (!empty($user->username)?$user->username:'')?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" id="password" value="<?php echo (!empty($user->password)?$user->password:'')?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="role_id">User Role</label>
                                        <?php //echo '<pre>'; print_r($user_roles); exit();?>
                                        <select id="role_id" class="form-control"><?php 
                                        if(!empty($user_roles)){
                                            echo '<option value="">Select Role</option>';
                                            foreach ($user_roles as $key => $role) {
                                                echo '<option value="'.$key.'"'.((!empty($user->role_id) and $key==$user->role_id)?' selected="selected"':'').'>'.ucfirst(strtolower($role)).'</option>';
                                            }
                                        }
                                        ?></select>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <button type="submit" id="submit" class="btn btn-primary btn-lg">Submit</button>
                                    <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake"></th>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <script type="text/javascript">
        $("#submit").click(function(){
            if($.trim($('#first_name').val()) == ''){
                toastr.error('First Name cannot be blank.');
                $('#first_name').focus();
                return false;
            };
            if($.trim($('#last_name').val()) == ''){
                toastr.error('Last Name cannot be blank.');
                $('#last_name').focus();
                return false;
            };
            if($.trim($('#email_address').val()) == ''){
                toastr.error('Email Address cannot be blank.');
                $('#email_address').focus();
                return false;
            };
            if($.trim($('#username').val()) == ''){
                toastr.error('Username cannot be blank.');
                $('#username').focus();
                return false;
            };
            if($.trim($('#password').val()) == ''){
                toastr.error('Password cannot be blank.');
                $('#password').focus();
                return false;
            };
            if($.trim($('#role_id').val()) == ''){
                toastr.error('Select Role');
                $('#role_id').focus();
                return false;
            }            

            var baseURL = '<?php echo Config::get('constants.PATH');?>';
            var form_data = new FormData(); 
            form_data.append('first_name',$('#first_name').val()); 
            form_data.append('last_name',$('#last_name').val()); 
            form_data.append('email_address',$('#email_address').val()); 
            form_data.append('username',$('#username').val()); 
            form_data.append('password',$('#password').val()); 
            form_data.append('role_id',$('#role_id').val()); 
            $.ajax({
                dataType: 'json',
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: baseURL+'ajax-register',
                data: form_data,
                beforeSend: function(){
                    $('#snake').show();
                    console.log('sending data...');
                },
                success: function(result) {
                    $('#snake').hide();
                    if(result.result.status=='success'){
                        toastr.success('User added successfully');
                        setTimeout(function() {
                            window.location.href = baseURL;
                        }, 5000);
                        return false;
                    }else{
                        toastr.error(result.result.message);
                        return false;
                    }
                }
            });
        });
    </script>
                @endsection