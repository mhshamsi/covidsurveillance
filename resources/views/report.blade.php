@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h2>Report <a style="font-size:16px;" href="/export">[Export]</a></h2>
            <div class="card-body" style="overflow: scroll;height: 500px;">
				<div class="row" style="display:none;">
                    <div class="col-md-3">
                        <input type="text" name="participant_name" id="participant_name" class="form-control form-control-custom" placeholder="Search By Participant Name">
                        <input type="hidden" name="base_url" id="base_url" value="<?php echo Config::get('constants.PATH');?>">
                        <input type="hidden" name="page" id="page" value="users">
					</div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="role_id" id="role_id" class="form-control">
                            <?php 
                            // if(!empty($user_roles)){
                            //     echo '<option value="">Search By Role</option>';
                            //     foreach ($user_roles as $key => $role) {
                            //         echo '<option value="'.$key.'"'.((!empty($user->role_id) and $key==$user->role_id)?' selected="selected"':'').'>'.ucfirst(strtolower($role)).'</option>';
                            //     }
                            // }
                            ?>
                            </select>
                        </div>
                    </div>
					<div class="col-md-2">
						<img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display:none;" alt="snake">
					</div>
                    <div class="col-md-2">
                        <button type="button" name="search" id="search" class="btn btn-primary btn-small float-right">Search</button>
                    </div>
					<div class="col-md-2">
						<button type="button" class="btn btn-info float-right" onclick="register()">Create Account</button>
					</div>
				</div>
                
                
                <div class="clearfix"></div><br>
                <div class="register">
                    <table class="table">
                        <thead>
                                <?php echo $title;?>
                        </thead>
                        <tbody>
                                <?php echo $data?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo asset('js/purumeed.js');?>"></script>
@endsection