@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Participants</h2>
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page" style="color: #333333;">Participants</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
				<div class="row">
                    <div class="col-md-4">
                        <input type="text" name="" id="study_id" class="form-control form-control-custom" placeholder="Search By Study ID">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="" id="participant_name" class="form-control form-control-custom" placeholder="Search By Participant Name">
                        <input type="hidden" name="base_url" id="base_url" value="<?php echo Config::get('constants.PATH');?>">
                        <input type="hidden" name="page" id="page" value="participants">
                    </div>
                    <div class="col-md-2">
                        <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display:none;" alt="snake">
                    </div>
                    <div class="col-md-2">
                        <button type="button" name="search" id="search" class="btn btn-primary btn-small float-right">Search</button>
                    </div>
                </div>
				<br>
                <div class="table-responsive" style="min-height:350px;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Actions</th>
                            <th><a href="add_new">Add New</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($users)>0){
                        $serial=0;
                        foreach ($users as $user) {
                            // echo '<pre>'; print_r($user); echo '</pre>'; continue;
                            ?>
                        <tr>
                            <td style="color: #333333"><?php echo ++$serial;?></td>
                            <td style="color: #333333"><?php echo $user->first_name.' '.$user->last_name;?></td>
                            <td>
                                <a href="/user-profile/<?php echo $user->uid;?>">Edit</a>
                            </td>
                        </tr>
                        <?php }}else{?>
                        <tr>
                            <td>
                                Participants are not available.
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
                <nav aria-label="Page navigation example" style="display:none;">
                    <ul class="pagination justify-content-end">
                        <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
            
        </div>
    </div>
</div>
<script src="<?php echo asset('js/purumeed.js');?>"></script>
@endsection