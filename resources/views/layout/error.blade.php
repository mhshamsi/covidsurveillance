<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="< ?php echo asset('assetsx/images/favicon.png');?>"> -->
    <title>TeekoPlus</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo asset('assetsx/css/lib/bootstrap/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo asset('assetsx/css/helper.css');?>" rel="stylesheet">
    <link href="<?php echo asset('assetsx/css/style.css');?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <script src="<?php echo asset('assetsx/js/lib/jquery/jquery.min.js');?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo asset('assetsx/js/lib/bootstrap/js/popper.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo asset('assetsx/js/jquery.slimscroll.js');?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo asset('assetsx/js/sidebarmenu.js');?>"></script>
    <!--stickey kit -->
    <script src="<?php echo asset('assetsx/js/lib/sticky-kit-master/dist/sticky-kit.min.js');?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo asset('assetsx/js/custom.min.js');?>"></script>

    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light" style="padding-bottom:10px;padding-top:5px;">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b><img src="<?php echo asset('assetsx/images/logo_01.png');?>" alt="homepage" class="dark-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img src="<?php echo asset('assetsx/images/logo-text_01.png')?>" alt="homepage" class="teekoplus-logo" /></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- End Messages -->
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo asset('assetsx/images/users/5.jpg');?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <!-- <li><a href="#"><i class="ti-user"></i> Profile</a></li> -->
                                    <!-- <li><a href="#"><i class="ti-wallet"></i> Balance</a></li> -->
                                    <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                                    <!-- <li><a href="#"><i class="ti-settings"></i> Setting</a></li> -->
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" style="padding-top:10px !important;">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li><a href="<?php echo Config::get('constants.PATH');?>dashboard"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li><a href="<?php echo Config::get('constants.PATH');?>monthly-report"><i class="fa fa-envelope"></i><span class="hide-menu">Monthly Report</span></a></li>
                        <li><a href="<?php echo Config::get('constants.PATH');?>detailed-report"><i class="fa fa-bar-chart"></i><span class="hide-menu">Detailed Report</span></a></li>
            <!-- <li><a href="#"><i class="fa fa-suitcase"></i><span class="hide-menu">Components</span></a></li> -->
                        <!-- <li><a href="#"><i class="fa fa-wpforms"></i><span class="hide-menu">Forms</span></a></li> -->
                        <!-- <li><a href="#"><i class="fa fa-table"></i><span class="hide-menu">Tables</span></a></li> -->
                        <!-- <li><a href="#"><i class="fa fa-book"></i><span class="hide-menu">Pages</span></a></li> -->
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            @yield('content')
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo asset('assetsx/js/lib/datatables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo asset('assetsx/js/lib/datatables/datatables-init.js');?>"></script>
</body>

</html>