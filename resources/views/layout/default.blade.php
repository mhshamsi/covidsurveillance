<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="< ?php echo asset('assets/images/favicon.png');?>"> -->
    <title>Hayat</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo asset('assets/css/lib/bootstrap/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo asset('assets/css/helper.css');?>" rel="stylesheet">
    <link href="<?php echo asset('assets/css/style.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css" integrity="sha256-BJ/G+e+y7bQdrYkS2RBTyNfBHpA9IuGaPmf9htub5MQ=" crossorigin="anonymous" />
    <link rel="stylesheet" href="<?php echo asset('js/toastr/toastr.css');?>">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="<?php echo asset('js/toastr/toastr.js');?>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <script src="<?php echo asset('assets/js/lib/jquery/jquery.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/bootstrap/js/popper.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/jquery.slimscroll.js');?>"></script>
    <script src="<?php echo asset('assets/js/sidebarmenu.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/custom.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/chosen-js/chosen.jquery.js');?>"></script>
    
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        @include('layout.header')
        <!-- End header header -->
        <!-- Left Sidebar  -->
        @include('layout.sidebar')        
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            @yield('content')
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo asset('assets/js/lib/datatables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo asset('assets/js/lib/datatables/datatables-init.js');?>"></script>
</body>

</html>