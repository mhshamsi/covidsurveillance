        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" style="padding-top:10px !important;">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                            <li><a href="/dashboard"><i class="fa fa-area-chart" aria-hidden="true"></i><span class="hide-menu">Dashboard</span></a></li>
                            <li><a href="/users"> <i class="fa fa-suitcase" aria-hidden="true"></i><span class="hide-menu">Users</span></a></li>
                            <!-- <li><a href="/user"><i class="fa fa-envelope"></i><span class="hide-menu">Register User</span></a></li> -->
                            <!-- <li><a href="/admin-profile"><i class="fa fa-suitcase"></i><span class="hide-menu">Profile</span></a></li> -->
                            <li><a href="/report"><i class="fa fa-line-chart"></i><span class="hide-menu">Report</span></a></li>
                            <li><a href="/logout"><i class="fa fa-suitcase"></i><span class="hide-menu">Logout</span></a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>