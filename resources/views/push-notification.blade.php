@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Push Notifications</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item active" aria-current="page" style="color: #333333;">Push Notification</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>    
                                <th>
                                    <div class="form-group">Categories:</div>
                                </th>
                                <th>
                                    <div class="form-group">
                                        <select id="category_uid" onchange="get_notifications(this.value);" class="form-control">
                                            <option value="">Select Category</option>
                                        <?php if(!empty($notification_categories)){ $count=0;
                                        foreach ($notification_categories as $uid => $name) {
                                            if($count==0) $selected = ' selected="selected"'; else $selected='';
                                            echo '<option value="'.$uid.'"'.$selected.'>'.$name.'</option>'; $count++;
                                        }}?></select>
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary float-right" onclick="add_notification()">Add Notification</button>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>S.No</th>
                                <th colspan="2">Notifications
                                    <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            // echo '<pre>'; print_r($push_notifications); echo '</pre>';
                            if($push_notifications->count()>0){
                            $serial=0;
                            foreach ($push_notifications as $push_notification) {?>
                            <tr>
                                <td style="color: #333333"><?php echo ++$serial;?></td>
                                <td colspan="2" style="color: #333333"><?php echo $push_notification->notification;?></td>
                            </tr>
                            <?php }}else{
                                ?>
                                <tr>
                                    <td colspan="3">Push notifications are not available.</td>
                                </tr>
                                <?php 
                            }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

function get_notifications(category_uid){

    if($.trim(category_uid) == ''){
        toastr.error('Category cannot be blank.');
        $('#category_uid').focus();
        return false;
    };
    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    form_data.append('category_uid',category_uid); 
    $.ajax({
        dataType: 'text',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-get-notification',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            $('#snake').hide();
            $("table").find("tr:gt(1)").remove();
            $("tbody").append(result);
            // $("tbody").append(result);
            return false;

            // if(result.result.status=='success'){
            //     $('#snake').hide();
            //     $("table").find("tr:gt(1)").remove();
            //     $("tbody").append(result);
            // }else{
            //     $('#snake').hide();
            //     toastr.error(result.result.message);
            // }
        }
    });
    return false;


}

function add_notification(){
    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    window.location.href = baseURL+"add-notification";
}
</script>
@endsection