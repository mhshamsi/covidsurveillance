@extends('layout.default') 
@section('content') 
<!-- ============================================================== --> 
<!-- pageheader --> 
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title">Create An Account</h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li>
						<li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li>
						<li class="breadcrumb-item active" aria-current="page" style="color:#333333;">Baseline Questionnaire</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- ============================================================== --> 
<!-- end pageheader --> 
<!-- ============================================================== -->
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="card">
			<div class="card-body">
                <form method="POST" name="register" id="register" data-persist="garlic" enctype="multipart/form-data">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <?php
                    if(!empty($categories)){
                    foreach ($categories as $category) {
                        if(!empty($category->question_subcategories)){
                        ?><h3><?php echo $category->category_roman;?></h3><?php 
                        foreach ($category->question_subcategories as $question_subcategory) {
                            if(!empty($question_subcategory->questions)){
                            $serial=0;
                            ?><h4 class=""><?php echo $question_subcategory->subcategory_roman;?></h4><?php 
                            foreach ($question_subcategory->questions as $question) {
                                echo '<div class="form-group">';
                                ?><label><?php echo ($question->code).') '.$question->urdu;?></label><?php
                                // echo '<br>qtype: '.$question->qtype;
                                switch ($question->qtype) {
                                    case 'datetime':
                                        echo '<input type="date" class="form-control" name="question_'.$question->id.'" id="question_'.$question->id.'" '.(($category->id==1)?' required="required"':'').'>';
                                        if($question->code=='B11'){
                                        ?><script>
                                        $('#question_28').blur(function(){
                                            var today   = new Date();
                                            var data_of_birth     = new Date($('#question_28').val()); //$('#dob').val()
                                            var timeDiff = today - data_of_birth;
                                            $('#question_29').val((timeDiff / (1000 * 60 * 60 * 24 * 365)).toFixed(0));
                                        });
                                        </script><?php 
                                        }
                                        break;
                                    case 'number':
                                        echo '<input type="number" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'" '.(($category->id==1)?' required="required"':'').'>';
                                        break;
                                    case 'minutes':
                                        echo '<input type="hidden" class="form-control" name="question_'.$question->id.'" id="answer_'.$question->id.'">';
                                        echo '<select class="form-control" name="question_'.$question->id.'_hours" id="hours_'.$question->id.'" '.(($category->id==1)?' required="required"':'').'>';
                                        for($i=0;$i<=12;$i++){
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                        echo '</select>';
                                        echo '<select class="form-control" name="question_'.$question->id.'_mins" id="minutes_'.$question->id.'">';
                                        for($i=0;$i<=60;($i+=15)){
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                        echo '</select>';
                                        break;
                                    case 'combo':
                                        if(!empty($question->answers)){ 
                                        echo '<div>';
                                        echo '<select class="form-control" name="question_'.$question->id.'" id="question_'.$question->id.'" '.(($category->id==1)?' required="required"':'').'>';
                                        echo '<option value="">Choose...</option>';
                                        foreach ($question->answers as $key => $answer) {
                                            echo '<option value="'.$answer->id.'">'.$answer->answer.'</option>';
                                        } 
                                        echo '</select>';
                                        echo '</div>';
                                        }
                                        break;
                                    case 'radio':
                                        if(!empty($question->answers)){
                                        echo '<div>';
                                        $style = (count($question->answers)>3)?' style="width:30px"':' style="width:89px"';
                                        foreach ($question->answers as $key => $answer) {
                                            echo '<input type="radio" name="question_'.$question->id.'" id="answer_'.$answer->id.'" value="'.$answer->id.'" '.(($category->id==1)?' required="required"':'').'>
                                            <label for="answer_'.$answer->id.'"'.$style.'>'.$answer->answer.'</label>';
                                        ?>                            
                                        <?php 
                                        } unset($style);echo '</div>';
                                        }
                                        break;
                                    case 'descriptive':
                                            echo '<input type="text" '.(($category->id==1)?' required="required"':'').' name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control" '.(($category->id==1)?' required="required"':'').'>';
                                        break;
                                    case 'readonly':
                                            echo '<input type="text" readonly="readonly" name="question_'.$question->id.'" id="question_'.$question->id.'" class="form-control">';
                                        break;
                                    default:
                                        echo '<div>answer type not available</div>'.$question->qtype;
                                    break;
                                }
                                echo '</div>';

                            }
                            }
                        }
                        }
                    
                    if($category->id==1){ #for registration only
                        ?><div class="form-group">
                        <label for="name">First Name</label>
                        <input type="text" class="form-control" id="first_name" required="required">
                    </div>
                    <div class="form-group">
                        <label for="name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" required="required">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" class="form-control">
                        <input type="hidden" id="user_role" value="Participant" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="username">Password</label>
                        <input type="password" id="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="username">Confirm Password</label>
                        <input type="password" id="confirm_password" class="form-control">
                    </div><?php 
                    }?>
                    <div class="form-group">
                        <button type="submit" id="submit" class="btn btn-primary btn-lg">Submit</button>
                        <button type="button" onclick="window.location='/control-panel'" class="btn btn-lg btn-primary">Cancel</button>
                        <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake">
                    </div><?php 
                    }
                    }
                    ?>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("form").submit(function(){

        if($.trim($('#first_name').val()) == ''){
            toastr.error('First Name cannot be blank.');
            $('#first_name').focus();
            return false;
        };
        if($.trim($('#last_name').val()) == ''){
            toastr.error('Last Name cannot be blank.');
            $('#last_name').focus();
            return false;
        };

        var query_string = $('form').serialize();
        var baseURL = '<?php echo Config::get('constants.PATH');?>';
        var form_data = new FormData(); 
        form_data.append('first_name',$('#first_name').val()); 
        form_data.append('last_name',$('#last_name').val()); 
        form_data.append('username',$('#username').val()); 
        form_data.append('password',$('#password').val()); 
        form_data.append('confirm_password',$('#confirm_password').val()); 
        form_data.append('user_role',$('#user_role').val()); 
        form_data.append('query_string',query_string); 
        $.ajax({
            dataType: 'json',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: baseURL+'ajax-register',
            data: form_data,
            beforeSend: function(){
              $('#snake').show();
              console.log('sending data...');
            },
            success: function(result) {
                // var result = $.parseJSON(result);
                $('#snake').hide();
                if(result.result.status=='success'){
                    toastr.success(result.result.message);
                    setTimeout(function() {
                        window.location.href = baseURL+"control-panel";
                    }, 5000);
                }else{
                      toastr.error(result.result.message);
                }
                return false;
            }
        });
        return false;
    });
</script>
@endsection
