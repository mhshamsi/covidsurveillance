@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Push Notifications</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page" style="color: #333333">Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label for="category_uid">Categories</label>
                        <?php 
                        echo '<select id="category_uid" class="form-control">';
                        if(!empty($notification_categories)){
                        echo '<option value="">Select Category</option>';
                        foreach ($notification_categories as $key => $notification_category) {
                            echo '<option value="'.$key.'">'.$notification_category.'</option>';                                
                        }
                        }
                        echo '</select>';
                        ?>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label for="name">Notifications</label>
                        <textarea class="form-control" id="notification"></textarea>
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <button type="submit" id="submit" class="btn btn-primary btn-lg">Add</button>
                    <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#submit").click(function(){
    if($.trim($('#category_uid').val()) == ''){
        toastr.error('Category ID cannot be blank.');
        $('#category_uid').focus();
        return false;
    };
    if($.trim($('#notification').val()) == ''){
        toastr.error('Notification cannot be blank.');
        $('#notification').focus();
        return false;
    };

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    form_data.append('category_uid',$('#category_uid').val()); 
    form_data.append('notification',$('#notification').val()); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-add-notification',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            if(result.result.status=='success'){
                $('#snake').hide();
                toastr.success(result.result.message);
                setTimeout(function() {
                    window.location.href = baseURL+"push-notification";
                }, 10);
            }else{
                $('#snake').hide();
                toastr.error(result.result.message);
            }
        }
    });
    return false;
});

</script>
@endsection