@extends('layout.home')
@section('content')
        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                                <div class="text-center mb-4"><span class="text-primary" style="font-size:2rem;">Signin</span></div>
                                    <div class="alert alert-danger" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <div id="alert-danger-message"></div>
                                    </div>
                                    <div class="form-group">
                                      <input type="text" class="form-control" name="username" id="username" placeholder="username" class="form-control" aria-required="true" autocorrect="off" autocapitalize="off" autofocus="autofocus">
                                    </div>
                                    <div class="form-group">
                                      <input type="password" name="password" id="password" placeholder="password" class="form-control" maxlength="70" aria-required="true" autocorrect="off" autocapitalize="off" autofocus="autofocus">
                                    </div>
                                    
                                    
                                    <div class="form-group form-check" style="display:none;">
                                        <a href="#" data-toggle="modal" data-target="#forgotPassword" class="float-right">Forgot Password?</a>
                                    </div>
                                    <button type="login" id="login" class="btn mt-4 btn-primary btn-block">Signin</button>
                                    <img id="snake" src="<?php echo asset('assets/images/loader.gif')?>" style="display:none;" alt="snake">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->
      <div class="modal" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content">
            <div class="modal-header">
              <div class="text-primary" id="exampleModalLabel" style="font-size:2rem;">Forgot Password</div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                  <div class="form-group">
                    <input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="username">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Reset Password</button>
            </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">

        $(document).on('keypress',function(e) { if(e.which == 13) { $("#login").click(); } });
        $("#login").click(function(){
            if($.trim($('#username').val()) == ''){
                alert('Username cannot be blank.');
                $('#alert-danger-message').html('Username cannot be blank.');
                $('.alert-danger').show(); 
                $('#username').focus();
                return false;
            };
            if($.trim($('#password').val()) == ''){
                alert('Password cannot be blank.');
                $('#alert-danger-message').html('Password cannot be blank.');
                $('.alert-danger').show(); 
                $('#password').focus();
                return false;
            }

            var baseURL = '<?php echo Config::get('constants.PATH');?>';
            var form_data = new FormData(); 
            form_data.append('username',$('#username').val());
            form_data.append('password',$('#password').val());
            $.ajax({
                dataType: 'json',
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: baseURL+'ajax-login',
                data: form_data,
                beforeSend: function(){
                  $('#snake').show();
                  console.log('sending data...');
                },
                success: function(result) {
                    if(result.result.status=='success'){
                        $('#snake').hide();
                        window.location.href = baseURL;
                        // if(result.result.role=='Admin') window.location.href = baseURL+"control-panel";
                        // else if(result.result.role=='Participant') window.location.href = baseURL+"users";
                        return false;
                    }else{
                        $('#snake').hide();
                        toastr.error(result.result.message);
                        return false;
                    }
                }
            });
        return false;
        });

      </script>
@stop