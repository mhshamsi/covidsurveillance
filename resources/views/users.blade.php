@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="page-header">
                <h2 class="pageheader-title">Users</h2>
            </div>
            <div class="card-body">
				<div class="row">
                    <div class="col-md-3">
                        <input type="text" name="participant_name" id="participant_name" class="form-control form-control-custom" placeholder="Search By Participant Name">
                        <input type="hidden" name="base_url" id="base_url" value="<?php echo Config::get('constants.PATH');?>">
                        <input type="hidden" name="page" id="page" value="users">
					</div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="role_id" id="role_id" class="form-control">
                            <?php if(!empty($user_roles)){
                                echo '<option value="">Search By Role</option>';
                                foreach ($user_roles as $key => $role) {
                                    echo '<option value="'.$key.'"'.((!empty($user->role_id) and $key==$user->role_id)?' selected="selected"':'').'>'.ucfirst(strtolower($role)).'</option>';
                                }
                            }?>
                            </select>
                        </div>
                    </div>
					<div class="col-md-2">
						<img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display:none;" alt="snake">
					</div>
                    <div class="col-md-2">
                        <button type="button" name="search" id="search" class="btn btn-primary btn-small float-right">Search</button>
                    </div>
					<div class="col-md-2">
						<button type="button" class="btn btn-info float-right" onclick="register()">Create Account</button>
					</div>
				</div>
                
                
                <div class="clearfix"></div><br>
                <div class="register">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($users)>0){
                                $serial=0;
                                foreach ($users as $key => $user) {
                                // echo '<pre>'; print_r($user); echo '</pre>'; continue;
                                ?>
                                <tr>
                                    <td style="color: #333333"><?php echo ++$serial;?></td>
                                    <td style="color: #333333"><a href="user/<?php echo $user->uid;?>"><?php echo $user->first_name.' '.$user->last_name;?></a></td>
                                    <td style="color: #333333"><?php echo $user->user_role;?></td>
                                    <td>
                                        <input type="checkbox" name="user_<?php echo (!empty($user->uid)?$user->uid:'');?>" id="<?php echo (!empty($user->uid)?$user->uid:'');?>"<?php echo (empty($user->approved_at)?'':' checked="checked"');?> onclick="change_status( this.id,this.checked,'users','')"> 
                                        <label for="" style="position: relative; top: -7px; left: 7px;"><?php echo (empty($user->approved_at)?'Approve':'Approved');?></label>
                                    </td>
                                </tr>
                                <?php }}else{?>
                                <tr>
                                    <td>
                                        Participants are not available.
                                    </td>
                                </tr>
                                <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo asset('js/purumeed.js');?>"></script>
@endsection