@extends('layout.default') 
@section('content')
<!-- ============================================================== -->
<!-- pageheader -->
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php if($_SESSION['role']=='Admin'){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Food Diary<?php echo ': '.$users[0]->first_name.' '.$users[0]->last_name.' ['.ucwords($user_days[0]->name).' - '.date('l',strtotime($user_days[0]->date)).']';?></h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="../admin/users-manage.html" class="breadcrumb-link">Users</a></li> -->
                        <!-- <li class="breadcrumb-item"><a href="../admin/user-visits.html" class="breadcrumb-link">User Visit</a></li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php }?>
<!-- ============================================================== -->
<!-- end pageheader -->
<!-- ============================================================== -->
<?php if(!empty($user_days[0]->date_start) and $user_days[0]->date_start > date("Y-m-d") or ( empty($users[0]->fooddiary_approved_at) or count($user_days)==0 )){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Yeh diary is waqt muyassir nahi hai. Aap is diary ko apne aglay tehqeeqi visit se aik haftey pehle pur kar sakenge.</h3>
            </div>
        </div>
    </div>
</div>
<?php }elseif(!empty($users[0]->fooddiary_approved_at)){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form class="form-row">
                    <input type="hidden" name="user_day_uid" id="user_day_uid" class="form-control" value="<?php echo $user_day_uid;?>">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <label for="wakeup_time" class="mr-sm-2">Wake Up Time</label>
                            <input name="wakeup_time" id="wakeup_time" type="time"<?php echo (empty($visit_uid)?'':' readonly');?> class="form-control" value="<?php echo $user_days[0]->wakeup_time;?>">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <label for="sleep_time" class="mr-sm-2">Time To Bed</label>
                            <input name="sleep_time" id="sleep_time" type="time"<?php echo (empty($visit_uid)?'':' readonly');?> class="form-control" value="<?php echo $user_days[0]->sleep_time;?>">
                        </div>
                    </div>
                    <?php 
                    if(!empty($meal_types)){
                    foreach ($meal_types as $meal_type_uid => $meal_type) {
                        // echo '<pre>';print_r($meal_type_uid);  echo '</pre>';
                        ?><div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="mr-sm-2"><?php echo $meal_type;?></label>
                                <?php if(empty($visit_uid)){?>
                                <i class="fa fa-plus" onclick="$('.modal-title').html('<?php echo ucwords($user_days[0]->name);?> - <?php echo $meal_type;?>'); $('#meal_type_uid').val('<?php echo $meal_type_uid;?>');" style="color:#fd8664;" data-toggle="modal" data-target="#mealModal"></i>
                                <?php }?>
                                <?php 
                        if(!empty($user_meals[$meal_type_uid])){
                        foreach ($user_meals[$meal_type_uid] as $key2 => $user_meal) {
                        ?><p>
                        <span class="badge badge-success"><?php echo $food_items[$user_meal->food_item_uid];?></span> &nbsp; 
                        <span class="badge badge-warning"><?php echo $food_portions[$user_meal->food_item_uid][htmlspecialchars_decode($user_meal->food_portion)];?></span>
                        </p>
                        <?php }}else{?>
                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <p>Meal does not exists</p>
                            </div>
                        </div><?php }?></div>
                        </div><?php 
                    }
                    }
                    ?>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-group">
                            <label class="mr-sm-2">Water</label>
                            <?php if(empty($visit_uid)){?>
                            <i class="fa fa-plus" onclick="$('.modal-title').html('<?php echo ucwords($user_days[0]->name);?> - Water'); $('#meal_type_uid').val('<?php echo $meal_type_uid;?>');" style="color:#fd8664;" data-toggle="modal" data-target="#waterModal"></i>
                            <?php }?>
                            <p><span class="badge badge-success">Number Of Glasses: </span> &nbsp; 
                        <span class="badge badge-warning"><?php echo (!empty($user_days[0]->water_intake)?$user_days[0]->water_intake:'0');?></span></p>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-group">
                            <label for="anyComplaints">Any Complaints</label>
                            <?php if(empty($visit_uid)){?>
                            <textarea class="form-control" name="complaints" id="complaints" cols="30" rows="2"><?php echo $user_days[0]->complaints;?></textarea>
                            <?php }else{
                                echo '
                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <p>'.$user_days[0]->complaints.'</p>
                            </div>
                        </div>';
                            }?>
                        </div>
                    </div>
                    &nbsp;
                    <?php if(empty($visit_uid)){?>
                    <button type="submit" id="submit" class="btn btn-lg btn-primary">Save</button>
                    <div><img id="diary_snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake"></div>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
</div>

<?php if(empty($visit_uid)){?>
<!-- meal modal start -->
<div class="modal" id="mealModal">
    <div class="modal-dialog">
      <div class="modal-content">      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Day # - Meal Name</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
            <input type="hidden" name="meal_type_uid" id="meal_type_uid">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group">
                    <label for="breakFastInput">Food</label>
                    <select name="food_group_uid" id="food_group_uid" onchange="select_food_items(this.value)" class="form-control">
                        <option value="">Choose...</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group">
                    <label for="breakFastInput">Food Items</label>
                    <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake">
                    <select name="food_item_uid" id="food_item_uid" class="form-control">
                        <option value="">Choose...</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group">
                    <label for="breakFastPortionInput">Food Portions</label>
                    <select name="food_portion" id="food_portion" class="form-control">
                        <option value="">Choose...</option>
                    </select>
                </div>
            </div>  
        </div>
        <!-- Modal footer -->
        <?php if(empty($visit_uid)){?>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="submit_meal">Submit</button>
        </div>
        <?php }?>
    </div>
    </div>
</div> 
<!-- meal modal end -->


<!-- water modal start -->
<div class="modal" id="waterModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Day # - Water</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group">
                    <label for="water">Number of Glasses</label>
                    <input type="number" name="water" id="water" class="form-control">
                </div>
                <?php if(empty($visit_uid)){?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submit_water">Submit</button>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
    </div>
</div> 
<!-- water modal end -->

<script type="text/javascript">
$('#submit').click(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    $('#diary_snake').show();
    console.log('sending data...');
    setTimeout(function() {
        $('#snake').hide();
        toastr.success('Food diary updated successfully');
        setTimeout(function() {
            window.location.href = baseURL+"user-food-diary";
        }, 3000);
    }, 5000);
    return false;
});

$('#wakeup_time').blur(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    var user_day_uid = $('#user_day_uid').val();
    form_data.append('user_day_uid',user_day_uid); 
    form_data.append('wakeup_time',$('#wakeup_time').val()); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-save-food-diary',
        data: form_data,
        beforeSend: function(){
          $('#diary_snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            if(result.status=='success'){
                $('#diary_snake').hide();
                // toastr.success(result.message);
            }else{
                $('#diary_snake').hide();
                toastr.error(result.message);
            }
        }
    });
    return false;
});

$('#sleep_time').blur(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    var user_day_uid = $('#user_day_uid').val();
    form_data.append('user_day_uid',user_day_uid); 
    form_data.append('sleep_time',$('#sleep_time').val()); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-save-food-diary',
        data: form_data,
        beforeSend: function(){
          $('#diary_snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            if(result.status=='success'){
                $('#diary_snake').hide();
                // toastr.success(result.message);
            }else{
                $('#diary_snake').hide();
                toastr.error(result.message);
            }
        }
    });
    return false;
});

$('#complaints').blur(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    var user_day_uid = $('#user_day_uid').val();
    form_data.append('user_day_uid',user_day_uid); 
    form_data.append('complaints',$('#complaints').val()); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-save-food-diary',
        data: form_data,
        beforeSend: function(){
          $('#diary_snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            if(result.status=='success'){
                $('#diary_snake').hide();
                // toastr.success(result.message);
            }else{
                $('#diary_snake').hide();
                toastr.error(result.message);
            }
        }
    });
    return false;
});

function select_food_groups() {

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    // var form_data = new FormData(); 
    $.ajax({
        dataType: 'text',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-add-food-groups',
        // data: form_data,
        beforeSend: function(){
          $('#snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            $('#food_group_uid').html('<option value="">Choose</option>'+result);
            $('#snake').hide();
        }
    });
    return false;
} select_food_groups();

// function select_food_items(food_group_uid) {}
$('#food_group_uid').change(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    form_data.append('food_group_uid',$('#food_group_uid').val()); 
    $.ajax({
        dataType: 'text',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-add-food-items',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          $('#food_item_uid').val('');
          $('#food_portion').val('');
          $('#food_item_uid').html('<option value="">Choose</option>');
          $('#food_portion').html('<option value="">Choose</option>');
          console.log('sending data...');
        },
        success: function(result) {
            $('#food_item_uid').html('<option value="">Choose</option>'+result);
            $('#snake').hide();
        }
    });
    return false;
});

$('#food_item_uid').change(function (){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    form_data.append('food_item_uid',$('#food_item_uid').val()); 
    $.ajax({
        dataType: 'text',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-add-food-portion',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          $('#food_portion').html('<option value="">Choose</option>');
          $('#food_portion').val('');
          console.log('sending data...');
        },
        success: function(result) {
            $('#food_portion').html('<option value="">Choose</option>'+result);
            $('#snake').hide();
        }
    });
    return false;
});

$('#submit_meal').click(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    var user_day_uid = $('#user_day_uid').val();

    if($.trim($('#meal_type_uid').val()) == ''){
        toastr.error('Meal type cannot be blank.');
        $('#meal_type_uid').focus();
        return false;
    };
    if($.trim($('#food_item_uid').val()) == ''){
        toastr.error('Food item cannot be blank.');
        $('#food_item_uid').focus();
        return false;
    };
    if($.trim($('#food_portion').val()) == ''){
        toastr.error('Food portion cannot be blank.');
        $('#food_portion').focus();
        return false;
    };

    form_data.append('user_day_uid', $('#user_day_uid').val()); 
    form_data.append('meal_type_uid',$('#meal_type_uid').val()); 
    form_data.append('food_item_uid',$('#food_item_uid').val()); 
    form_data.append('food_portion', $('#food_portion').val()); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-save-food-items',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          $('#food_group_uid').val('');
          $('#meal_type_uid').val('');
          $('#food_item_uid').val('');
          $('#food_portion').val('');
          console.log('sending data...');
        },
        success: function(result) {
            if(result.status=='success'){
                $('#snake').hide();
                toastr.success(result.message);
                // setTimeout(function() {
                    window.location.href = baseURL+"user-food-diary-detail/"+user_day_uid;
                // }, 3000);
            }else{
                $('#snake').hide();
                toastr.error(result.message);
            }
        }
    });
    $('#mealModal').modal('hide');
});

$('#submit_water').click(function(){

    var baseURL = '<?php echo Config::get('constants.PATH');?>';
    var form_data = new FormData(); 
    var user_day_uid = $('#user_day_uid').val();
    if($.trim($('#water').val()) == ''){
        toastr.error('Number of glasses cannot be blank.');
        $('#water').focus();
        return false;
    };
    form_data.append('water',$('#water').val()); 
    form_data.append('user_day_uid',$('#user_day_uid').val()); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-save-water',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          $('#water').val('');
          console.log('sending data...');
        },
        success: function(result) {
            // if(result.status=='success'){
                $('#snake').hide();
                toastr.success('Water updated successfully');
                // setTimeout(function() {
                    window.location.href = baseURL+"user-food-diary-detail/"+user_day_uid;
                // }, 3000);
            // }else{
            //     $('#snake').hide();
            //     toastr.error(result.message);
            // }
        }
    });
    $('#waterModal').modal('hide');
});
</script>

<?php }?>
<?php } ?>

@endsection