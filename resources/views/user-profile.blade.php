@extends('layout.default') 
@section('content') 
<!-- ============================================================== --> 
<!-- pageheader --> 
<!-- ============================================================== -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php if($_SESSION['role']=='Admin'){?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Profile: <?php echo $users[0]->first_name.' '.$users[0]->last_name;?></h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="./users-manage.html" class="breadcrumb-link">Users</a></li> -->
                        <!-- <li class="breadcrumb-item"><a href="./user-visits.html" class="breadcrumb-link">User Visits</a></li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page" style="color:#333333;">Questionnaire</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php }?>
<!-- ============================================================== --> 
<!-- end pageheader --> 
<!-- ============================================================== -->
<?php {?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <?php if(empty($visit_uuid)){?>
                <form method="POST" name="follow-up" id="follow-up" data-persist="garlic" enctype="multipart/form-data">
                    <?php }?>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <?php
                    if(!empty($categories)){
                    foreach ($categories as $category) {
                        if(!empty($category->question_subcategories)){
                        ?><h3><?php echo $category->category_roman;?></h3><?php 
                        foreach ($category->question_subcategories as $question_subcategory) {
                            if(!empty($question_subcategory->questions)){
                            $serial=0;
                            ?><h4 class=""><?php echo $question_subcategory->subcategory_roman;?></h4><?php 
                            foreach ($question_subcategory->questions as $question) {
                                echo '<div class="form-group">';
                                ?><label><?php echo ($question->code).') '.$question->urdu;?></label><?php
                                // echo '<br>qtype: '.$question->qtype;
                                if(!empty($visit_uuid)){

                                    if(!empty($query_answers)){
                                        // echo '<br>qtype: '.$question->qtype;
                                    switch ($question->qtype) {
                                        case 'datetime':
                                            if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.date('F d, Y',strtotime($query_answers[$question->id])).'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                            break;
                                        case 'radio':
                                            if(!empty($query_answers[$question->id]))
                                                // echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                                echo '<br><label>Ans) '.$question_answers[$query_answers[$question->id]].'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                        break;
                                        case 'number':
                                            if(!empty($query_answers[$question->id]))
                                                echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                            break;
                                        case 'combo':
                                                // if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                                if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.$question_answers[$query_answers[$question->id]].'</lable>';
                                                else echo '<br><label>Ans) -- Not Answered --';
                                        break;
                                        default:
                                            if(!empty($query_answers[$question->id])) echo '<br><label>Ans) '.$query_answers[$question->id].'</lable>';
                                            else echo '<br><label>Ans) -- Not Answered --';
                                        break;
                                    }
                                    }else{
                                        echo '<br><label>Ans) -- Not Answered --</lable>';
                                    }
                                }
                                echo '</div>';

                            }
                            }
                        }
                        }
                        if(empty($visit_uuid)){?>
                        <div class="form-group">
                            <button type="submit" id="submit" class="btn btn-primary btn-lg">Submit</button>
                            <button type="button" name="save" id="save" class="btn btn-lg btn-primary">Save for Later</button>
                            <img id="snake" src="<?php echo asset('assets/images/loader.gif');?>" style="display: none;" alt="snake">
                        </div>
                        <?php 
                        }
                    }
                    }
                    ?>
                </div>
                <?php if(empty($visit_uuid)){?></form><?php }?>
            </div>
        </div>
    </div>
</div>
<?php }?>
@endsection
