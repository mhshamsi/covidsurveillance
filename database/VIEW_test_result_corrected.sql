CREATE OR REPLACE VIEW test_result_corrected AS
SELECT t0.* FROM test_result as t0
INNER JOIN (
  SELECT MAX(id) as id, `member_uid`, `record_date`, `added_on`, `type`, `added_by`
	FROM `test_result`
	GROUP BY `member_uid`, `record_date`, `type`, `added_on`, `added_by`
) as t1 ON t0.id = t1.id