CREATE OR REPLACE VIEW outcome_status_report AS
SELECT 
  member_uid as member_uid, 
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lat")) as lat,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lng")) as lng,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.type")) as type,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.form_id")) as form_id,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.added_on")) as added_on,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_outcome")) as sp_outcome,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.record_date")) as record_date,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_outcome_pos")) as sp_outcome_pos,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_test_result")) as sp_test_result,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.followup_status")) as followup_status,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.current_datetime")) as current_datetime,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.registration_uid")) as registration_uid,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_end_of_followup")) as sp_end_of_followup,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_test_was_conducted")) as sp_test_was_conducted,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_condition_of_patient")) as sp_condition_of_patient,
  COALESCE(t1.username, t2.username, "-") as added_by,
  t0.entered_on as entered_on
FROM `outcome_status_corrected` as t0
LEFT JOIN `user-list` as t1 ON t1.uid = t0.added_by
LEFT JOIN `users` as t2 ON t2.uid = t0.added_by