CREATE OR REPLACE VIEW test_result_report AS
SELECT 
  member_uid as member_uid, 
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lat")) as lat,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lng")) as `lng`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.type")) as `type`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.form_id")) as `form_id`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.added_on")) as `added_on`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.record_date")) as `record_date`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.current_datetime")) as `current_datetime`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.registration_uid")) as `registration_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_outcome")) as `sp_outcome`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_RTPCR_test_result")) as `sp_RTPCR_test_result`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_was_sample_tested")) as `sp_was_sample_tested`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.date_of_sample_tested")) as `date_of_sample_tested`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_RTPCR_test_result_pos")) as `sp_RTPCR_test_result_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_geneXpert_test_result")) as `sp_geneXpert_test_result`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_was_sample_tested_pos")) as `sp_was_sample_tested_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_geneXpert_test_result_pos")) as `sp_geneXpert_test_result_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sampleId")) as `sampleId`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.result_communicated")) as `result_communicated`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_test_result")) as `sp_test_result`,
  COALESCE(t1.username, t2.username, "-") as added_by,
  t0.entered_on as entered_on
FROM `test_result_corrected` as t0
LEFT JOIN `user-list` as t1 ON t1.uid = t0.added_by
LEFT JOIN `users` as t2 ON t2.uid = t0.added_by