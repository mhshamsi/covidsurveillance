CREATE OR REPLACE VIEW followup_corrected AS
SELECT t0.* FROM followup as t0
INNER JOIN (
  SELECT MAX(id) as id, `member_uid`, `record_date`, `added_on`, `type`, `added_by`
	FROM `followup`
	GROUP BY `member_uid`, `record_date`, `type`, `added_on`, `added_by`
) as t1 ON t0.id = t1.id
