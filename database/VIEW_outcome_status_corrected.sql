CREATE OR REPLACE VIEW outcome_status_corrected AS
SELECT t0.* FROM `followup` as t0
INNER JOIN (
  SELECT `member_uid`, `record_date`, `added_on`, `type`, `added_by`, max(id) as id
  FROM `followup`
  WHERE `type` = "LHV_PatientStatus"
  GROUP BY `member_uid`, `record_date`, `type`, `added_on`, `added_by`
) as t1 ON t0.id = t1.id