CREATE OR REPLACE VIEW registeration_corrected AS
SELECT t0.* FROM
registration as t0
INNER JOIN (
    SELECT
      max(id) as id, `member_uid`, `record_date`, `type`, `added_on`, `added_by`
    FROM `registration`
    GROUP BY `member_uid`, `record_date`, `type`, `added_on`, `added_by`
) as t1 ON t1.id = t0.id