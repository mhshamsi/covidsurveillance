CREATE OR REPLACE VIEW assessment_corrected AS
SELECT t0.* FROM assessment as t0
INNER JOIN (
	SELECT MAX(id) as id, `member_uid`, `record_date`, `type`, `added_on`, `added_by`
	FROM assessment
	GROUP BY `member_uid`, `record_date`, `type`, `added_on`, `added_by`
) as t1 ON t0.id = t1.id