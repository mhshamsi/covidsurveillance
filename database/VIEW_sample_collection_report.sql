CREATE OR REPLACE VIEW sample_collection_report AS
SELECT 
  member_uid as member_uid, 
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lat")) as lat,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lng")) as `lng`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.type")) as `type`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.form_id")) as `form_id`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.added_on")) as `added_on`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_nasal")) as `cb_nasal`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_other")) as `cb_other`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_saliva")) as `cb_saliva`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_sputum")) as `cb_sputum`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.LIS_number")) as `LIS_number`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_uc_name")) as `sp_uc_name`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_test_type")) as `sp_test_type`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_uc_name_pos")) as `sp_uc_name_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_uc_name_uid")) as `sp_uc_name_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_collected_by")) as `sp_collected_by`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_village_name")) as `sp_village_name`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_mid_turbinate")) as `cb_mid_turbinate`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.current_datetime")) as `current_datetime`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_district_name")) as `sp_district_name`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_test_type_pos")) as `sp_test_type_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_oropharangeyal")) as `cb_oropharangeyal`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_nasopharangeyal")) as `cb_nasopharangeyal`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_reason_for_test")) as `sp_reason_for_test`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_collected_by_pos")) as `sp_collected_by_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_village_name_pos")) as `sp_village_name_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_village_name_uid")) as `sp_village_name_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_BHCs_facility_uid")) as `sp_BHCs_facility_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_district_name_pos")) as `sp_district_name_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_district_name_uid")) as `sp_district_name_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_health_facilities")) as `sp_health_facilities`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_patient_consented")) as `sp_patient_consented`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_broncheolar_lavage")) as `cb_broncheolar_lavage`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cb_nasal_wash_aspirine")) as `cb_nasal_wash_aspirine`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_reason_for_test_pos")) as `sp_reason_for_test_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.specify_other_checkbox")) as `specify_other_checkbox`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.gps_coordinate_latitude")) as `gps_coordinate_latitude`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_collected_by_user_uid")) as `sp_collected_by_user_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_health_facilities_pos")) as `sp_health_facilities_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_health_facilities_uid")) as `sp_health_facilities_uid`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_patient_consented_pos")) as `sp_patient_consented_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.date_of_sample_collection")) as `date_of_sample_collection`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.gps_coordinate__longitude")) as `gps_coordinate__longitude`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.specify_other_facility_name")) as `specify_other_facility_name`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.specify_other_reason_for_test")) as `specify_other_reason_for_test`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_speciman_collection_BHCs_facility")) as `sp_speciman_collection_BHCs_facility`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_sample_collection_available_facility")) as `sp_sample_collection_available_facility`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_facility_name_sample_sent_for_testing")) as `sp_facility_name_sample_sent_for_testing`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_speciman_collection_BHCs_facility_pos")) as `sp_speciman_collection_BHCs_facility_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_sample_collected_facility_or_community")) as `sp_sample_collected_facility_or_community`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_sample_collection_available_facility_pos")) as `sp_sample_collection_available_facility_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_facility_name_sample_sent_for_testing_pos")) as `sp_facility_name_sample_sent_for_testing_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_sample_collected_facility_or_community_pos")) as `sp_sample_collected_facility_or_community_pos`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.specify_other_speciman_collection_BHCs_facility")) as `specify_other_speciman_collection_BHCs_facility`,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sampleId")) as `sampleId`,
  COALESCE(t1.username, t2.username, "-") as added_by,
  t0.entered_on as entered_on
FROM `sample_collection_corrected` as t0
LEFT JOIN `user-list` as t1 ON t1.uid = t0.added_by
LEFT JOIN `users` as t2 ON t2.uid = t0.added_by