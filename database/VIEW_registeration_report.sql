CREATE OR REPLACE VIEW registeration_report AS
SELECT 
  member_uid as member_uid, 
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.registration_uid")) as registration_uid, 
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lat")) as lat,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.lng")) as lng,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.full_name")) as full_name,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.dob")) as dob,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.age")) as age,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_gender")) as sp_gender,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.akhsp_medical_record_number")) as akhsp_medical_record_number,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.contact_number_1")) as contact_number_1,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.contact_number_2")) as contact_number_2,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.cnic_number")) as cnic_number,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_known_contact_with_symtoms")) as sp_known_contact_with_symtoms,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.address")) as address,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.jamati")) as jamati,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_region_name")) as sp_region_name,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_district_name")) as sp_district_name,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_tehsil_name")) as sp_tehsil_name,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_uc_name")) as sp_uc_name,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.sp_village_name")) as sp_village_name,
  COALESCE(t1.username, t2.username, "-") as added_by,
  JSON_UNQUOTE(JSON_EXTRACT(`metadata`, "$.current_datetime")) as current_datetime,
  t0.entered_on as entered_on
FROM `registeration_corrected` as t0
LEFT JOIN `user-list` as t1 ON t1.uid = t0.added_by
LEFT JOIN `users` as t2 ON t2.uid = t0.added_by;