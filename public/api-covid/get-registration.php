<html>
<head>
<title>Get Patient Registration</title>
</head>
<?php //include("header.php");?>
<h1>Get Patient Registration</h1>

<form method="POST">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="<?php echo ($_SERVER['SERVER_NAME']=='covid.surveillance')?'http://':'https://';?><?php echo $_SERVER['SERVER_NAME'];?>/corona-get-registration" size="50"></td>
        </tr>
        <tr>
            <td><label for="member_uid">Member Uid</label></td>
            <td><input name="member_uid" id="member_uid" value="4b6b0220d52760b875cdae8121a49f4f"></td>
        </tr>
        <tr>
            <td><label for="type">Type</label></td>
            <td>
                <select name="type">
                    <option value="LHV" selected="selected">LHV</option>
                    <option value="community">Community</option>
                    <option value="helpline">Helpline</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label for="record_date">Record date</label></td>
            <td><input name="record_date" id="record_date" value="2020-07-07 16:16:18"></td>
        </tr>
        <tr>
            <td><label for="metadata">Metadata</label></td>
            <td><textarea name="metadata" id="metadata" cols="50" rows="20">We believe that a loyal consumer is a brand’s greatest asset. Our vision recognizes that loyalty must be earned, first through a great product or service, and then through emotional bonding and personalized experiences. We have aligned our solutions to deliver digital moments of value at every stage of the loyalty spectrum, creating enriched experiences for the consumer while driving economic value for the brand. Our solutions typically result in lower cost of acquisition, foster more active members, increased advocacy and sustained sales velocity.</textarea></td>
        </tr>
        <tr>
            <td><label for="added_by">Added By</label></td>
            <td><input name="added_by" id="added_by" value="85a990d5a5e31a9f6f8f79e4714eaa9a"></td>
        </tr>
        <tr>
            <td><label for="added_on">Added On</label></td>
            <td><input name="added_on" id="added_on" value="<?php echo strtotime('now');?>"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align:left;">
                <input type="submit" name="submit" value="Submit">
            </td>
        </tr>
    </table>
</form>
<?php 
    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        echo $url = $_POST['url'];
        $fields = array(
            'member_uid'    => $_POST['member_uid'],
            'type'          => $_POST['type'],
            'record_date'   => $_POST['record_date'],
            'metadata'      => $_POST['metadata'],
            'added_by'      => $_POST['added_by'],
            'added_on'      => $_POST['added_on'],
        );
        echo '<pre>POST '; print_r($fields); echo '</pre>';
        //curl open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Corona Surveilance');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);
        exit();
    }
?>
</html>