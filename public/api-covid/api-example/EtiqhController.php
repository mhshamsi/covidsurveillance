<?php

class EtiqhController extends BaseController {

	public function postToolDetails(){

		$api_transaction_id   = Api::get_api_transaction_id();		
		if($authentication_token == Input::get('auth_token'))
		{
			$parameters =  array(
				'api_transaction_id'	=> $api_transaction_id,
				'device_id'             => $_POST['device_id'],
			 );
			$result = etiqh::tool_details($parameters);

			Api::update_activity_log($api_transaction_id, Input::all(), $result);
			return Response::json($result);
		}
		else
		{
			$status['api_transaction_id'] = $api_transaction_id;
      $status['status']   = 'error';
			$status['message']   = 'Authentication Token is not valid.';
			$result['status']  = $status;

			// Api::update_activity_log($api_transaction_id, Input::all(), $result);
			return Response::json($result);
		}
	}
}