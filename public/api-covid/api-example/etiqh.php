<?php

class Etiqh {

    public static function tool_details($input){

        if(!empty($input['api_transaction_id']))
        $status['api_transaction_id'] = $input['api_transaction_id'];

        // Validate the Input Fields
        $rules = array(
            'device_id'     => 'required',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
        {
            $status['status'] = 'error';
            $status['message'] = $validator->messages()->all();
            $result['result'] = $status;
            return $result;
        }

        $tools = DB::table('tools')
                    ->orderBy('id','asc')
                    ->select(array(
                        DB::raw('COALESCE(id,\'\') as id'),
                        DB::raw('COALESCE(name,\'\') as name'),
                    ))->get();
        if(!empty($tools)){
            foreach ($tools as $tool) {
                // echo '<pre>tools: '.$tool->id; print_r($tool); echo '</pre>';
                $indicators = DB::table('indicators')
                    ->where('indicators.tool_id', $tool->id)
                    ->orderBy('id','asc')
                    ->select(array(
                        DB::raw('COALESCE(id,\'\') as id'),
                        DB::raw('COALESCE(tool_id,\'\') as tool_id'),
                        DB::raw('COALESCE(name,\'\') as name'),
                    ))->get();

                if(!empty($indicators))
                {
                    foreach ($indicators as $indicator)
                    {
                        // echo '<pre>indicators: '.$indicator->id; print_r($indicator); echo '</pre>';
                        $questions = DB::table('questions')
                            ->where('questions.indicator_id', $indicator->id)
                            ->orderBy('id','asc')
                            ->select(array(
                                DB::raw('COALESCE(questions.id,\'\') as id'),
                                DB::raw('COALESCE(indicator_id,\'\') as indicator_id'),
                                DB::raw('COALESCE(sub_indicator,\'\') as sub_indicator'),
                                DB::raw('COALESCE(weightage,\'\') as weightage'),
                                DB::raw('COALESCE(question,\'\') as question'),
                            ))->get();

                        if(!empty($questions))
                        {   
                            $indicator->questions = $questions;
                        }
                    }
                    $tool->indicators = $indicators;
                }
            }
        }
        // echo '<pre>';print_r($tools); echo '</pre>'; exit();                      
        $status['status']   = 'success';
        $result['result']   = $status;
        $result['response'] = $tools;
        return $result;
    }

}