<html>
<head>
<title>Test Result</title>
</head>
<?php //include("header.php");?>
<h1>Test Result</h1>

<form method="POST">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="<?php echo ($_SERVER['SERVER_NAME']=='covid.surveillance')?'http://':'https://';?><?php echo $_SERVER['SERVER_NAME'];?>/corona-test-result" size="50"></td>
        </tr>
        <tr>
            <td><label for="member_uid">Member Uid</label></td>
            <td><input name="member_uid" id="member_uid" value="4b6b0220d52760b875cdae8121a49f4f"></td>
        </tr>
        <tr>
            <td><label for="type">Type</label></td>
            <td>
                <select name="type">
                    <option value="LHV_TestResultForm" selected="selected">LHV_TestResultForm</option>
                    <option value="COMM_TestResultForm">COMM_TestResultForm</option>
                    <option value="HLP_TestResultForm">HLP_TestResultForm</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label for="record_date">Record date</label></td>
            <td><input name="record_date" id="record_date" value="2020-07-07"></td>
        </tr>
        <tr>
            <td><label for="metadata">Metadata</label></td>
            <td><textarea name="metadata" id="metadata" cols="50" rows="15">{"result":{"status":"success"},"response":{"cc6f0a297a9749cdb7925ebf9bcf01d3":[{"id":26,"uid":"fbe6b08aa46589941dbf1b3a8d6cb6f2","member_uid":"cc6f0a297a9749cdb7925ebf9bcf01d3","type":"LHV_AssessmentForm","record_date":"2020-09-18","metadata":"{\"lat\": \"24.899239777587354\", \"lng\": \"67.08206062205136\", \"plan\": \"C\", \"type\": \"LHV_AssessmentForm\", \"Result\": \"LHV Logics Result\", \"form_id\": \"6b00a3cf\", \"cb_cough\": \"1\", \"cb_fever\": \"0\", \"et_other\": \"none\", \"cb_cancer\": \"1\", \"cb_chills\": \"-1\", \"cb_nausea\": \"-1\", \"plan_text\": \"C: Send patient for home isolation with counseling and community volunteer contact. If risk factors are there, counsel patient.\", \"Section 01\": \"Section 01\", \"Section 02\": \"Section 02\", \"Section 03\": \"Section 03\", \"Section 04\": \"Section 04\", \"et_remarks\": \"Ok\", \"member_uid\": \"cc6f0a297a9749cdb7925ebf9bcf01d3\", \"cb_diabetic\": \"1\", \"cb_diarrhea\": \"-1\", \"cb_headache\": \"1\", \"risk_factor\": \"With Risk Factor\", \"cb_body_ache\": \"1\", \"cb_pregnancy\": \"-1\", \"cb_muscle_pain\": \"1\", \"cb_sore_throat\": \"-1\", \"et_temperature\": \"none\", \"cb_stomach_ache\": \"-1\", \"cb_new_confusion\": \"1\", \"current_datetime\": \"2020-09-18 04:19:10 pm\", \"disease_severity\": \"Mild\", \"cb_kidney_disease\": \"1\", \"cb_severe_fatigue\": \"0\", \"et_date_of_contact\": \"none\", \"sp_profession_name\": \"null\", \"cb_previous_history\": \"1\", \"cb_respiratory_rate\": \"-1\", \"et_date_of_symptoms\": \"none\", \"cb_immunocompromised\": \"1\", \"et_medical_prescribe\": \"Ok\", \"cb_history_of_contact\": \"0\", \"sp_refer_facility_pos\": \"-1\", \"sp_refer_facility_uid\": \"none\", \"sp_profession_name_pos\": \"-1\", \"sp_refer_facility_name\": \"null\", \"cb_local_area_residence\": \"1\", \"sp_assign_community_pos\": \"0\", \"sp_assign_community_uid\": \"d68618d995bff9436fe09428107118f5\", \"et_other_profession_name\": \"none\", \"sp_assign_community_name\": \"com1\", \"cb_cardiovascular_disease\": \"1\", \"cb_loss_of_taste_or_smell\": \"1\", \"et_oxygen_saturation_value\": \"50\", \"cb_public_facing_profession\": \"0\", \"cb_attended_public_gathering\": \"1\", \"cb_travel_outside_the_country\": \"1\", \"presumptive_or_nonpresumptive\": \"Presumptive symptomatic\", \"cb_rapid_and_shallow_breathing\": \"-1\", \"cb_severe_respiratory_distress\": \"1\", \"et_blood_pressure_value_systolic\": \"none\", \"et_how_patient_immunocompromised\": \"none\", \"et_blood_pressure_value_diastolic\": \"none\", \"cb_shortness_or_difficulty_breathing\": \"0\", \"cb_bluish_discoloration_of_tongue_lips\": \"1\"}","added_on":"1600445950000","added_by":"4b6b0220d52760b875cdae8121a49f4f","entered_on":"2020-09-18 16:19:10","is_synced":null}]}}</textarea></td>
        </tr>
        <tr>
            <td><label for="added_by">Added By</label></td>
            <td><input name="added_by" id="added_by" value="85a990d5a5e31a9f6f8f79e4714eaa9a"></td>
        </tr>
        <tr>
            <td><label for="added_on">Added On</label></td>
            <td><input name="added_on" id="added_on" value="<?php echo strtotime('now');?>"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align:left;">
                <input type="submit" name="submit" value="Submit">
            </td>
        </tr>
    </table>
</form>
<?php 
    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        echo $url = $_POST['url'];
        $fields = array(
            'member_uid'    => $_POST['member_uid'],
            'type'          => $_POST['type'],
            'record_date'   => $_POST['record_date'],
            'metadata'      => $_POST['metadata'],
            'added_by'      => $_POST['added_by'],
            'added_on'      => $_POST['added_on'],
        );
        echo '<pre>POST '; print_r($fields); echo '</pre>';
        //curl open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Corona Surveilance');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);
        exit();
    }
?>
</html>