<html>
<head>
<title></title>
<script src="http://etiqh.local/assets/js/lib/jquery/jquery.min.js"></script>
</head>
<?php //include("header.php");?>
<h1>Listings</h1>
<form method="POST" action="">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="http://<?php echo $_SERVER['SERVER_NAME'];?>/<?php echo ($_SERVER['SERVER_NAME']!='etiqh.local')?'public/':'';?>api/v1/etiqh/listings" size="50"></td>
        </tr>
        <tr>
            <td><label for="listings">Show Listing</label></td>
            <td>
                <select name="listings" id="listings">
                    <option value="interviewee_titles"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='interviewee_titles'))?' selected="selected"':'';?>>interviewee_titles</option>
                    <option value="users"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='users'))?' selected="selected"':'';?>>users</option>
                    <option value="countries"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='countries'))?' selected="selected"':'';?>>countries</option>
                    <option value="regions"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='regions'))?' selected="selected"':'';?>>regions</option>
                    <option value="districts"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='districts'))?' selected="selected"':'';?>>districts</option>
                    <option value="cities"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='cities'))?' selected="selected"':'';?>>cities</option>
                    <option value="zones"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='zones'))?' selected="selected"':'';?>>zones</option>
                    <option value="health_facilities"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='health_facilities'))?' selected="selected"':'';?>>health_facilities</option>
                    <option value="tools"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='tools'))?' selected="selected"':'';?>>tools</option>
                    <option value="indicators"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='indicators'))?' selected="selected"':'';?>>indicators</option>
                    <option value="questions"<?php echo (!empty($_POST['listings']) and ($_POST['listings']=='questions'))?' selected="selected"':'';?>>questions</option>
                </select>
            </td>
        </tr>
        <?php 
        if(!empty($_POST['listings']) and $_POST['listings'] == 'questions'){
            ?>
            <tr>
            <td><label for="tool_id">Tool</label></td>
            <td>
                <select name="tool_id" id="tool_id">
                    <option value="0">Select tool</option>
                    <option value="1"<?php echo (!empty($_POST['tool_id']) and ($_POST['tool_id']=='1'))?' selected="selected"':'';?>>Physical environment and equipment</option>
                    <option value="2"<?php echo (!empty($_POST['tool_id']) and ($_POST['tool_id']=='2'))?' selected="selected"':'';?>>Job expectations</option>
                    <option value="3"<?php echo (!empty($_POST['tool_id']) and ($_POST['tool_id']=='3'))?' selected="selected"':'';?>>Professional knowledge and skills</option>
                    <option value="4"<?php echo (!empty($_POST['tool_id']) and ($_POST['tool_id']=='4'))?' selected="selected"':'';?>>Management and administration of the facility</option>
                    <option value="5"<?php echo (!empty($_POST['tool_id']) and ($_POST['tool_id']=='5'))?' selected="selected"':'';?>>Staff motication</option>
                    <option value="6"<?php echo (!empty($_POST['tool_id']) and ($_POST['tool_id']=='6'))?' selected="selected"':'';?>>Client satisfaction</option>
                </select>
            </td>
            </tr>
            <?php 
        }
        ?>
        <tr>
            <td colspan="2" style="text-align:right;"><input type="submit" name="submit" value="Submit"></td>
        </tr>
    </table>
</form>
<script>
$('#listings').change(function(){
    if($('#listings').val()=='questions'){

    // alert('--------'); return false;
        var form_data = new FormData();
        // if($('#present_complaint_'+appointment_id).val() !== undefined)
        // form_data.append('present_complaint',$('#present_complaint_'+appointment_id).val());
        
        $.ajax({
          dataType: 'text',
          type: "POST",
          url: 'ajax-search-doctor',
          // data: mydata,
          beforeSend: function(){ 
            console.log('sending doctor search request...');
            $('#snake').show();
          },
          success: function(msg) {
            console.log('receiving doctor search response...');
            $('#search_doctor').html(msg);
            $('#snake').hide();
            }
        });
    }
});
</script>
<?php 


    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        $url = $_POST['url'];
        $fields = array(
            'listings'      => $_POST['listings'],
        );
        if($_POST['listings'] == 'questions'){
            if(!empty($_POST['tool_id'])) $fields['tool_id'] = $_POST['tool_id'];
            else $fields['tool_id']=0;
        } 
        echo '<pre>POST '; print_r($fields); echo '</pre>';
        
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'slate Client');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);

        //execute post
        //echo '<pre>';
        $result = curl_exec($ch);
        //echo '</pre>';

        //close connection
        curl_close($ch);
    }
?>
</html>