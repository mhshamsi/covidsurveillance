<html>
<head>
<title>Family</title>
</head>
<?php include("header.php");?>
<h1>Family</h1>
<form method="POST" enctype = "multipart/form-data">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="<?php echo $hostname;?>sync/save/family" size="50"></td>
        </tr>
        <tr>
            <td><label for="uid">uid</label></td>
            <td><input name="uid" id="uid" value="e7b65e8ff1e742f48294fdaa8e749777"></td>
        </tr>
        <tr>
            <td><label for="manual_id">manual_id</label></td>
            <td><input name="manual_id" id="manual_id" value="210"></td>
        </tr>
        <tr>
            <td><label for="country_id">country_id</label></td>
            <td><input name="country_id" id="country_id" value="1"></td>
        </tr>
        <tr>
            <td><label for="province_id">province_id</label></td>
            <td><input name="province_id" id="province_id" value="3ab0b1caffe3e319"></td>
        </tr>
        <tr>
            <td><label for="district_id">district_id</label></td>
            <td><input name="district_id" id="district_id" value="8e9316fe4e874765"></td>
        </tr>
        <tr>
            <td><label for="tehsil_id">tehsil_id</label></td>
            <td><input name="tehsil_id" id="tehsil_id" value="0b7550c5dd487dee"></td>
        </tr>
        <tr>
            <td><label for="uc_id">uc_id</label></td>
            <td><input name="uc_id" id="uc_id" value="7b8e353017588baa"></td>
        </tr>
        <tr>
            <td><label for="village_id">village_id</label></td>
            <td><input name="village_id" id="village_id" value="525e0ec1ef443499"></td>
        </tr>
        <tr>
            <td><label for="family_head_name">family_head_name</label></td>
            <td><input name="family_head_name" id="family_head_name" value="Akbar"></td>
        </tr>
        <tr>
            <td><label for="family_address">family_address</label></td>
            <td><input name="family_address" id="family_address" value="Villge Munoor"></td>
        </tr>
        <tr>
            <td><label for="water_source">water_source</label></td>
            <td><input name="water_source" id="water_source" value="Tap"></td>
        </tr>
        <tr>
            <td><label for="toilet_facility">toilet_facility</label></td>
            <td><input name="toilet_facility" id="toilet_facility" value="Flash"></td>
        </tr>
        <tr>
            <td><label for="added_by">added_by</label></td>
            <td><input name="added_by" id="added_by" value="4d15a7ae41da19b874e00ec764ce4104000690d32451bbd99778236da769cedf"></td>
        </tr>
        <tr>
            <td><label for="added_on">added_on</label></td>
            <td><input name="added_on" id="added_on" value="1556007588101"></td>
        </tr>
        <tr>
            <td><label for="added_in_db">added_in_db</label></td>
            <td><input name="added_in_db" id="added_in_db" value="2019-04-23 11:16:44"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align:left;">
                <input type="submit" name="submit" value="Submit">
            </td>
        </tr>
    </table>
</form>
<?php 
    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        echo $url = $_POST['url'];
        $fields = array(
            'uid' =>    $_POST['uid'],
            'manual_id' =>  $_POST['manual_id'],
            'country_id' => $_POST['country_id'],
            'province_id' =>    $_POST['province_id'],
            'district_id' =>    $_POST['district_id'],
            'tehsil_id' =>  $_POST['tehsil_id'],
            'uc_id' =>  $_POST['uc_id'],
            'village_id' => $_POST['village_id'],
            'family_head_name' =>   $_POST['family_head_name'],
            'family_address' => $_POST['family_address'],
            'water_source' =>   $_POST['water_source'],
            'toilet_facility' =>    $_POST['toilet_facility'],
            'added_by' =>   $_POST['added_by'],
            'added_on' =>   $_POST['added_on'],
        );
        echo '<pre>POST '; print_r($fields); echo '</pre>';
        //curl open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Corona Surveilance');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);
        exit();
    }
?>
</html>