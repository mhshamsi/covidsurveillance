<html>
<head>
<title>Member</title>
</head>
<?php include("header.php");?>
<h1>Member</h1>
<form method="POST" enctype = "multipart/form-data">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="<?php echo $hostname;?>sync/save/member" size="50"></td>
        </tr>
        <tr>
            <td><label for="uid">uid</label></td>
            <td><input name="uid" id="uid" value="e7b65e8ff1e742f48294fdaa8e749777"></td>
        </tr>
        <tr>
            <td><label for="manual_id">manual_id</label></td>
            <td><input name="manual_id" id="manual_id" value="210"></td>
        </tr>
        <tr>
            <td><label for="family_id">family_id</label></td>
            <td><input name="family_id" id="family_id" value="1809f056a87a43d1ba60dd408123c68e"></td>
        </tr>
        <tr>
            <td><label for="qr_code">qr_code</label></td>
            <td><input name="qr_code" id="qr_code" value="qr_code"></td>
        </tr>
        <tr>
            <td><label for="bio_code">bio_code</label></td>
            <td><input name="bio_code" id="bio_code" value="bio_code"></td>
        </tr>
        <tr>
            <td><label for="full_name">full_name</label></td>
            <td><input name="full_name" id="full_name" value="Nipe"></td>
        </tr>
        <tr>
            <td><label for="nic_number">nic_number</label></td>
            <td><input name="nic_number" id="nic_number" value="9666888"></td>
        </tr>
        <tr>
            <td><label for="phone_number">phone_number</label></td>
            <td><input name="phone_number" id="phone_number" value="03332222333"></td>
        </tr>
        <tr>
            <td><label for="data">data</label></td>
            <td>
                <textarea name="data" id="data">{\"dob\": \"2019-04-23\", \"lat\": \"24.8992295\", \"lng\": \"67.0821483\", \"type\": \"LHW_Vaccinate_UnListed\", \"sp_gaon\": \"Beshqir\", \"sp_zila\": \"Chitral\", \"sp_tahseel\": \"Chitral\", \"father_name\": \"None\", \"sp_union_council\": \"Charun\"}</textarea>
            </td>
        </tr>
        <tr>
            <td><label for="gender">gender</label></td>
            <td><input name="gender" id="gender" value="1"></td>
        </tr>
        <tr>
            <td><label for="dob">dob</label></td>
            <td><input name="dob" id="dob" value="2019-04-23"></td>
        </tr>

        <tr>
            <td><label for="added_by">added_by</label></td>
            <td><input name="added_by" id="added_by" value="4d15a7ae41da19b874e00ec764ce4104000690d32451bbd99778236da769cedf"></td>
        </tr>
        <tr>
            <td><label for="added_on">added_on</label></td>
            <td><input name="added_on" id="added_on" value="1556007588101"></td>
        </tr>
        <tr>
            <td><label for="added_in_db">added_in_db</label></td>
            <td><input name="added_in_db" id="added_in_db" value="2019-04-23 11:16:44"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align:left;">
                <input type="submit" name="submit" value="Submit">
            </td>
        </tr>
    </table>
</form>
<?php 
    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        echo $url = $_POST['url'];
        $fields = array(
            'uid' =>    $_POST['uid'],
            'manual_id' =>  $_POST['manual_id'],
            'family_id'    =>  $_POST['family_id'],
            'qr_code'  =>  $_POST['qr_code'],
            'bio_code' =>  $_POST['bio_code'],
            'full_name'    =>  $_POST['full_name'],
            'nic_number'   =>  $_POST['nic_number'],
            'phone_number' =>  $_POST['phone_number'],
            'data' =>  $_POST['data'],
            'gender'   =>  $_POST['gender'],
            'dob'  =>  $_POST['dob'],
            'added_by' =>   $_POST['added_by'],
            'added_on' =>   $_POST['added_on'],
        );
        echo '<pre>POST '; print_r($fields); echo '</pre>';
        //curl open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Corona Surveilance');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);
        exit();
    }
?>
</html>