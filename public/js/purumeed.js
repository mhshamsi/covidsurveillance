
function register(){
    var baseURL = $('#base_url').val();
    window.location.href = baseURL+"user";
}

function change_status(user_uid,checked,type,starting_date){
    // alert('user_uid: '+user_uid +'; checked: '+ checked +'; type: '+ type+'; starting_date: '+starting_date); return false;
    var baseURL = $('#base_url').val();
    var page = $('#page').val();
    var form_data = new FormData(); 
    form_data.append('user_uid',user_uid); 
    form_data.append('checked',checked); 
    form_data.append('type',type); 
    // form_data.append('starting_date',starting_date); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-change-status',
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          console.log('sending data...');
        },
        success: function(result) {
            $('#snake').hide();
            if(result.result.status=='success'){
                // toastr.success(result.result.message);
                window.location.href = baseURL+page;
            }else{
                // toastr.error(result.result.message);
                return false;
            }
        }
    });
}

$('#submit').click(function(){/*

    var baseURL = $('#base_url').val();
    var form_data = new FormData(); 

    if($.trim($('#starting_date').val()) == ''){
        toastr.error('Date of starting food diary cannot be blank.');
        $('#starting_date').focus();
        return false;
    }
    var visit_uid = $('#visit_uid').val();
    var starting_date = $('#starting_date').val();

    var start_date   = new Date(starting_date);
    var end_date     = new Date();
    var diffs = start_date.getTime() - end_date.getTime();
    var mills = 1000 * 60 * 60 * 24;
    var days  = Math.floor(diffs / (mills));
    if(days < -1 ){
        toastr.error('Starting date cannot be in the past');
        return false;
    }
    change_status( visit_uid,true,'food-diary',starting_date);
    $('#foodDiaryModal').modal('hide');
*/});


$('#role_id').click(function() {
    $('#participant_name').val('');
});

$('#participant_name').click(function() {
    $('#role_id').val('');
});

$('#search').click(function() {
    if($('#role_id').val() != ''){
        search('role_id',$('#role_id').val());
    }
    if($('#participant_name').val() != ''){
        search('participant_name',$('#participant_name').val());
    }
});

function search(search_type,search_value){
    
    // if($.trim(search_value) == ''){
    //     return false;
    // }

    var form_data = new FormData();
    var baseURL = $('#base_url').val();
    var page = $('#page').val();
    var form_data = new FormData(); 
    form_data.append('search_type',search_type); 
    form_data.append('search_value',search_value); 
    form_data.append('page',page); 
    $.ajax({
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseURL+'ajax-search-'+page,
        data: form_data,
        beforeSend: function(){
          $('#snake').show();
          $.getScript(baseURL+"js/purumeed.js")
          console.log('sending data...');
        },
        success: function(result){
            $('#snake').hide();
            if(result.result.status=='success'){
                $("table").find("tr:gt(0)").remove();
                $("tbody").append(result.response);
                return false;
            }else{
                // toastr.error(result.result.message);
                return false;
            }
        }
    });
}
