var icons = {
  "defaulters":       "assetsx/images/icons/red_circle.png",
  "videos":           "assetsx/images/icons/green_triangle.png",
  "anc-visits":       "assetsx/images/icons/green_rectangle.png",
  "family-planning":  "assetsx/images/icons/green_star.png",
  "registered":       "assetsx/images/icons/green_square.png",
  "vac-insideuc":     "assetsx/images/icons/green_circle.png",
  "vac-outsideuc":    "assetsx/images/icons/orange_circle.png",
  "at-risk":          "assetsx/images/icons/amber_triangle.png",
};
var query = {
  "uc_id": null,
  "lhw_id": null,
  "type": [],
  "startdate": null,
  "enddate": null
}
var map;
var markers = [];
$(function() {
  $(".filters").hide();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('#lhw_id').on("change", function(){
    $('#submit').click();
  });
  $('#uc_id').on("change", function(){
    var form_data = new FormData();
    form_data.append('uc_id',this.value);
    $.ajax({
      dataType: 'text',
      type: 'POST',
      cache: false,
      contentType: false,
      processData: false,
      url: '/ajax-admin-lhws',
      data: form_data,
      beforeSend: function(){
        $('#lhw_id').html('<option value="">Select Health Worker</option>');
        $('#snake').show();
      },
      success: function(msg){
        $('#lhw_id').html(msg);
        $('#submit').click();
      }
    });
  });
  $(".toggle-filters").on("click", (e) => {
    e.preventDefault();
    $(".filters").toggle();
    if($(".toggle-filters > span.oi").hasClass("oi-chevron-bottom")){
      $(".toggle-filters > span.oi").removeClass("oi-chevron-bottom");
      $(".toggle-filters > span.oi").addClass("oi-chevron-top");
    }else{
      $(".toggle-filters > span.oi").removeClass("oi-chevron-top");
      $(".toggle-filters > span.oi").addClass("oi-chevron-bottom");
    }
  });
  infowindow = new google.maps.InfoWindow();
  map = new google.maps.Map(document.getElementById('map'), {
    panControl: false,
    zoom: 6,
    maxZoom: 10,
    gestureHandling: 'cooperative',
    center: {
      lat: 31.32842697417807, lng: 70.93325610624991
      // lat: 34.757488439740655, lng: 74.85168898747247
    },
    styles: [{
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }]
    }, {
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#f5f5f5"
      }]
    }, {
      "featureType": "administrative.country",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#000000"
      }, {
        "visibility": "on"
      }, {
        "weight": 0.5
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#bdbdbd"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dadada"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#c9c9c9"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }]
  });
  map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(document.getElementById("filter-checkboxes"));
  map.addListener("click", () => {
    infowindow.close();
  });
  getListWithFilters();
  getStats();
  $("#submit").on("click", (event)=>{
    event.stopPropagation();
    getListWithFilters();
  });
  $("input[type=checkbox]").on("change", () => {
    getListWithFilters();
  });
  $(".cbutton > a").on("click", async function() {
    let target = $(this).data("target");
    $(".cbutton > a").each( (i, el) => {
      if($(el).data("target") == target) return;
      $(el).removeClass("clink-active");
      $($(el).parent().parent()).removeClass("card-active");
      $("#"+$(el).data("target")).hide();
    });
    if($(this).hasClass("clink-active")){
      $("#"+target).hide();
      $(this).html("More");
      $(this).removeClass("clink-active");
      $($(this).parent().parent()).removeClass("card-active");
    }else{
      $("#"+target).show();
      $(this).html("Less");
      $(this).addClass("clink-active");
      $($(this).parent().parent()).addClass("card-active");
      let data = await $.ajax({
        url: "/dashboard/stats/" + (target == "preg_stats" ? "pregnancies" : "vaccinations"),
        method: "post"
      });
      for(let x in data){
        $("#"+target+"_"+x).html(data[x]);
      }
    }
    
  });
});

async function getListWithFilters() {
  query.lhw_id = $("select[name=lhw]").val().length > 0 ? $("select[name=lhw]").val() : null;
  query.uc_id = $("select[name=uc]").val().length > 0 ? $("select[name=uc]").val() : null;
  query.startdate = $("input[name=startdate]").val().length > 0 ? $("input[name=startdate]").val() : null;
  query.enddate = $("input[name=enddate]").val().length > 0 ? $("input[name=enddate]").val() : null;
  query.requested_by = $("input[name=requested_by]").val().length > 0 ? $("input[name=requested_by]").val() : null;
  let types = []
  for(let cb of $("input[type=checkbox]:checked")){
    types.push($(cb).attr("id"));
  }
  query.type = types;
  $('#snake').show();
  let data = await $.ajax({
    method: "post",
    url: "/dashboard/map/markers", 
    data: JSON.stringify(query),
    contentType: "application/json",
    dataType: "json",
    processData: false,
  });
  $('#snake').hide();
  clearMarkers();
  for(let location of data){
    setMarker(location);
  }
}

async function getStats(){
  let data = await $.ajax({
    method: "post",
    url: "/dashboard/stats", 
    data: JSON.stringify({
      "requested_by": $("input[name=requested_by]").val().length > 0 ? $("input[name=requested_by]").val() : nulsl
    }),
    contentType: "application/json",
    processData: false,
  });
  for(let key in data){
    $("#dashboard_"+key+" > .h2").html(data[key]);
  }
}

function clearMarkers(){
  for(let marker of markers){
    marker.setMap(null);
  }
  markers = [];
}

function setMarker(place) {  
  let marker = new google.maps.Marker({
    map: map,
    position: {
      lat: parseFloat(place.lat),
      lng: parseFloat(place.lng),
    },
    icon: {
      url: icons[place.type], // url
      scaledSize: new google.maps.Size(15, 15), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    },
    title: place.type_show,
  });
  switch (place.type) {
    case "anc-visits":
    case "at-risk":
    case "family-planning":
    case 'registered':
    case 'vac-insideuc':
    case 'vac-outsideuc':
      marker.addListener('click', async () => {
        let view = await $.ajax({
          url: "/dashboard/map/info/profile",
          method: "post",
          data: {
            member_id: place.member_id,
            added_by: place.added_by,
            activity: place.type,
          }
        });
        infowindow.close();
        infowindow.setContent(view);
        infowindow.open(map, marker);
        $(".close-info-window").on("click", () => {
          infowindow.close();
        });
      });
      break;
      case 'videos':
        marker.addListener('click', async () => {
          let view = await $.ajax({
            url: "/dashboard/map/info/videos",
            method: "post",
            data: {
              video_name: place.video_name,
              added_by: place.added_by,
              added_on: place.added_on,
            }
          });
          infowindow.close();
          infowindow.setContent(view);
          infowindow.open(map, marker);
          $(".close-info-window").on("click", () => {
            infowindow.close();
          });
        });
      break;
    default:
      break;
  }
  markers.push(marker);
}
