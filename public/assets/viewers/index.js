"use strict";
$(function(){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  if($("#_page_dashboard_vaccine").length > 0){
    let vac = new VacDashboard();
    vac.constructor();
  }
  if($("#_page_dashboard_mnch").length > 0){
    let vac = new MNCHDashboard();
    vac.constructor();
  }
  if($("#_page_map").length > 0){
    let map = new MapDashboard();
    map.constructor();
  }
});

var VacDashboard = function(){
  this.chart = null;
  this.datatable = null;
  this.type = null;

  this.dateStart = "2019-04-01";
  this.dateEnd = moment().format("YYYY-MM-DD");
  
  this.constructor = function(){
    $('.daterange').daterangepicker({
      "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
      },
      "startDate": "2019-01-01",
      "minDate": "2019-01-01",
      "opens": "center"
    });
    $('.daterange.filter').daterangepicker({
      "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
      },
      "startDate": "2019-04-01",
      "minDate": "2019-01-01",
      "opens": "center"
    }, (start, end, label) => {
      this.dateStart = start.format("YYYY-MM-DD");
      this.dateEnd = end.format("YYYY-MM-DD");
      this.getStats();
    });
    $("#details").hide();
    $("#user_details").hide();
    $(".text-auto-width").fitText(0.7);
    $(".page-wrapper").on('resize', function(){
      $(".text-auto-width").fitText(0.7);
    });
    this.getStats();
    $(".get-info").on("click", this.getInfo);
    $(".online-status").on("click",  this.getUserDetails);
  }

  this.getUserDetails = async (e) => {
    $(".sk-loader").show()
    $("#landing").hide("slide", {direction: "left"});
    $("#user_details").show("slide", {direction: "right"});
    this.datatable = $("#user_list").DataTable({
      "processing": false,
      "serverSide": false,
      "paging": false,
      "ajax": {
        url: "/viewers/user-status",
        method: "post",
        data: $(e.delegateTarget).data(),
      },
      columns: [
        { "data": "district_name" },
        { "data": "uc_name" },
        { "data": "username" },
        { "data": "last_online" },
      ],
    });
    $(".sk-loader").hide();
    $(".btnGoBack").on("click", () => {
      this.datatable.destroy();
      $("#user_details").hide("slide", {direction: "left"});
      $("#landing").show("slide", {direction: "right"});
    });
  }

  this.getStats = async () => {
    $(".sk-loader").show();
    let response = await $.ajax({
      url: "/viewers/epi/stats",
      method: "post",
      data: {
        date_start: this.dateStart,
        date_end: this.dateEnd
      },
    });
    $("#child_reg_number").html(response["children"]["number"]);
    $("#bcg_number").html(response["bcg"]["number"]);
    $("#polio_number").html(response["polio"]["number"]);
    $("#penta_number").html(response["penta"]["number"]);
    $("#measles_number").html(response["measles"]["number"]);
    $("#fully_immunized").html(response["fully_vaccinated"]["number"]);
    $("#rota_number").html(response["rota"]["number"]);
    $("#vaccinators_online").html(response["vaccinators"]["online"]);
    $("#vaccinators_w5days").html(response["vaccinators"]["w5days"]);
    $("#vaccinators_m5days").html(response["vaccinators"]["m5days"]);
    $("#dropoff_penta12").html(response["dropoff"]["penta12"]);
    $("#dropoff_penta23").html(response["dropoff"]["penta23"]);
    $("#dropoff_m12").html(response["dropoff"]["m12"]);
    $(".text-auto-width").fitText(0.7);
    $(".sk-loader").hide();
  }

  this.getInfo = (e) => {
    this.type = $(e.currentTarget).data("type");
    this.setUpPathJS();
    $("#landing").hide("slide", {direction: "left"});
    $("#details").show("slide", {direction: "right"});
    $(".btnGoBack").on("click", this.eventBtnGoBack);
  }

  this.setUpPathJS = function(){
    if(this.type.indexOf("dropout") !== -1){
      Path.map("#/numbers").to(this.paths.numbers);
      Path.root("#/numbers")
    }else{
      Path.map("#/numbers").to(this.paths.numbers);
      Path.map("#/graph").to(this.paths.graph);
      Path.root("#/graph");
    }
    Path.listen();
  }

  this.tabGraph = {
    "getUCList" : async () => {
      let response = await $.ajax({
        url: "/location/ucs",
        method: "post",
      });
      $("#graph select[name=uc]").html("");
      $("#graph select[name=uc]").append("<option selected value=\"\">Select Union Council</option>");
      for(let uc of response){
        $("#graph select[name=uc]").append("<option value=\""+uc.uid+"\">"+uc.name+"</option>");
      }
    },
    "getVaccineList": async () => {
      let response = await $.ajax({
        url: "/viewers/list/vaccines",
        method: "post",
        data: {
          "group": this.type,
        }
      });
      $("#graph select[name=vaccine]").html("");
      if(response.length > 1) $("#graph select[name=vaccine]").append("<option selected value=\"\">Select Vaccine</option>");
      for(let vaccine of response){
        $("#graph select[name=vaccine]").append("<option value=\""+vaccine.uid+"\">"+vaccine.name+"</option>");
      }
    },
    "getUsers" : async () => {
      let response = await $.ajax({
        url: "/viewers/list/users",
        method: "post",
      });
      $("#graph select[name=user]").html("");
      for(let user of response){
        $("#graph select[name=user]").append("<option value=\""+user.uid+"\">"+user.username+" ("+ (user.privilege === 1 ? "LHW" : "VAC") +")"+"</option>");
      }
      $("#graph select[name=user]").select2({
        placeholder: 'Select a Health Worker',
        allowClear: true
      });
    },
    "graphData" : async () => {
      let dr = $("input[name=daterange]").val().split(" - ");
      let response = await $.ajax({
        url: "/viewers/epi/info",
        method: "post",
        data: {
          "user": $("#graph select[name=user]").val() === "" ? null : $("#graph select[name=user]").val(),
          "vaccine_group": this.type,
          "vaccine_id": $("#graph select[name=vaccine]").val() === "" ? null : $("#graph select[name=vaccine]").val(),
          "start_date": dr[0],
          "end_date": dr[1],
        },
      });
      let ctx = document.getElementById('vaccination_chart').getContext('2d');
      if(this.chart !== null) this.chart.destroy();
      this.chart = new Chart(ctx, {
        type: 'bar',
        data: response,
      });
    }
  };

  this.tabNumbers = {
    "getUCList" : async () => {
      let response = await $.ajax({
        url: "/location/ucs",
        method: "post",
      });
      $("#numbers select[name=uc]").html("");
      $("#numbers select[name=uc]").append("<option selected value=\"\">Select Union Council</option>");
      for(let uc of response){
        $("#numbers select[name=uc]").append("<option value=\""+uc.uid+"\">"+uc.name+"</option>");
      }
    },
    "getVaccineList": async () => {
      let response = await $.ajax({
        url: "/viewers/list/vaccines",
        method: "post",
        data: {
          "group": this.type,
        }
      });
      $("#numbers select[name=vaccine]").html("");
      if(response.length > 1) $("#numbers select[name=vaccine]").append("<option selected value=\"\">Select Vaccine</option>");
      for(let vaccine of response){
        $("#numbers select[name=vaccine]").append("<option value=\""+vaccine.uid+"\">"+vaccine.name+"</option>");
      }
    },
    "getUsers" : async () => {
      let response = await $.ajax({
        url: "/viewers/list/users",
        method: "post",
      });
      $("#numbers select[name=user]").html("");
      for(let user of response){
        $("#numbers select[name=user]").append("<option value=\""+user.uid+"\">"+user.username+" ("+ (user.privilege === 1 ? "LHW" : "VAC") +")"+"</option>");
      }
      $("#numbers select[name=user]").select2({
        placeholder: 'Select a Health Worker',
        allowClear: true
      });
    },
    "numberData" : async () => {
      let dr = $("#numbers input[name=daterange]").val().split(" - ");
      if(this.type.indexOf("dropout") !== -1){
        $("#numbers select[name=vaccine]").attr("disabled", "disabled");
        if(this.datatable !== null) this.datatable.destroy();
        this.datatable = $("#numbers table.table").DataTable({
          "processing": true,
          "serverSide": true,
          "paging": false,
          "ajax": {
            url: "/viewers/epi/dropout",
            method: "post",
            data: {
              "user": $("#numbers select[name=user]").val() === "" ? null : $("#numbers select[name=user]").val(),
              "vaccine_group": this.type,
              "start_date": dr[0],
              "end_date": dr[1],
            },
            "dataSrc": function(json){
              return json;
            }
          },
          "columns": [
            { "data": 'uc_name', "searchable": true },
            { "mRender": function(data, type, row){
              return row["males"]+"%";
            }, "searchable": false },
            { "mRender": function(data, type, row){
              return row["females"]+"%";
            }, "searchable": false },
            { "mRender": function(data, type, row){
              return row["undefined"]+"%";
            }, "searchable": false },
            { "mRender": function(data, type, row){
              return parseInt(row["males"])+parseInt(row["females"])+parseInt(row["undefined"])+"%";
            }},
          ],
        });
      }else{
        if(this.datatable !== null) this.datatable.destroy();
        this.datatable = $("#numbers table.table").DataTable({
          "processing": true,
          "serverSide": true,
          "paging": false,
          "ajax": {
            url: "/viewers/epi/tabular",
            method: "post",
            data: {
              "user": $("#numbers select[name=user]").val() === "" ? null : $("#numbers select[name=user]").val(),
              "vaccine_group": this.type,
              "vaccine_id": $("#numbers select[name=vaccine]").val() === "" ? null : $("#numbers select[name=vaccine]").val(),
              "start_date": dr[0],
              "end_date": dr[1],
            },
          },
          "columns": [
            { "data": 'uc_name', "searchable": true },
            { "data": "males", "searchable": false },
            { "data": "females", "searchable": false },
            { "data": "undefined", "searchable": false },
            { "mRender": function(data, type, row){
              return parseInt(row["males"])+parseInt(row["females"])+parseInt(row["undefined"]);
            }},
          ],
        });
      }
    }
  };

  this.eventBtnGoBack = () => {
    if(this.chart !== null) this.chart.destroy();
    $("#numbers select[name=vaccine]").removeAttr("disabled");
    $("#btnGoBack").off();
    $(".filter").off();
    $("#details").hide("slide", {direction: "right"});
    $("#landing").show("slide", {direction: "left"});
    window.location.hash = "";
  }

  this.filterData = this.detailedData;

  this.paths = {
    "graph" : () => {
      $(".tabs > .tab").hide();
      $("li.nav-item a.active").removeClass("active");
      $('a[href="#/graph"]').addClass("active");
      $("div.tab#graph").show();
      this.tabGraph.getUCList();
      this.tabGraph.getVaccineList();
      this.tabGraph.getUsers();
      this.tabGraph.graphData();
      $(".filter").off();
      $(".filter").on("change", this.tabGraph.graphData);
    },
    "numbers": () => {
      $(".tabs > .tab").hide();
      $("li.nav-item a.active").removeClass("active");
      $('a[href="#/numbers"]').addClass("active");
      $("div.tab#numbers").show();
      this.tabNumbers.getUCList();
      this.tabNumbers.getVaccineList();
      this.tabNumbers.getUsers();
      this.tabNumbers.numberData();
      $(".filter").off();
      $(".filter").on("change", this.tabNumbers.numberData);
    }
  }
}

var MNCHDashboard = function(){
  this.chart = null;
  this.datatable = null;
  this.type = null;
  this.dateStart = moment().month(moment().month() - 1).date(1).format("YYYY-MM-DD");
  this.dateEnd = moment().month(moment().month()).date(0).format("YYYY-MM-DD");
  
  this.constructor = function(){
    $("#details").hide();
    $("#user_details").hide();
    $(".accordian-body").hide();
    this.getDistricts();
    this.getUsers();
    $(".accordian-header").on("click", function(){
      if($($(this).siblings()[0]).hasClass("show")){
        $($(this).siblings()[0]).removeClass("show");
        $($(this).siblings()[0]).slideUp();
      }else{
        $(".show").slideUp().removeClass("show");
        $($(this).siblings()[0]).addClass("show");
        $($(this).siblings()[0]).slideDown();
        $(".text-auto-width").fitText(0.9);
      }
    });
    $('.daterange').daterangepicker({
      "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
      },
      "startDate": this.dateStart,
      "endDate": this.dateEnd,
      "minDate": "2019-01-01",
      "opens": "center",
    }, (start, end, label) => {
      this.dateStart = start.format("YYYY-MM-DD");
      this.dateEnd = end.format("YYYY-MM-DD");
      this.getStats();
    });
    this.getStats();
    $(".online-status").on("click",  this.getUserDetails);
    $("select[name=filter_district]").on("change", async () => {
      await this.getStats();
    });
    $("select[name=filter_hw]").on("change", async () => {
      await this.getStats();
    });
  }

  this.getDistricts = async function(){
    let response = await $.ajax({
      url: "/location/districts",
      method: "post",
    });
    $("select[name=filter_district]").html("");
    $("select[name=filter_district]").html("<option value=\"\" selected>Select a District</option>");
    for(let resp of response){
      $("select[name=filter_district]").append("<option value=\""+resp["uid"]+"\">"+resp["name"]+"</option>");
    }
  }

  this.getUsers = async function(){
    let response = await $.ajax({
      url: "/viewers/list/users",
      method: "post",
    });
    $("select[name=filter_hw]").html("");
    $("select[name=filter_hw]").html("<option value=\"\" selected>Select a Health Worker</option>");
    for(let resp of response){
      $("select[name=filter_hw]").append("<option value=\""+resp["uid"]+"\">"+resp["username"]+"</option>");
    }
  }

  this.getStats = async function(){
    $(".loading").fadeIn();
    let response = await $.ajax({
      url: "/viewers/mnch/stats",
      method: "post",
      data: {
        date_start: this.dateStart,
        date_end: this.dateEnd,
        district_id: $("select[name=filter_district]").val(),
        hw_id: $("select[name=filter_hw]").val()
      },
    });
    // console.log(response);
    $("#hh_registered").html(response["hh_registered"]["number"]);
    $("#hh_registered_members").html(response["hh_members_registered"]["number"]);
    $("#highrisk_number").html(response["highrisk_number"]["number"]);
    /* water source */
    $("#water_source_tap").html(response["hh_water"]["tap"]);
    $("#water_source_hp").html(response["hh_water"]["hp"]);
    $("#water_source_spring").html(response["hh_water"]["spring"]);
    $("#water_source_well").html(response["hh_water"]["well"]);
    $("#water_source_other").html(response["hh_water"]["other"]);
    /* maternal pregnancies */
    $("#maternal_new_pregnancies").html(response["maternal"]["new_pregnancies"]);
    $("#maternal_visits").html(response["maternal"]["visits"]);
    $("#maternal_iron_supplements").html(response["maternal"]["iron_supplements"]);
    $("#maternal_delivery_with_anv4").html(response["maternal"]["delivery_with_anv4"]);
    $("#maternal_delivery_with_tt").html(response["maternal"]["delivery_with_tt"]);
    $("#maternal_delivery_by_tp").html(response["maternal"]["delivery_by_tp"]);
    $("#maternal_pnc_wi_24").html(response["maternal"]["pnc_wi_24"]);
    /* children */
    $("#children_borns_weighted").html(response['children']['borns_weighted']);
    $("#children_vaccinated_month").html(response['children']['vaccinated_month']);
    $("#children_between_1223").html(response['children']['between_1223']);
    $("#children_fully_vaccinated_age_1223").html(response['children']['fully_vaccinated_age_1223']);
    $("#children_age_3").html(response['children']['age_3']);
    $("#children_fully_vaccinated").html(response['children']['fully_vaccinated']);
    /* family plan */
    $("#fp_new").html(response['family_plan']['new']);
    $("#fp_old").html(response['family_plan']['old']);
    $("#fp_total").html(response['family_plan']['total']);
    $("#fp_users_condom").html(response['family_plan']['users_condom']);
    $("#fp_users_pills").html(response['family_plan']['users_pills']);
    $("#fp_users_injections").html(response['family_plan']['users_pills']);
    $("#fp_users_iud").html(response['family_plan']['users_iud']);
    $("#fp_users_others").html(response['family_plan']['users_others']);
    $("#fp_referrals").html(response['family_plan']['referrals']);
    $("#fp_supplied_condoms").html(response['family_plan']['supplied_condoms']);
    $("#fp_supplied_pills").html(response['family_plan']['supplied_pills']);
    $("#fp_supplied_injectibles").html(response['family_plan']['supplied_injectibles']);
    $("#lhw_online").html(response["lhw"]["online"]);
    $("#lhw_w5days").html(response["lhw"]["w5days"]);
    $("#lhw_m5days").html(response["lhw"]["m5days"]);
    $(".loading").fadeOut();
  }

  this.getUserDetails = async (e) => {
    $(".sk-loader").show()
    $("#landing").hide("slide", {direction: "left"});
    $("#user_details").show("slide", {direction: "right"});
    this.datatable = $("#user_list").DataTable({
      "processing": false,
      "serverSide": false,
      "paging": false,
      "ajax": {
        url: "/viewers/user-status",
        method: "post",
        data: $(e.delegateTarget).data(),
      },
      columns: [
        { "data": "district_name" },
        { "data": "uc_name" },
        { "data": "username" },
        { "data": "last_online" },
      ],
    });
    $(".sk-loader").hide();
    $(".btnGoBack").on("click", () => {
      this.datatable.destroy();
      $("#user_details").hide("slide", {direction: "left"});
      $("#landing").show("slide", {direction: "right"});
    });
  }

}

var MapDashboard = function(){
  this.map = null;
  this.markers = [];
  this.infowindow = null;
  this.icons = {
    "defaulters":       "/assetsx/images/icons/red_circle.png",
    "videos":           "/assetsx/images/icons/green_triangle.png",
    "anc-visits":       "/assetsx/images/icons/green_rectangle.png",
    "family-planning":  "/assetsx/images/icons/green_star.png",
    "registered":       "/assetsx/images/icons/green_square.png",
    "vac-insideuc":     "/assetsx/images/icons/green_circle.png",
    "vac-outsideuc":    "/assetsx/images/icons/orange_circle.png",
    "at-risk":          "/assetsx/images/icons/amber_triangle.png",
  };
  
  this.constructor = () => {
    $(".filters").hide();
    $(".toggle-filters").on("click", (e) => {
      e.preventDefault();
      $(".filters").toggle();
      if($(".toggle-filters > span.oi").hasClass("oi-chevron-bottom")){
        $(".toggle-filters > span.oi").removeClass("oi-chevron-bottom");
        $(".toggle-filters > span.oi").addClass("oi-chevron-top");
      }else{
        $(".toggle-filters > span.oi").removeClass("oi-chevron-top");
        $(".toggle-filters > span.oi").addClass("oi-chevron-bottom");
      }
    });
    this.mapInit();
    $("#submit").on("click", (event) => {
      event.stopPropagation();
      this.getListWithFilters();
    });
    $("button#mapLegend").on("click", () => {
      this.getListWithFilters();
    });
    $("input#cb_all").on("change", function() {
      if($(this).is(":checked")){
        $(".legend_filter").each(function(){
          $(this).prop("checked", true);
        });
      }else{
        $(".legend_filter").each(function(){
          $(this).prop("checked", false);
        });
      }
    });
    this.getListWithFilters();
  }

  this.mapInit = () => {
    let zoom = $("meta[name=map_zoom]").attr("value");
    let center = $("meta[name=map_center]").attr("value");
    this.infowindow = new google.maps.InfoWindow();
    this.map = new google.maps.Map(document.getElementById('map'), {
      panControl: false,
      zoom: typeof zoom === "undefined" || zoom === null ? 6 : parseInt(zoom),
      maxZoom: 10,
      gestureHandling: 'cooperative',
      center: typeof center === "undefined" || center === null ? {
        lat: 31.32842697417807, lng: 70.93325610624991
      } : JSON.parse(center),
      styles: 
      [{
        "elementType": "geometry",
        "stylers": [{
          "color": "#f5f5f5"
        }]
      }, {
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#616161"
        }]
      }, {
        "elementType": "labels.text.stroke",
        "stylers": [{
          "color": "#f5f5f5"
        }]
      }, {
        "featureType": "administrative.country",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#000000"
        }, {
          "visibility": "on"
        }, {
          "weight": 0.5
        }]
      }, {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#bdbdbd"
        }]
      }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [{
          "color": "#eeeeee"
        }]
      }, {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#757575"
        }]
      }, {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{
          "color": "#e5e5e5"
        }]
      }, {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#9e9e9e"
        }]
      }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [{
          "color": "#ffffff"
        }]
      }, {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#757575"
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
          "color": "#dadada"
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#616161"
        }]
      }, {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#9e9e9e"
        }]
      }, {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [{
          "color": "#e5e5e5"
        }]
      }, {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [{
          "color": "#eeeeee"
        }]
      }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
          "color": "#c9c9c9"
        }]
      }, {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#9e9e9e"
        }]
      }]
    });
    this.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.getElementById("filter-checkboxes"));
    this.map.addListener("click", () => {
      this.infowindow.close();
    });
  }

  this.getListWithFilters = async () => {
    let query = {};
    query['lhw_id'] = $("select[name=hw_id]").val().length > 0 ? $("select[name=hw_id]").val() : null;
    query['uc_id'] = $("select[name=uc_id]").val().length > 0 ? $("select[name=uc_id]").val() : null;
    query['startdate'] = $("input[name=startdate]").val().length > 0 ? $("input[name=startdate]").val() : null;
    query['enddate'] = $("input[name=enddate]").val().length > 0 ? $("input[name=enddate]").val() : null;
    // query['requested_by'] = $("input[name=requested_by]").val().length > 0 ? $("input[name=requested_by]").val() : null;
    let types = []
    for(let cb of $("input[type=checkbox]:checked")){
      types.push($(cb).attr("id"));
    }
    query.type = types;
    $('#snake').show();
    let data = await $.ajax({
      method: "post",
      url: "/viewers/map/markers", 
      data: JSON.stringify(query),
      contentType: "application/json",
      dataType: "json",
      processData: false,
    });
    $('#snake').hide();
    this.clearMarkers();
    for(let location of data){
      this.setMarker(location);
    }
  }

  this.clearMarkers = () => {
    for(let i in this.markers){
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }
  
  this.setMarker = (place) => {  
    let marker = new google.maps.Marker({
      map: this.map,
      position: {
        lat: parseFloat(place.lat),
        lng: parseFloat(place.lng),
      },
      icon: {
        url: this.icons[place.type], // url
        scaledSize: new google.maps.Size(15, 15), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
      },
      title: place.type_show,
    });
    switch (place.type) {
      case "anc-visits":
      case "at-risk":
      case "family-planning":
      case 'registered':
      case 'vac-insideuc':
      case 'vac-outsideuc':
        marker.addListener('click', async () => {
          let view = await $.ajax({
            url: "/dashboard/map/info/profile",
            method: "post",
            data: {
              member_id: place.member_id,
              added_by: place.added_by,
              activity: place.type,
            }
          });
          this.infowindow.close();
          this.infowindow.setContent(view);
          this.infowindow.open(this.map, marker);
          $(".close-info-window").on("click", () => {
            this.infowindow.close();
          });
        });
        break;
        case 'videos':
          marker.addListener('click', async () => {
            let view = await $.ajax({
              url: "/dashboard/map/info/videos",
              method: "post",
              data: {
                video_name: place.video_name,
                added_by: place.added_by,
                added_on: place.added_on,
              }
            });
            this.infowindow.close();
            this.infowindow.setContent(view);
            this.infowindow.open(this.map, marker);
            $(".close-info-window").on("click", () => {
              this.infowindow.close();
            });
          });
        break;
      default:
        break;
    }
    this.markers.push(marker);
  }
}