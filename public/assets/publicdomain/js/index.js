$(function(){
  Chart.pluginService.register({
		beforeDraw: function (chart) {
			if (chart.config.options.elements.center) {
        //Get ctx from string
        var ctx = chart.chart.ctx;
				//Get options from the center object in options
        var centerConfig = chart.config.options.elements.center;
      	var fontStyle = centerConfig.fontStyle || 'Arial';
				var txt = centerConfig.text;
        var color = centerConfig.color || '#000';
        var sidePadding = centerConfig.sidePadding || 20;
        var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
        //Start with a base font of 30px
        ctx.font = "15px " + fontStyle;
        
				//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
        var stringWidth = ctx.measureText(txt).width;
        var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

        // Find out how much the font can grow in width.
        var widthRatio = elementWidth / stringWidth;
        var newFontSize = Math.floor(30 * widthRatio);
        var elementHeight = (chart.innerRadius * 2);

        // Pick a new font size so it will not be larger than the height of label.
        var fontSizeToUse = Math.min(newFontSize, elementHeight);

				//Set font settings to draw it correctly.
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
        var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
        ctx.font = fontSizeToUse+"px " + fontStyle;
        ctx.fillStyle = color;
        
        //Draw text in center
        centerY -= (txt.length - 2) * 20;
        let i = 0;
        for(let z of txt){
          ctx.fillText(z, centerX, centerY + (i++ * 20));
        }
			}
		}
	});
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: "/public-domain/vaccinations/individuals",
    method: "post",
    success: function( response ){
      let chartable = response;
      let ctx = $("#vaccination_i");
      let cOpt = {
        scales: {
          xAxes: [{
            barPercentage: 1,
            categoryPercentage: 0.7,
            ticks: {
              fontColor: "#000000",
            }
          }],
          yAxes: [{
            ticks: {
              fontColor: "#000000",
            }
          }],
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
      };
      let chart0 = new Chart(ctx, {
        type: "bar",
        data: chartable,
        options: cOpt,
      });
      
    },
    error: function(error) {
      console.log(error);
    }
  });
  $.ajax({
    url: "/public-domain/vaccinations/fully",
    method: "post",
    success: function( response ){
      let chartable = response;
      let ctx = $("#vaccination_f");
      let cOpt = {
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
        elements: {
          center: {
            text: ['Children'.toUpperCase(), 'Fully'.toUpperCase(), 'Vaccinated'.toUpperCase()],
            color: '#2e86ab', // Default is #000000
            fontStyle: 'Roboto', // Default is Arial
            sidePadding: 20 // Defualt is 20 (as a percentage)
          }
        },
        legend: {
          display: true,
          position: "bottom"
        },
        tooltips: {
          callbacks: {
            label: function(item, data) {
              return data.datasets[item.datasetIndex].label+ " - "+ data.labels[item.index]+ ": "+ data.datasets[item.datasetIndex].data[item.index];
            }
          }
        }
      };
      let chart0 = new Chart(ctx, {
        type: "doughnut",
        data: chartable,
        options: cOpt,
      });    
    },
    error: function(error) {
      console.log(error);
    }
  });
  $.ajax({
    url: "/public-domain/anc-visits",
    method: "post",
    success: function( response ){
      for(let type in response){
        let c0 = new CountUp("anc_"+type, 0, response[type]);
        if (!c0.error) {
          c0.start();
        } else {
          console.error(c0.error);
        }
        let c1 = new CountUp("anc_"+type, 0, response[type]);
        if (!c1.error) {
          c1.start();
        } else {
          console.error(c1.error);
        }
      }
    },
    error: function(error) {
      console.log(error);
    }
  });
  $.ajax({
    url: "/public-domain/registrations",
    method: "post",
    success: function( response ){
      for(let type in response){
        let c0 = new CountUp("registerations_"+type+"_males", 0, response[type]['males']);
        if (!c0.error) {
          c0.start();
        } else {
          console.error(c0.error);
        }
        let c1 = new CountUp("registerations_"+type+"_females", 0, response[type]['females']);
        if (!c1.error) {
          c1.start();
        } else {
          console.error(c1.error);
        }
      }
    },
    error: function(error) {
      console.log(error);
    }
  });
  $.ajax({
    url: "/public-domain/client-entries",
    method: "post",
    success: function( response ){
      for(let type in response){
        let c0 = new CountUp("entries_"+type, 0, response[type]);
        if (!c0.error) {
          c0.start();
        } else {
          console.error(c0.error);
        }
      }
    },
    error: function(error) {
      console.log(error);
    }
  });
  $.ajax({
    url: "/public-domain/app-users",
    method: "post",
    success: function( response ){
      for(let type in response){
        let c0 = new CountUp("users_"+type, 0, response[type]);
        if (!c0.error) {
          c0.start();
        } else {
          console.error(c0.error);
        }
      }
    },
    error: function(error) {
      console.log(error);
    }
  });
  $.ajax({
    url: "/public-domain/awareness-sessions",
    method: "post",
    success: function( response ){
      for(let type in response){
        let c0 = new CountUp("awareness_"+type, 0, response[type]);
        if (!c0.error) {
          c0.start();
        } else {
          console.error(c0.error);
        }
      }
    },
    error: function(error) {
      console.log(error);
    }
  });
});