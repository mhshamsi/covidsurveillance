<?php

class Api {
	public static function get_api_auth_token()
    {
        $authentication_token = 'g3hphugb4i76x3ffgejucb3fgl1zapld';
        return $authentication_token;
    }

    public static function get_api_new_auth_token()
    {
        $authentication_token = 'h398culov18a8sf4ftz5m385auiigh2y';
        return $authentication_token;
    }

    public static function get_user_info($user_auth_token)
    {
        $user = DB::table('users')
                    ->where('user_auth_token', $user_auth_token)
                    ->first();

        if(!empty($user))
        {
            return $user;    
        }
        else
        {
            return '';
        }
    }

    public static function get_api_transaction_id()
    {
        // $apiTransactionID = LMongo::collection('api_activity_logs')

        $date = date('Y-m-d h:i:s');
        $data = array('uri' => URL::current(), 'created_at' => $date);
        $apiTransactionID = DB::connection('mysql')
          ->table('api_activity_logs')
          ->insertGetId($data);
        return $apiTransactionID;
    }

    public static function update_activity_log($apiTransactionID, $parameters, $response)
    {
        // LMongo::collection('api_activity_logs')

        $date = date('Y-m-d h:i:s');
        $data = array(
                'parameters' => serialize($parameters),
                'response' => serialize($response),
                'updated_at' => $date
            );
        DB::connection('mysql')
          ->table('api_activity_logs')
          ->where('id', $apiTransactionID)
          ->update($data);    

        return true;
    }

    public static function get_device_status($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'device_id' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            $device = LMongo::collection('devices')
                        ->where('device_id', $input['device_id'])
                        ->where('user_id', $user->id)
                        ->first();

            if(!empty($device))
            {
                if($device['is_active'] == 1)
                {
                    $status['code'] = 'success';
                    $result['status'] = $status;
                    $result['response'] = array(
                                                'status' => $device['status']
                                            );
                }
                else
                {
                    $status['code'] = 'error';
                    $result['status'] = $status;
                    $result['response'] = array(
                                                'general' => 'Device registered but not activated.'
                                            );
                }
            }
            else
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('invalid' => 'Device not exists!');
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }    	

        return $result;
    }

    public static function update_device_status($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'phone_no'      => 'required|regex:/^[0-9\-]+$/',
                'device_id'     => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            $device = LMongo::collection('devices')
                        ->where('phone_no', $input['phone_no'])
                        ->where('device_id', $input['device_id'])
                        ->where('user_id', $user->id)
                        ->first();

            if(!empty($device))
            {
                $data = array(
                    'status'        => 'online', 
                    'updated_at'    => date('Y-m-d h:i:s')
                    );

                LMongo::collection('devices')
                  ->where('phone_no', $device['phone_no'])
                  ->where('device_id', $device['device_id'])
                  ->where('user_id', $user->id)
                  ->update($data);

                $device = LMongo::collection('devices')
                        ->where('phone_no', $input['phone_no'])
                        ->where('device_id', $input['device_id'])
                        ->where('user_id', $user->id)
                        ->first();  

                $status['code'] = 'success';
                $result['status'] = $status;
                $result['response'] = array(
                                            'status' => $device['status']                                            
                                        );
            }
            else
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('invalid' => 'Device not exists!');
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }

    public static function register_device($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'user_id'       => 'required',
                'phone_carrier' => 'required',
                'phone_no'      => 'required|regex:/^[0-9\-]+$/',
                'device_type'   => 'required',
                'device_id'     => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            $device = LMongo::collection('devices')
                        ->where('device_id', $input['device_id'])
                        ->first();

            if(empty($device))
            {
                $data = array(
                    'user_id'           => $user->id, 
                    'phone_carrier'     => $input['phone_carrier'], 
                    'phone_no'          => $input['phone_no'], 
                    'status'            => 'online', 
                    'device_type'       => $input['device_type'], 
                    'device_id'         => $input['device_id'], 
                    'registered_at'     => date('Y-m-d h:i:s'),
                    'updated_at'        => date('Y-m-d h:i:s'),
                    'activation_code'   => rand(1000, 9999),
                    'is_active'         => 0
                    );

                $transaction_id = LMongo::collection('devices')->insert($data);

                if($transaction_id)
                {
                    $carriers = LMongo::collection('carriers')
                                ->where('name', $input['phone_carrier'])
                                ->first();

                    // Get activation code from db
                    $device = LMongo::collection('devices')
                                ->where('device_id', $input['device_id'])
                                ->first();

                    //$to_email = $input['phone_no'].'@'.$carriers['tld'];
                    $to_email = 'azam.wahaj@cooperativecomputing.net';
                    $msg = 'Your Slate code is: '.$device['activation_code'].', Close this message and enter the code into Slate to activate your account.';

                    // email activation code to telcom service

                    // Mail
                    $transport = Swift_MailTransport::newInstance();
                    
                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);

                    // Create a message
                    $message = Swift_Message::newInstance()
                      ->setSubject('Slate Activation Code')  
                      ->setFrom(array('noreply@devapi.slateschedules.com' => 'Slate Mobile App'))
                      ->setTo($to_email)
                      ->setBody($msg, 'text/plain')
                      ;

                    // Send the message
                    $mailer->send($message);

                    $status['code'] = 'success';
                    $result['status'] = $status;
                    $result['response'] = array('transaction_id' => $transaction_id);
                }
                else
                {
                    $status['code'] = 'error';
                    $result['status'] = $status;
                    $result['response'] = array('general' => 'Record not added.');   
                }
            }
            else
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('general' => 'Device already registered!');
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }

    public static function generate_ssl_keys($input)
    {
        $input['ssl_keys_path'] = (isset($input['ssl_keys_path'])) ? $input['ssl_keys_path'] : ''; 
        $input['export_to_file'] = (isset($input['export_to_file'])) ? $input['export_to_file'] : false;

        $privateKey = openssl_pkey_new($input['config']);

        if($input['export_to_file']==true)
        {
            openssl_pkey_export_to_file($privateKey, $input['ssl_keys_path'].'private.key', NULL, $input['config']);
        }
        else
        {
            openssl_pkey_export($privateKey, $private_key, NULL, $input['config']);
        }
        
        $a_key = openssl_pkey_get_details($privateKey);
        
        if($input['export_to_file']==true)
        {
            file_put_contents($input['ssl_keys_path'].'public.key', $a_key['key']);
        }
        else
        {
            $public_key = $a_key['key'];
        }

        openssl_free_key($privateKey);

        if($input['export_to_file']==true)
        {
            $data = array(
                    'private_key'   => file_get_contents($input['ssl_keys_path'].'private.key'),
                    'public_key'    => file_get_contents($input['ssl_keys_path'].'public.key')
                );
        }
        else
        {
            $data = array(
                    'private_key'   => $private_key,
                    'public_key'    => $public_key
                );
        }

        return $data;
    }

    public static function authorize_device($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'user_id'         => 'required',
                'activation_code' => 'required|numeric',
                'device_id'       => 'required',
                'public_key'      => 'required'                
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }
            
            $device = LMongo::collection('devices')
                        ->where('device_id', $input['device_id'])
                        ->where('user_id', $user->id)
                        ->first();

            if(!empty($device))
            {
                if($device['is_active']==0)
                {
                    if($device['activation_code']==$input['activation_code'])
                    {
                        $data = array(
                            'status'     => 'online',
                            'updated_at' => date('Y-m-d h:i:s'),
                            'is_active'  => 1,
                            'public_key' => $input['public_key']
                            );

                        LMongo::collection('devices')
                          ->where('device_id', $device['device_id'])
                          ->where('user_id', $user->id)
                          ->update($data);

                        // fetch web public key
                        $ssl_keys_path = Config::get('openssl.ssl_keys_path');  
                        $public_key  = @file_get_contents($ssl_keys_path.'public.key');

                        $status['code'] = 'success';
                        $result['status'] = $status;
                        $result['response'] = array('web_public_key' => $public_key);
                    }
                    else
                    {
                        $status['code'] = 'error';
                        $result['status'] = $status;
                        $result['response'] = array('invalid' => 'Invalid Activation Code!');
                    }
                }
                else
                {
                    $status['code'] = 'error';
                    $result['status'] = $status;
                    $result['response'] = array('general' => 'Device already authorized!');
                }
            }
            else
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('general' => 'Device not exists!');
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }


    public static function check_password($passwd, $dbEntryx) {
    $dbEntry = explode(':',$dbEntryx);
      // new format as {HASH}:{SALT}
      $cryptpass = $dbEntry[0];
      $salt = $dbEntry[1];

      if($dbEntryx == md5($passwd.$salt).':'.$salt)
            return true;
      else  return false;
    }

    public static function authenticate_user($input){

        $status['api_transaction_id'] = $input['api_transaction_id'];        

        {
            // Validate the Input Fields
            $rules = array(
                'username'     => 'required',
                'password'     => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();
                return $result;
            }

            $user = DB::connection('mysql')
              ->table('users')
              ->where('username', $input['username'])
              ->first();

            $authenticated = static::check_password($input['password'],$user->password);

            if($authenticated == true)
            {

            $groups = DB::connection('mysql')
            ->table('user_usergroup_map')
            ->join('usergroups', 'user_usergroup_map.group_id', '=', 'usergroups.id')
            ->where('user_usergroup_map.user_id', '=', '43')->get();

            if(count($groups)>0){
                for($i=0;$i<count($groups);$i++){
                        $my_groups[] = $groups[$i]->id;
                }
                $my_groups = implode(',',$my_groups);
            }else $my_groups = '';

            $accounts = DB::connection('mysql')
            ->table('slatescheduler_useraccount_map')
            ->join('slatescheduler_accounts', 'slatescheduler_useraccount_map.accountId', '=', 'slatescheduler_accounts.id')
            ->where('slatescheduler_useraccount_map.userId', '=', '43')->get();
            //echo '<pre>';print_r($accounts[0]->id);
            
            if(count($accounts)>0){ 
                for($i=0;$i<count($accounts);$i++){
                        $my_accounts[] = $accounts[$i]->id;

                }
                $my_accounts = implode(',',$my_accounts);
            } else $my_accounts = '';

                $status['code'] = 'success';
                $result['status'] = $status;
                $result['response'] = array(
                    'ID' => $user->id,
                    'Name' => $user->name,
                    'Email' => $user->email,
                    'UserGroup' => $my_groups,
                    'UserAuthToken' => $user->user_auth_token,
                    'UserAccounts' => $my_accounts,
                    );
            }
            else
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('invalid' => 'User authentication failed!');
            }

            return $result;

        }
    }

    public static function decrypt_message($info)
    {
        if(!empty($info))
        {
            $privateKey = $info['private_key'];
            $encrypted  = $info['message'];

            if (!$privateKey = openssl_pkey_get_private($privateKey))
                die('Private Key failed');

            $a_key = openssl_pkey_get_details($privateKey);
            $chunkSize = ceil($a_key['bits'] / 8);
            $output = '';
            while ($encrypted) {
                $chunk = substr($encrypted, 0, $chunkSize);
                $encrypted = substr($encrypted, $chunkSize);
                $decrypted = '';
                if (!openssl_private_decrypt($chunk, $decrypted, $privateKey))
                    die('Failed to decrypt data');

                $output .= $decrypted;
            }

            openssl_free_key($privateKey);
        }
        else
        {
            $output = false;
        }
        
        return $output;
    }

    public static function encrypt_message($info)
    {
        if(!empty($info))
        {
            $publicKey = $info['public_key'];
            $plaintext = $info['message'];

            $publicKey = openssl_pkey_get_public($publicKey);
            $a_key = openssl_pkey_get_details($publicKey);
            $chunkSize = ceil($a_key['bits'] / 8) - 11;
            $output = '';
            while ($plaintext) {
                $chunk = substr($plaintext, 0, $chunkSize);
                $plaintext = substr($plaintext, $chunkSize);
                $encrypted = '';
                if (!openssl_public_encrypt($chunk, $encrypted, $publicKey))
                    die('Failed to encrypt data');
                $output .= $encrypted;
            }
            openssl_free_key($publicKey);
        }
        else
        {
            $output = false;
        }

        return $output;
    }

    public static function send_message($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'sender_no'   => 'required|regex:/^[0-9\-]+$/',
                'receiver_no' => 'required|regex:/^[0-9\-]+$/',
                'message'     => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            $devices_sender = LMongo::collection('devices')
                                ->where('phone_no', $input['sender_no'])
                                ->where('user_id', $user->id)
                                ->get();

            $devices_receiver = LMongo::collection('devices')
                                ->where('phone_no', $input['receiver_no'])
                                ->get();                    
                        
            foreach ($devices_sender as $device_sender)
            {
                $sender_devices[] = $device_sender;
            }

            foreach ($devices_receiver as $device_receiver)
            {
                $receiver_devices[] = $device_receiver;
            }

            if(empty($sender_devices))
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('general' => 'Sender device not exists!');
            }
            elseif(empty($receiver_devices))
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('general' => 'Receiver device not exists!');
            }
            else
            {
                // get web private key
                $ssl_keys_path = Config::get('openssl.ssl_keys_path');
                $private_key  = @file_get_contents($ssl_keys_path.'private.key');

                // parameters to decrypt message by web private key
                $web_key_message_info = [
                            'message'     => $input['message'],
                            'private_key' => $private_key
                        ];
                $decrypted_message = static::decrypt_message($web_key_message_info);

                foreach ($sender_devices as $sender_device) {
                    
                    // parameters to encrypt sender message by device public key
                    $sender_device_key_message_info = [
                                                            'message'    => $decrypted_message,
                                                            'public_key' => $sender_device['public_key']
                                                      ];
                    $sender_encrypted_message = static::encrypt_message($sender_device_key_message_info);

                    $sender_messages[] = [
                                            'device_id' => $sender_device['device_id'],
                                            'message'   => $sender_encrypted_message
                                         ];
                }

                foreach ($receiver_devices as $receiver_device) {
                    
                    // parameters to encrypt receiver message by device public key
                    $receiver_device_key_message_info = [
                                                    'message'    => $decrypted_message,
                                                    'public_key' => $receiver_device['public_key']
                                               ];
                    $receiver_encrypted_message = static::encrypt_message($receiver_device_key_message_info);

                    $receiver_messages[] = [
                                                'device_id' => $receiver_device['device_id'],
                                                'message'   => $receiver_encrypted_message
                                           ];
                }
                
                $messages_to_store = [
                                        'web'      => $input['message'],
                                        'sender'   => $sender_messages,
                                        'receiver' => $receiver_messages
                                     ];

                // serialize and encode array to store in mongodb
                $messages_to_store_encoded = base64_encode(serialize($messages_to_store));                     

                $data = array(
                    'sender_no'   => $input['sender_no'], 
                    'receiver_no' => $input['receiver_no'], 
                    'message'     => $messages_to_store_encoded,
                    'status'      => 'sent', 
                    'created_at'  => date('Y-m-d h:i:s'),
                    'updated_at'  => date('Y-m-d h:i:s')
                    );

                $transaction_id = LMongo::collection('messages')->insert($data);

                $status['code'] = 'success';
                $result['status'] = $status;
                $result['response'] = array('transaction_id' => $transaction_id);
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }

    public static function get_messages($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'user_id'   => 'required|regex:/^[0-9\-]+$/',
                'device_id' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            $device = LMongo::collection('devices')
                                ->where('device_id', $input['device_id'])
                                ->where('user_id', $user->id)
                                ->first();
                                
            if(!empty($device))
            {
                $messages_sent = LMongo::collection('messages')
                                    ->where('sender_no', $device['phone_no'])
                                    ->get();

                $messages_received = LMongo::collection('messages')
                                    ->where('receiver_no', $device['phone_no'])
                                    ->where('status', 'sent')
                                    ->get();

                foreach ($messages_sent as $message_sent)
                {
                    $sent_messages[] = $message_sent;
                }

                foreach ($messages_received as $message_received)
                {
                    $received_messages[] = $message_received;
                }

                if(!empty($sent_messages))
                {
                    foreach ($sent_messages as $sent_item) {
                        $sent_items[] = unserialize(base64_decode($sent_item['message']));
                    }

                    foreach ($sent_items as $sender_message) {
                        $sender_messages[] = $sender_message['sender'];
                    }

                    foreach ($sender_messages as $sender_items) {
                        foreach ($sender_items as $key => $sender_item) {
                            if($sender_item['device_id'] == $input['device_id'])
                            {
                                $sender_item['receiver_no'] = $sent_messages[$key]['receiver_no'];
                                $sender_item['message_id'] = $sent_messages[$key]['_id'];
                                $sender_device_messages[] = $sender_item;
                            }
                        }
                    }
                }

                if(!empty($received_messages))
                {
                    foreach ($received_messages as $received_item) {
                        $received_items[] = unserialize(base64_decode($received_item['message']));
                    }

                    foreach ($received_items as $receiver_message) {
                        $receiver_messages[] = $receiver_message['receiver'];
                    }

                    foreach ($receiver_messages as $receiver_items) {
                        foreach ($receiver_items as $key => $receiver_item) {
                            if($receiver_item['device_id'] == $input['device_id'])
                            {
                                $receiver_item['sender_no'] = $received_messages[$key]['sender_no'];
                                $receiver_item['message_id'] = $received_messages[$key]['_id'];
                                $receiver_device_messages[] = $receiver_item;
                            }
                        }
                    }
                }

                $device_sent_received_messages = [
                            'sent_messages'     => (isset($sender_device_messages)) ? $sender_device_messages : '',
                            'received_messages' => (isset($receiver_device_messages)) ? $receiver_device_messages : ''
                         ];

                $status['code'] = 'success';
                $result['status'] = $status;
                $result['response'] = base64_encode(serialize($device_sent_received_messages));
            }
            else
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('invalid' => 'Device not exists!');
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }

    public static function get_message_status($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'message_id' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            try
            {
                $message = LMongo::collection('messages')->find($input['message_id']);

                $status['code'] = 'success';
                $result['status'] = $status;
                $result['response'] = array('message_status' => $message['status']);
            } 
            catch (Exception $e) 
            {

                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('Caught exception' => $e->getMessage());
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }

    public static function update_message_status($input)
    {
        $status['api_transaction_id'] = $input['api_transaction_id'];
        $user = static::get_user_info($input['user_auth_token']);

        if(!empty($user))
        {
            // Validate the Input Fields
            $rules = array(
                'message_id'     => 'required',
                'message_status' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails())
            {
                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = $validator->messages()->all();

                return $result;
            }

            try
            {
                $message = LMongo::collection('messages')->find($input['message_id']);

                $date = date('Y-m-d h:i:s');

                $data = array(
                        'status'     => $input['message_status'],
                        'updated_at' => $date
                    );

                LMongo::collection('messages')
                  ->where('_id', $message['_id'])
                  ->update($data);

                $message = LMongo::collection('messages')->find($input['message_id']);  

                $status['code'] = 'success';
                $result['status'] = $status;
                $result['response'] = array('message_status' => $message['status']);
            } 
            catch (Exception $e) 
            {

                $status['code'] = 'error';
                $result['status'] = $status;
                $result['response'] = array('Caught exception' => $e->getMessage());
            }
        }
        else
        {
            $status['code'] = 'error';
            $result['status'] = $status;
            $result['response'] = array('invalid' => 'User Authentication Token is not valid.');
        }       

        return $result;
    }
}