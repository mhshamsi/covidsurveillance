<html>
<head>
<title></title>
</head>
<?php //include("header.php");?>
<h1>Arabic Text</h1>
<form method="POST" action="">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="http://<?php echo $_SERVER['SERVER_NAME'];?>/<?php echo ($_SERVER['SERVER_NAME']!='etiqh.local')?'public/':'';?>api/v1/etiqh/arabic-text" size="50"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;"><input type="submit" name="submit" value="Submit"></td>
        </tr>
    </table>
</form>
<?php
    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        $url = $_POST['url'];
        $fields = array(
            // 'auth_token'            => $_POST['auth_token'],
            // 'device_id'             => $_POST['device_id'],
        );
        echo '<pre>POST '; print_r($fields); echo '</pre>';
        
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'slate Client');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);

        //execute post
        //echo '<pre>';
        $result = curl_exec($ch);
        //echo '</pre>';

        //close connection
        curl_close($ch);
    }
?>
</html>