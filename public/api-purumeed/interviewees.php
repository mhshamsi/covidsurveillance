<html>
<head>
<title></title>
</head>
<?php //include("header.php");?>
<h1>Interviewees</h1>
<form method="POST" action="">
    <table width="50%">
        <tr>
            <td><label for="url">Api Url</label></td>
            <td><input type="text" name="url" id="url" value="http://<?php echo $_SERVER['SERVER_NAME'];?>/<?php echo ($_SERVER['SERVER_NAME']!='etiqh.local')?'public/':'';?>api/v1/etiqh/interviewees" size="50"></td>
        </tr>
        <tr>
            <td><label for="interviewees">Interviewees</label></td>
            <td>
                <textarea name="interviewees" id="interviewees" cols="100" rows="20">
{"result":{"status":"success"},"response":[{"ID":"1","created_by_id":"1","health_facility_id":"1","assessment_id":"1","tool_id":"1","interviewee_userid":"1","interviewee_name":"Fabiha  ","interviewee_title":"Medical Officer ","indicator_id":"1","sub_indicator":"1.1a","question_id":"1","question":"The facility’s immediate surroundings are free from long grass, paper debris and solid waste.","answer":"0","isSynced":"0","created_at":"2019-01-03 14:04:09"},{"ID":"2","created_by_id":"1","health_facility_id":"1","assessment_id":"1","tool_id":"1","interviewee_userid":"1","interviewee_name":"Fabiha  ","interviewee_title":"Medical Officer ","indicator_id":"1","sub_indicator":"1.1b","question_id":"2","question":"The facility has clear demarcated boundaries.","answer":"1","isSynced":"0","created_at":"2019-01-03 14:04:10"},{"ID":"3","created_by_id":"1","health_facility_id":"1","assessment_id":"1","tool_id":"1","interviewee_userid":"1","interviewee_name":"Fabiha  ","interviewee_title":"Medical Officer ","indicator_id":"1","sub_indicator":"1.1c","question_id":"3","question":"The building infrastructure is in good and solid condition.","answer":"0","isSynced":"0","created_at":"2019-01-03 14:04:10"},{"ID":"4","created_by_id":"1","health_facility_id":"1","assessment_id":"1","tool_id":"1","interviewee_userid":"1","interviewee_name":"Fabiha  ","interviewee_title":"Medical Officer ","indicator_id":"2","sub_indicator":"1.1d","question_id":"4","question":"The roof is intact.","answer":"0","isSynced":"0","created_at":"2019-01-03 14:07:30"},{"ID":"5","created_by_id":"1","health_facility_id":"1","assessment_id":"1","tool_id":"1","interviewee_userid":"1","interviewee_name":"Fabiha  ","interviewee_title":"Medical Officer ","indicator_id":"2","sub_indicator":"1.1e","question_id":"5","question":"The walls and floors are intact, smooth and cleanable.","answer":"0","isSynced":"0","created_at":"2019-01-03 14:07:30"},{"ID":"6","created_by_id":"1","health_facility_id":"1","assessment_id":"1","tool_id":"1","interviewee_userid":"1","interviewee_name":"Fabiha  ","interviewee_title":"Medical Officer ","indicator_id":"2","sub_indicator":"1.1f","question_id":"6","question":"The infrastructure is user friendly for physically challenged individuals.","answer":"0","isSynced":"0","created_at":"2019-01-03 14:07:31"}]}
                </textarea>
            </td>
            <td colspan="2" style="text-align:right;"><input type="submit" name="submit" value="Submit"></td>
        </tr>
    </table>
</form>
<?php 
    if(isset($_POST['submit']) && $_POST['submit']=='Submit')
    {
        echo '<h3>Api Response</h3>';
        
        //set POST variables
        $url = $_POST['url'];
        $fields = array(
            'interviewees'      => $_POST['interviewees'],
        );
        // echo '<pre>POST '; print_r($fields); echo '</pre>';
        
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'slate Client');
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);

        //execute post
        //echo '<pre>';
        $result = curl_exec($ch);
        //echo '</pre>';

        //close connection
        curl_close($ch);
    }
?>
</html>