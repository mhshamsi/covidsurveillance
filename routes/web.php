<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/', 'CoronaController@getIndex');
    Route::get('/dashboard', 'CoronaController@getDashboard');
    Route::get('/generate-uid/{table_name}', 'CoronaController@getGenerateUid');
    Route::get('/users', 'CoronaController@getUsers');
    Route::get('/user', 'CoronaController@getUser');
    Route::get('/report', 'CoronaController@getReport');
    Route::get('/export', 'CoronaController@getExport');
    Route::get('/user/{user_uid}', 'CoronaController@getUser');
    Route::get('/logout', 'CoronaController@getLogout');
    Route::get('/login', 'CoronaController@getLogin');
    Route::get('/register', 'CoronaController@getRegister');
    Route::get('/generate-passwords', 'CoronaController@getGeneratePassword');
    Route::get('/generate-usernames', 'CoronaController@getGenerateUsernames');
    Route::get('/forgot-password', 'CoronaController@getForgotPassword');
    Route::get('/reset-password', 'CoronaController@getResetPassword');
    Route::get('/create-account', 'CoronaController@getCreateAccount');
    Route::get('/signin/{token_uid}', 'CoronaController@getSignin');
    Route::get('/participants', 'CoronaController@getParticipants');
    Route::get('/ineligible-participants', 'CoronaController@getIneligibleParticipants');
    Route::get('/admin-profile', 'CoronaController@getAdminProfile');
    Route::get('/poppulate-users', 'CoronaController@getPoppulateUsers');
    Route::post('/ajax-search-control-panel', 'CoronaController@postAjaxSearchControlPanel');
    Route::post('/ajax-search-participants', 'CoronaController@postAjaxSearchParticipants');
    Route::post('/ajax-change-status', 'CoronaController@postAjaxChangeStatus');
    Route::post('/ajax-search-users', 'CoronaController@postAjaxSearchControlPanel');

    Route::post('/ajax-admin-profile', 'CoronaController@postAdminProfile');
    Route::post('/ajax-login', 'CoronaController@postAjaxLogin');
    Route::post('/ajax-register', 'CoronaController@postAjaxRegister');

    Route::post('/corona-login', 'CoronaController@postApiLogin');
    Route::post('/corona-register', 'CoronaController@postApiRegister');
    Route::post('/corona-assessment', 'CoronaController@postApiAssessment');
    Route::post('/corona-followup', 'CoronaController@postApiFollowup');
    Route::post('/corona-sample-collection', 'CoronaController@postApiSampleCollection');
    Route::post('/corona-test-result', 'CoronaController@postApiTestResult');
    Route::post('/corona-synchronize', 'CoronaController@postApiSynchronize');

    Route::post('/corona-download-lhv-data', 'CoronaController@postApiDownloadLhvData');
    Route::post('/corona-download-lhv-comm-data', 'CoronaController@postApiDownloadLhvCommData');
    Route::post('/corona-download-comm-lhv-data', 'CoronaController@postApiDownloadCommLhvData');
    Route::post('/corona-download-helpline-lhv-data', 'CoronaController@postApiDownloadHelplineLhvData');
    Route::post('/corona-download-helpline-comm-data', 'CoronaController@postApiDownloadHelplineCommData');
    /*
    Route::get('/push-notification', 'CoronaController@getPushNotification');
    Route::get('/add-notification', 'CoronaController@getAddNotification');
    Route::post('/ajax-add-notification', 'CoronaController@postAjaxAddNotification');
    Route::post('/ajax-add-user-notification', 'CoronaController@postAjaxAddUserNotification');
    Route::post('/ajax-search-ineligible-participants', 'CoronaController@postAjaxSearchIneligibleParticipants');

    Route::get('/user-profile/{user_uid}', 'CoronaController@getUserProfile');
    Route::get('/user-push-notification/{user_uid}', 'CoronaController@getUserPushNotification');
    Route::post('/ajax-user-push-notification', 'CoronaController@postAjaxGetUserNotification');
    Route::post('/ajax-get-notification', 'CoronaController@postAjaxGetNotification');
    */
    